
const chatService = require('../services/chat.service')

module.exports = function (io) {
    global.onlineUsers = new Map();
    io.on("connection", (socket) => {
        global.chatSocket = socket;
        socket.on("add-user", (userId) => {
            onlineUsers.set(userId, socket.id);
        });

        socket.on("send-msg", async (data) => {
            const sendUserSocket = onlineUsers.get(data.to);
            const conversationId = await chatService.addMessage(data);
            if (sendUserSocket) {
                socket.to(sendUserSocket).emit("receive-msg", data.message, data.type);
            }
        });
    });
}