const OneSignal = require('onesignal-node');
const UserModel = require('../models/user_model');

const ConversationsModel = require('../models/conversation_model');
const MessagesModel = require('../models/message_model');
const NotificationModel = require('../models/notification_model')
const NotificationType = require("../models/notification_model").NotificationType;



class NotificationService {
    static async sendNotificationFromMsg(message, conversationId) {
        const client = new OneSignal.Client(process.env.ONESIGNAL_APP_ID, process.env.ONESIGNAL_API_KEY);

        const receiver = await UserModel.findOne({ _id: message.receiverId }).select(['pushNotificationToken', 'mutedUsers', 'notificationsSettings', '_id']);
        const sender = await UserModel.findOne({ _id: message.senderId }).select(['name', '_id']);

        // check if receiver muted the sender
        if (receiver.mutedUsers.findIndex(mutedUser => mutedUser.toString() == message.senderId) != -1
            || (receiver.notificationsSettings && receiver.notificationsSettings.messages == false)) {
            return
        }
        const findQuery = { _id: conversationId }
        let conversation = await ConversationsModel.findOne(findQuery).populate({
            path: 'lastMessage',
            model: 'Message',
        }).populate({
            path: 'users',
            model: 'User',
            select: ['_id', 'name', 'avatar']
        })
        const msgsFindQuery = { conversationId: conversationId, receiverId: message.receiverId, isRead: { $ne: true } }

        const data = {
            ...conversation._doc,
            unReadMsgsCount: await MessagesModel.countDocuments(msgsFindQuery),
            isPinned: conversation.pinnedBy.findIndex(by => by == message.receiverId) != -1
        }

        const notification = {
            data: {
                type: 'Message',
                reference: conversationId,
                value: {
                    ...data
                }
            },
            headings: {
                en: sender.name
            },
            contents: {
                en: message.type == 'Text' ? message.message : message.type,
            },
            include_player_ids: [receiver.pushNotificationToken],
            android_channel_id: "5f5e0a98-82ff-40b3-85f4-c71ab19226dc"
        };
        try {
            const response = await client.createNotification(notification);
            console.log(response.body.id);
            NotificationModel.create({
                user: receiver._id,
                triggeredUser: sender._id,
                media: null,
                message: `${sender.name} sent you a message`,
                type: NotificationType.MESSAGE,
                createdTime: Date.now()
            });
        } catch (e) {
            if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
                console.log(e.statusCode);
                console.log(e.body);
            }
        }
    }

    static async videoProcessingNotification(userId, mediaData) {
        const client = new OneSignal.Client(process.env.ONESIGNAL_APP_ID, process.env.ONESIGNAL_API_KEY);
        const receiver = await UserModel.findOne({ _id: userId }).select(['pushNotificationToken']);
        // Video Uploaded Successfully!
        const notification = {
            data: {
                type: 'VideoStatus',
                reference: mediaData._id,
                value: {
                    id: mediaData._id,
                }
            },
            headings: {
                en: 'Video Posting'
            },
            contents: {
                en: 'Your video is now Posted!'
            },
            include_player_ids: [receiver.pushNotificationToken],
            android_channel_id: "5f5e0a98-82ff-40b3-85f4-c71ab19226dc"
        };
        console.log(notification)
        try {
            NotificationModel.create({
                user: userId,
                triggeredUser: null,
                media: mediaData._id,
                message: 'Your video is now Posted!',
                type: NotificationType.VIDEO_POSTED,
                createdTime: Date.now()
            });
            const response = await client.createNotification(notification);

        } catch (e) {
            if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
                console.log(e.statusCode);
                console.log(e.body);
            }
        }
    }

    static async sendNewFollowerNotification(currUserId, followedUserId) {
        const client = new OneSignal.Client(process.env.ONESIGNAL_APP_ID, process.env.ONESIGNAL_API_KEY);

        const currUser = await UserModel.findOne({ _id: currUserId }).select(['name', 'username', '_id']);
        const followedUser = await UserModel.findOne({ _id: followedUserId }).select(['name', 'pushNotificationToken']);

        const notification = {
            data: {
                type: 'NewFollower',
                reference: currUserId,
                value: {
                    ...currUser
                }
            },
            headings: {
                en: "New Follower!"
            },
            contents: {
                en: `${currUser.name} started following you!`
            },
            include_player_ids: [followedUser.pushNotificationToken],
            android_channel_id: "5f5e0a98-82ff-40b3-85f4-c71ab19226dc"
        };
        try {
            NotificationModel.create({
                user: followedUserId,
                triggeredUser: currUserId,
                media: null,
                message: `${currUser.name} started following you!`,
                type: NotificationType.FOLLOW,
                createdTime: Date.now()
            });
            const response = await client.createNotification(notification);
            console.log(response.body.id);
        } catch (e) {
            if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
                console.log(e.statusCode);
                console.log(e.body);
            }
        }
    }
    static async sendNewProfileVisitNotification(currUserId, targetUserId) {
        const client = new OneSignal.Client(process.env.ONESIGNAL_APP_ID, process.env.ONESIGNAL_API_KEY);

        const currUser = await UserModel.findOne({ _id: currUserId }).select(['name', 'username', '_id']);
        const targetUser = await UserModel.findOne({ _id: targetUserId }).select(['name', 'pushNotificationToken']);

        const notification = {
            data: {
                type: 'NewProfileVisit',
                reference: currUserId,
                value: {
                    ...currUser
                }
            },
            headings: {
                en: "New Profile Visit!"
            },
            contents: {
                en: `${currUser.name} visited your profile!`
            },
            include_player_ids: [targetUser.pushNotificationToken],
            android_channel_id: "5f5e0a98-82ff-40b3-85f4-c71ab19226dc"
        };
        try {
            const response = await client.createNotification(notification);
            console.log(response.body.id);
        } catch (e) {
            if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
                console.log(e.statusCode);
                console.log(e.body);
            }
        }
    }

    static async sendNewLikeNotification(currUserId, followedUserId, mediaId) {
        const client = new OneSignal.Client(process.env.ONESIGNAL_APP_ID, process.env.ONESIGNAL_API_KEY);

        const currUser = await UserModel.findOne({ _id: currUserId }).select(['name', 'username', '_id']);
        const followedUser = await UserModel.findOne({ _id: followedUserId }).select(['name', 'pushNotificationToken']);
        const notification = {
            data: {
                type: 'NewLike',
                reference: mediaId,
                value: {
                    ...currUser
                }
            },
            headings: {
                en: "New Like!"
            },
            contents: {
                en: `${currUser.name} Liked your video!`
            },
            include_player_ids: [followedUser.pushNotificationToken],
            android_channel_id: "5f5e0a98-82ff-40b3-85f4-c71ab19226dc"
        };
        try {
            console.log("Sending Notification", currUser)
            NotificationModel.create({
                user: followedUserId,
                triggeredUser: currUserId,
                media: mediaId,
                message: `${currUser.name} Liked your video!`,
                type: NotificationType.LIKE,
                createdTime: Date.now()
            });
            const response = await client.createNotification(notification);
            console.log(response.body.id);
        } catch (e) {
            if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
                console.log(e.statusCode);
                console.log(e.body);
            }
        }
    }

    static async sendCommentLikeNotification(currUserId, followedUserId, type, mediaId) {
        const client = new OneSignal.Client(process.env.ONESIGNAL_APP_ID, process.env.ONESIGNAL_API_KEY);

        const currUser = await UserModel.findOne({ _id: currUserId }).select(['name', 'username', '_id']);
        const followedUser = await UserModel.findOne({ _id: followedUserId }).select(['name', 'pushNotificationToken']);

        const notification = {
            data: {
                type: 'NewLike',
                reference: mediaId,
                value: {
                    ...currUser
                }
            },
            headings: {
                en: "New Like!"
            },
            contents: {
                en: `${currUser.name} Liked your ${type}!`
            },
            include_player_ids: [followedUser.pushNotificationToken],
            android_channel_id: "5f5e0a98-82ff-40b3-85f4-c71ab19226dc"
        };
        try {
            NotificationModel.create({
                user: followedUserId,
                triggeredUser: currUserId,
                media: null,
                message: `${currUser.name} Liked your ${type}!`,
                type: NotificationType.LIKE,
                createdTime: Date.now()
            });
            const response = await client.createNotification(notification);
            console.log(response.body.id);
        } catch (e) {
            if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
                console.log(e.statusCode);
                console.log(e.body);
            }
        }
    }

    static async sendNewCommentNotification(currUserId, followedUserId, mediaId) {
        const client = new OneSignal.Client(process.env.ONESIGNAL_APP_ID, process.env.ONESIGNAL_API_KEY);

        const currUser = await UserModel.findOne({ _id: currUserId }).select(['name', 'username', '_id']);
        const followedUser = await UserModel.findOne({ _id: followedUserId }).select(['name', 'pushNotificationToken']);

        const notification = {
            data: {
                type: 'NewComment',
                reference: mediaId,
                value: {
                    ...currUser
                }
            },
            headings: {
                en: "New Comment!"
            },
            contents: {
                en: `${currUser.name} commented on your video!`
            },
            include_player_ids: [followedUser.pushNotificationToken],
            android_channel_id: "5f5e0a98-82ff-40b3-85f4-c71ab19226dc"
        };
        try {
            NotificationModel.create({
                user: followedUserId,
                triggeredUser: currUserId,
                media: mediaId,
                message: `${currUser.name} commented on your video!`,
                type: NotificationType.COMMENT,
                createdTime: Date.now()
            });
            const response = await client.createNotification(notification);
            console.log(response.body.id);
        } catch (e) {
            if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
                console.log(e.statusCode);
                console.log(e.body);
            }
        }
    }

    static async sendNewReplyNotification(currUserId, followedUserId, mediaId) {
        const client = new OneSignal.Client(process.env.ONESIGNAL_APP_ID, process.env.ONESIGNAL_API_KEY);

        const currUser = await UserModel.findOne({ _id: currUserId }).select(['name', 'username', '_id']);
        const followedUser = await UserModel.findOne({ _id: followedUserId }).select(['name', 'pushNotificationToken']);

        const notification = {
            data: {
                type: 'NewReply',
                reference: mediaId,
                value: {
                    ...currUser
                }
            },
            headings: {
                en: "New Reply!"
            },
            contents: {
                en: `${currUser.name} replied on your comment!`
            },
            include_player_ids: [followedUser.pushNotificationToken],
            android_channel_id: "5f5e0a98-82ff-40b3-85f4-c71ab19226dc"
        };
        try {
            NotificationModel.create({
                user: followedUserId,
                triggeredUser: currUserId,
                media: null,
                message: `${currUser.name} replied on your comment!`,
                type: NotificationType.REPLY,
                createdTime: Date.now()
            });
            const response = await client.createNotification(notification);
            console.log(response.body.id);
        } catch (e) {
            if (e instanceof OneSignal.HTTPError) {
                // When status code of HTTP response is not 2xx, HTTPError is thrown.
                console.log(e.statusCode);
                console.log(e.body);
            }
        }
    }

}

module.exports = NotificationService;
