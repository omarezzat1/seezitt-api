const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

class SendGridService {
    static async sendEmailHtml(email, subject, html) {
        try {
            const msg = {
                to: email,
                from: "no-reply@seezitt.com",
                subject: subject,
                html: html,
            };

            await sgMail.send(msg);
        } catch (error) {
            console.log(error.response.body);
        }
    }
}

module.exports = SendGridService;
