const { v4: uuidv4 } = require('uuid');
const giftModel = require('../models/gift_model');
const mediaModel = require('../models/media_model');
const userModel = require('../models/user_model');
const constants = require("../utils/constants")
const coinTransactionModel = require("../models/coin_transaction_model");
const TransactionType = require("../models/coin_transaction_model").TransactionType;

async function getAll(paginationDto) {
    const { page, pageSize } = paginationDto;
    let pageNum = page ?? 1;
    let pageLimit = pageSize ?? constants.pageLimit;
    let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;
    return giftModel.find().skip(skip)
        .sort({
            createdTime: -1,
        })
        .limit(pageLimit);
}

async function getGiftById(id) {
    return giftModel.findById(id);
}

async function sendGift(userId, giftId, mediaId) {
    const media = await mediaModel.findOne({ _id: mediaId });
    if (!media) {
        throw new Error('Media not found');
    }

    const gift = await giftModel.findOne({ _id: giftId });
    if (!gift) {
        throw new Error('Gift not found');
    }

    const currUser = await userModel.findOne({ _id: userId });
    if (currUser.balance < gift.price) {
        throw new Error('Not enough balance');
    }

    currUser.balance -= gift.price;
    media.receivedGifts.push({
        userId,
        giftId
    });

    await coinTransactionModel.create({
        sourceUserId: userId,
        destinationUserId: media.user_id,
        transactionId: uuidv4(),
        amount: gift.price,
        type: TransactionType.GIFT,
        createdTime: Date.now(),
    });
    await currUser.save();
    await media.save();
}

async function createGift(name, price, description, imageUrl) {
    return giftModel.create({
        name,
        price,
        description,
        imageUrl
    });
}
async function markGiftAsSeen(giftId,mediaId){
    const media = await mediaModel.findById(mediaId);
    const targetGiftIndex = media.receivedGifts.findIndex(gift =>gift._id.toString() == giftId);
    media.receivedGifts[targetGiftIndex].isSeen=true;
    await media.save();
}

module.exports = {
    getAll,
    getGiftById,
    createGift,
    sendGift,
    markGiftAsSeen
};
