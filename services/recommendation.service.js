const axios = require('axios');


async function getVideosRecommendations(userId, retrain) {
    try {
        const query = `?user_id=${userId}&to_retrain=1`;
        const url = `${process.env.RECOMMENDATION_API_URL}/get-recommendations/${query}`;
        const response = await axios.get(url);
        return response.data.recs;
    } catch (error) {
        console.log('Failed to get recommendations', error);
        return [];
    }

}

async function getFollowingRecommendations(userId, retrain) {
    try {
        const query = `?user_id=${userId}&to_retrain=1`;
        const url = `${process.env.RECOMMENDATION_API_URL}/get-recommendations/${query}`;
        const response = await axios.get(url);
        return response.data.recs;
    } catch (error) {
        console.log('Failed to get following recommendations', error);
        return [];
    }

}

module.exports = {
    getVideosRecommendations,
    getFollowingRecommendations
};
