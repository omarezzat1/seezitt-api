let soundGallery = require("../models/sound_gallery_model");
let artistGallery = require("../models/sound_artist_model");

let Constants = require("../utils/constants");
const logger = require("../utils/logger");

async function getSoundGallery(paginationDto) {
    try {
        let pageNum = paginationDto.page ?? 1;
        let skip = pageNum > 0 ? (pageNum - 1) * Constants.pageLimit : 1;
        let soundGalleryData = await soundGallery.find({ active: true })
            .skip(skip).limit(Constants.pageLimit);
        return await Promise.all(soundGalleryData.map(async (soundContent, i) => {
            let artist = await artistGallery.findOne({ id: soundContent.artist_id })
            soundContent.artist = artist;
            return soundContent;
        }))
    } catch (error) {
        // logger.e(error);
        throw error
    }
};

module.exports = {
    getSoundGallery
};

