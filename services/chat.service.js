const constants = require("../utils/constants");
const logger = require("../utils/logger");

const UserFollowerModel = require("../models/user_followers_model");
const userModel = require("../models/user_model");
const ConversationsModel = require('../models/conversation_model');
const MessagesModel = require('../models/message_model');
const MediaModel = require('../models/media_model');
const NotificationService = require('../services/notifications.service')

async function addMessage(data) {
    try {
        let { from, to, message, conversationId, type } = data;
        if (!conversationId) {
            const conversationExists = await ConversationsModel.findOne({ users: { $all: [from, to] } })
            if (!conversationExists) {
                const conversation = await ConversationsModel.create({
                    users: [from, to],
                    createdTime: Date.now()
                })
                await conversation.save();
                conversationId = conversation._id.toString()
            }
        }
        const newMessage = await MessagesModel.create({
            message: message,
            senderId: from,
            receiverId: to,
            conversationId: conversationId,
            type: type ? type : 'Text',
            createdTime: Date.now()

        });
        await newMessage.save();
        const conversation = await ConversationsModel.findOne({ _id: newMessage.conversationId })
        conversation.lastMessage = newMessage._id;
        await conversation.save();
        await NotificationService.sendNotificationFromMsg(newMessage, conversation._id)
        return conversation._id;
    } catch (error) {
        throw error
    }
};

function performIntersection(arr1, arr2) {
    arr1 = arr1?.map((item) => item?.toString());
    arr2 = arr2?.map((item) => item?.toString());
    const intersectionResult = arr1?.filter((x) => arr2?.indexOf(x) !== -1);

    return intersectionResult.length != 0;
}

async function pinConversation(conversationId, userId) {
    try {
        const findQuery = { _id: conversationId };
        const conversations = await ConversationsModel.findOne(findQuery);
        const index = conversations.pinnedBy?.findIndex(
            (pinnedUser) => pinnedUser == userId
        );
        if (index != -1) {
            conversations.pinnedBy.splice(index, 1);
        } else {
            conversations.pinnedBy.push(userId);
        }
        await conversations.save();

    } catch (error) {
        logger.e(error);
        throw error;
    }
}

async function getMessages(paginationDto, conversationId, currUser) {
    try {
        const { page, pageSize } = paginationDto;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;

        const findQuery = { conversationId: conversationId, isDeleted: false };
        const messages = await MessagesModel.find(findQuery)
            .populate({
                path: "senderId",
                model: "User",
                select: ["_id", "name", "avatar", "username", "activityStatus"],
            })
            .populate({
                path: "receiverId",
                model: "User",
                select: ["_id", "name", "avatar", "username", "activityStatus"],
            })
            .skip(skip)
            .sort({
                createdTime: -1,
            })
            .limit(pageLimit);
        const msgsDto = await Promise.all(messages.map(async (msg) => {
            let thumbnail = null;
            if (msg.type == 'SeezittVideo') {
                const media = await MediaModel.findOne({ _id: msg.message }).select(['thumbnailUrl'])
                thumbnail = media.thumbnailUrl;
            }
            return {
                ...msg._doc,
                thumbnail: thumbnail
            }
        }))
        const idsToUpdate = messages
            .filter(
                (msg) => msg.receiverId._id.toString() == currUser && msg.isRead != true
            )
            .map((msg) => msg._id);
        await MessagesModel.updateMany(
            { _id: { $in: idsToUpdate } },
            { isRead: true }
        );
        const count = await MessagesModel.countDocuments(findQuery);
        return {
            data: msgsDto.reverse(),
            total: count,
        };

    } catch (error) {
        logger.e(error);
        throw error;
    }
}

async function getConversations(paginationDto, userId) {
    try {
        const { page, pageSize } = paginationDto;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;

        let currUser = await userModel
            .findOne({ _id: userId })
            .select(["mutedUsers", "blockedUsers"]);

        //Pinned conversations first
        const pinnedConversationsFindQuery = { pinnedBy: userId, isDeleted: false };
        const pinnedConversations = await ConversationsModel.find(
            pinnedConversationsFindQuery
        )
            .populate({
                path: "lastMessage",
                model: "Message",
            })
            .populate({
                path: "users",
                model: "User",
                select: ["_id", "name", "avatar", "username", "activityStatus"],
            })
            .sort({
                createdTime: -1,
            });
        const pinnedConversationsIds = pinnedConversations.map(
            (conversation) => conversation._id
        );
        const findQuery = {
            _id: { $nin: pinnedConversationsIds },
            users: userId,
            isDeleted: false,
        };

        let conversations = await ConversationsModel.find(findQuery)
            .populate({
                path: "lastMessage",
                model: "Message",
            })
            .populate({
                path: "users",
                model: "User",
                select: ["_id", "name", "avatar", "mutedUsers", "blockedUsers", "activityStatus"],
            })
            .skip(skip)
            .sort({
                createdTime: -1,
            })
            .limit(pageLimit);

        conversations = [...pinnedConversations, ...conversations];
        const msgsFindQuery = {
            conversationId: {
                $in: [
                    ...conversations.map((conversation) => conversation._id),
                    ...pinnedConversationsIds,
                ],
            },
            receiverId: userId,
            isRead: { $ne: true },
        };
        const messages = await MessagesModel.find(msgsFindQuery).select([
            "conversationId",
        ]);
        let readStatusCount = new Map();
        for (msg of messages) {
            let currentCount = readStatusCount.get(msg.conversationId.toString());
            if (currentCount == undefined) currentCount = 0;
            readStatusCount.set(msg.conversationId.toString(), currentCount + 1);
        }
        const count = await ConversationsModel.countDocuments(findQuery);
        const data = conversations.map((conversation) => {
            return {
                ...conversation._doc,
                unReadMsgsCount: readStatusCount.get(conversation._id.toString())
                    ? readStatusCount.get(conversation._id.toString())
                    : 0,
                isPinned:
                    pinnedConversationsIds.findIndex(
                        (pinnedConversation) => pinnedConversation == conversation._id
                    ) != -1,
                isMuted: currUser.mutedUsers
                    ? performIntersection(
                        currUser.mutedUsers,
                        conversation.users.map((user) => user.id)
                    )
                    : false,
                isBlocked: currUser.blockedUsers
                    ? performIntersection(
                        currUser.blockedUsers,
                        conversation.users.map((user) => user.id)
                    )
                    : false,
                isBlockedByOtherUser: false,
            };
        });
        const sorter = (a, b) => {
            if (a.isPinned && !b.isPinned) {
                return -1; // a should come first since it's pinned
            } else if (!a.isPinned && b.isPinned) {
                return 1; // b should come first since it's pinned
            } else {
                if (a.lastMessage._id > b.lastMessage._id) {
                    return -1; // a should come first since it has a later createdTime
                } else if (a.lastMessage._id < b.lastMessage._id) {
                    return 1; // b should come first since it has a later createdTime
                } else {
                    return 0; // they have the same createdTime and pinned status, so order doesn't matter
                }
            }
        };

        data.sort(sorter);
        return { data: data, total: count };
    } catch (error) {
        logger.e(error);
        throw error;
    }
}

async function search(paginationDto, currentUserId) {
    try {
        const { page, pageSize, searchQuery } = paginationDto;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;
        let currUser = await userModel
            .findOne({ _id: currentUserId })
            .select(["mutedUsers", "blockedUsers"]);

        const users = await userModel
            .find({
                isDeleted: false,
                name: { $regex: new RegExp(searchQuery, "i") },
            })
            .select(["_id", "name", "avatar", "username", "activityStatus"]);

        const userIds = users.map((user) => user._id);

        const findFollowersQuery = {
            followed_userID: currentUserId,
            isDeleted: false,
            follower_userID: { $in: userIds },
        };
        const myFollowers = await UserFollowerModel.find(findFollowersQuery);

        const findFollowingQuery = {
            followed_userID: {
                $in: myFollowers.map((follower) => follower.follower_userID),
            },
            isDeleted: false,
            follower_userID: currentUserId,
        };
        let myFollowing = await UserFollowerModel.find(findFollowingQuery);
        myFollowing = myFollowing.map((following) =>
            following.followed_userID.toString()
        );
        const result = users.filter((user) =>
            myFollowing.includes(user._id.toString())
        );
        const data = [];
        for (const res of result) {
            const findQuery = {
                users: { $all: [res._id.toString(), currentUserId] },
            };
            let conversation = await ConversationsModel.findOne(findQuery)
                .populate({
                    path: "lastMessage",
                    model: "Message",
                })
                .populate({
                    path: "users",
                    model: "User",
                    select: ["_id", "name", "avatar", "username", "activityStatus"],
                });
            const msgsFindQuery = {
                conversationId: conversation?._id ? conversation._id : null,
                receiverId: currentUserId,
                isRead: { $ne: true },
            };

            if (conversation) {
                conversation = {
                    ...conversation._doc,
                    unReadMsgsCount: await MessagesModel.countDocuments(msgsFindQuery),
                    isPinned: false, //conversation.pinnedBy.findIndex(by => by == message?.receiverId) != -1
                    isMuted: currUser.mutedUsers
                        ? performIntersection(
                            currUser.mutedUsers,
                            conversation.users.map((user) => user.id)
                        )
                        : false,
                    isBlocked: currUser.blockedUsers
                        ? performIntersection(
                            currUser.blockedUsers,
                            conversation.users.map((user) => user.id)
                        )
                        : false,
                    isBlockedByOtherUser: false,
                };
            }
            data.push({
                ...res._doc,
                conversation: { ...conversation },
            });
        }
        return data;
    } catch (error) {
        throw error;
    }
}

async function updateActivityStatus(userId, isActive) {
    const user = await userModel.findOne({ _id: userId });
    user.activityStatus.isActive = isActive;
    await user.save();
}


module.exports = {
    addMessage,
    pinConversation,
    getMessages,
    getConversations,
    search,
    updateActivityStatus,
    performIntersection
};
