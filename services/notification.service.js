const constants = require("../utils/constants");
const notificationModel = require('../models/notification_model')

async function getNotifications(currentUserId) {
    try {
        const query = {
            user: currentUserId,
            isDeleted: false,
        };
        const count = await notificationModel.countDocuments(query);

        const notifications = await notificationModel.find(query)
            .populate({
                path: "user",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            }).populate({
                path: "triggeredUser",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            }).populate({
                path: "media",
                model: "Media",
                select: ["_id", "thumbnailUrl",],
            }).sort({
                createdTime: -1,
            })


        const ids = notifications.map((notification) => notification._id);

        await notificationModel.updateMany(
            { _id: { $in: ids } },
            { $set: { isRead: true } }
        );

        if (!notifications.length) {
            return { data: [] }
        }
        return { data: notifications, count };
    } catch (error) {
        console.log(error)
        throw error;
    }
}


module.exports = {
    getNotifications
};
