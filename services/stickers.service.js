

const stickersModel = require("../models/stickers_model");

const Constants = require("../utils/constants");
const logger = require("../utils/logger");


async function getStickers(query) {
    try {
        const { sticker_type, name, page } = query;
        let pageNum = page ?? 1;
        let skip = pageNum > 0 ? (pageNum - 1) * Constants.pageLimit : 1;

        let searchStickers = { type: sticker_type, active: true };
        if (name != null && name.trim() !== "") {
            searchStickers = {
                type: sticker_type,
                name: { $regex: name, $options: "i" },
                active: true,
            };
        }

        let stickers = await stickersModel
            .find(searchStickers)
            .sort({ name: 1 })
            .skip(skip)
            .limit(Constants.pageLimit);

        return stickers
    } catch (error) {
        logger.e(error);
        throw error;
    }
}

module.exports = {
    getStickers,
};
