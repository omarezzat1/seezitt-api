const appVersionModel = require("../models/app_version_model");

async function checkAppVersion(version) {
    try {
        console.log(version)
        const versionStatus = await appVersionModel.findOne({ version: version })
        return versionStatus;
    } catch (error) {
        throw error;
    }
}


module.exports = {
    checkAppVersion
}