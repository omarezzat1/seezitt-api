const constants = require("../utils/constants");

const UserFollowerModel = require("../models/user_followers_model");
const userModel = require("../models/user_model");
const MediaModel = require("../models/media_model");
const MediaComments = require("../models/media_comments_model");
const HashtagModel = require("../models/hashtag_model");
const MediaCategories = require("../models/media_categories_model");
const SearchHistory = require("../models/search_history_model");

async function searchUsers(paginationDto, currentUserId) {
    try {
        const { page, pageSize, searchQuery } = paginationDto;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;

        const usersDbQuery = {
            _id: { $ne: currentUserId }, isDeleted: false, $or: [{ name: { $regex: new RegExp(searchQuery, "i") } },
            { email: { $regex: new RegExp(searchQuery, "i") } },
            { username: { $regex: new RegExp(searchQuery, "i") } }]
        }
        const users = await userModel.find(usersDbQuery).limit(constants.pageLimit).select(['_id', 'name', 'avatar', 'username'])
        let usersFollowing = await UserFollowerModel.find({
            follower_userID: currentUserId,
            followed_userID: { $in: users.map(user => user._id) },
            isDeleted: false,
        }).select("followed_userID");
        if (usersFollowing.length) {
            usersFollowing = usersFollowing.map((userFollowing) =>
                userFollowing.followed_userID.toString()
            );
        }
        if (!users.length) {
            return { data: [] }
        }
        const responseDto = await Promise.all(users.map(async user => {
            const userFollowersCount = await UserFollowerModel.countDocuments({
                followed_userID: user._id,
                isDeleted: false,
            });
            return {
                ...user._doc,
                isFollowed: usersFollowing.includes(user._id.toString()),
                numberOfFollowers: userFollowersCount,

            }
        }));
        return { data: responseDto };
    } catch (error) {
        throw error;
    }
}

async function searchVideos(paginationDto, currentUserId) {
    try {
        const { page, pageSize, searchQuery } = paginationDto;
        const userId = currentUserId;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;

        const mediaDbQuery = {
            status: "Processed",
            isDeleted: false,
            is_story: { $ne: true },
            isOnlyMe: { $ne: true },
            $or: [
                { description: { $regex: new RegExp(searchQuery, "i") } },
                { categories: { $regex: new RegExp(searchQuery, "i") } }]
        }


        const mediaContents = await MediaModel.find(mediaDbQuery)
            .populate({
                path: "user_id",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            })
            .populate({
                path: "receivedGifts.giftId",
                model: "Gift",
                select: ["_id", "name", "imageUrl", "price"],
            })
            .populate({
                path: "receivedGifts.userId",
                model: "User",
                select: ["_id", "name", "avatar", "username"]
            })
            .sort({
                _id: -1,
            })
            .limit(constants.pageLimit);

        const mediaUsers = mediaContents.map((content) => content.user_id._id);
        let usersFollowing = await UserFollowerModel.find({
            follower_userID: userId,
            followed_userID: { $in: mediaUsers },
            isDeleted: false,
        }).select("follower_userID");

        if (usersFollowing.length) {
            usersFollowing = usersFollowing.map((userFollowing) =>
                userFollowing.follower_userID.toString()
            );
        }
        const mediaContentsIds = mediaContents.map((mediaContent) =>
            mediaContent._id.toString()
        );
        const mediaContentsResponseData = [];
        await Promise.all(
            mediaContents.map(async (mediaContent) => {
                let mediaCategory = null;
                if (mediaContent.category !== "") {
                    try {
                        mediaCategory = await MediaCategories.findOne({
                            _id: mediaContent.category,
                        });
                    } catch (e) { }
                }

                let comments = await MediaComments.find({
                    _id: { $in: mediaContent.comments },
                })
                    .populate({
                        path: "user_id",
                        model: "User",
                        select: ["_id", "name", "avatar", "username", "isVerified"],
                    })
                    .populate({
                        path: "replies.user_id",
                        model: "User",
                        select: ["_id", "name", "avatar", "username", "isVerified"],
                    });

                comments = comments.map((comment) => ({
                    id: comment._id,
                    user: comment.user_id,
                    comment: comment.comment,
                    likes: comment.likes.length,
                    isLiked: comment.likes.includes(userId),
                    createdTime: comment.createdTime,
                    replies: comment.replies?.map((reply) => ({
                        id: reply.id,
                        user: reply.user_id,
                        reply: reply.reply,
                        likes: reply.likes.length,
                        isLiked: reply.likes.includes(userId),
                        createdTime: reply.createdTime,
                    })),
                }));
                const mediaContentResponse = {
                    mediaId: mediaContent._id.toString(),
                    createdTime: mediaContent.createdTime,
                    user: {
                        _id: mediaContent.user_id._id,
                        name: mediaContent.user_id.name,
                        avatar: mediaContent.user_id.avatar,
                        username: mediaContent.user_id.username,
                        isFollowed: usersFollowing.includes(userId),
                    },
                    likes: mediaContent.likes.length,
                    shares: mediaContent.shares ? mediaContent.shares.length : 0,
                    views: mediaContent.watched_users
                        ? mediaContent.watched_users.length
                        : 0,
                    description: mediaContent.description,
                    linkedFiles: mediaContent.linked_files,
                    hlsUrl: mediaContent.hls_url,
                    reducedVideoUrl: mediaContent.reducedVideoUrl,
                    reducedVideoHlsUrl: mediaContent.reducedVideoHlsUrl,
                    shortVideoUrl: mediaContent.shortVideoUrl,
                    shortVideoHlsUrl: mediaContent.shortVideoHlsUrl,
                    privacyOptions: mediaContent.privacyOptions,
                    thumbnailUrl: mediaContent.thumbnailUrl,
                    thumbnail: mediaContent.thumbnail,
                    originalUrl: mediaContent.originalUrl,
                    comments: comments,
                    isLiked: mediaContent.likes.includes(userId),
                    category: mediaCategory != null ? mediaCategory.name : "",
                    type: mediaContent.type,
                    privacyOptions: mediaContent.privacyOptions,
                    receivedGifts: mediaContent.receivedGifts,

                };

                mediaContentsResponseData.push(mediaContentResponse);
            })
        );
        const sortedMediaContentsResponseData = mediaContentsResponseData.sort((a, b) => {
            if (a.mediaId > b.mediaId) return -1;
            if (a.mediaId < b.mediaId) return 1;
            return 0;
        });
        addToSearchHistory(searchQuery);
        return { data: sortedMediaContentsResponseData };
    } catch (error) {
        throw error;
    }
}

async function searchHashtags(paginationDto, currentUserId) {
    try {
        const { page, pageSize, searchQuery } = paginationDto;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;

        const mediaDbQuery = {
            isDeleted: false,
            active: true,
            $or: [
                { name: { $regex: new RegExp(searchQuery, "i") } }]
        }

        const hashTags = await HashtagModel.find(mediaDbQuery)
        return { data: hashTags };
    } catch (error) {
        throw error;
    }
}

async function getTrendingHashtags() {
    try {
        const trendingHashtags = await HashtagModel.find()
            .sort({ views: -1 })
            .limit(5)

        return { data: trendingHashtags };
    } catch (error) {
        throw error;
    }
}

async function addToSearchHistory(searchQuery) {
    const searchHistory = await SearchHistory.findOne({
        query: searchQuery,
    });

    if (searchHistory) {
        await SearchHistory.updateOne({
            query: searchQuery,
        }, {
            $inc: {
                count: 1
            }
        });
    } else {
        await SearchHistory.create({
            query: searchQuery,
            count: 1,
            createdTime: Date.now()
        });
    }
}

async function suggestedSearch(paginationDto) {
    try {
        const { page, pageSize, searchQuery } = paginationDto;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;

        const searchHistory = await SearchHistory.find({
            query: {
                $regex: new RegExp("^" + searchQuery, "i")
            }
        })
            .sort({
                count: -1
            })
            .limit(pageLimit)
            .skip(skip);


        return searchHistory;
    } catch (error) {
        throw error;
    }
}

async function getTopVideosByWatchedUsers() {
    try {
        const topVideos = await MediaModel.find()
            .sort({ 'watched_users.length': -1 }) // Sort by the length of watched_users array in descending order
            .limit(5) // Limit the result to 5 videos
            .exec();

        return { data: topVideos };
    } catch (error) {
        throw error;
    }
}


module.exports = {
    searchUsers,
    searchVideos,
    searchHashtags,
    suggestedSearch,
    getTrendingHashtags,
    getTopVideosByWatchedUsers
};
