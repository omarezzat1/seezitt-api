module.exports = {

  pageLimit: 10,

  userRole: {
    Admin: 1,
    Super: 2,
    User: 3,
  },

  StickersType: {
    Sticker: 1,
    Gif: 2,
    Emoji: 3,
  },

  uploadFile: './uploads/',

  horizontalResolutions: {
    '144w': 256,
    '144h': 144,
    '240w': 352, //SD
    '240h': 240,
    '360w': 480,
    '360h': 360,
    '480w': 858,
    '480h': 480,
    '720w': 1280, //Half HD
    '720h': 720,
    '1080w': 1920, //Full HD
    '1080h': 1080,
    '2160w': 3860, //Ultra-HD
    '2160h': 2160,
  },

  verticalResolutions: {
    '144h': 256,
    '144w': 144,
    '240h': 352, //SD
    '240w': 240,
    '360h': 480,
    '360w': 360,
    '480h': 858,
    '480w': 480,
    '720h': 1280, //Half HD
    '720w': 720,
    '1080h': 1920, //Full HD
    '1080w': 1080,
    '2160h': 3860, //Ultra-HD
    '2160w': 2160,
  },

  allowedImages: {
    jpg: 'jpg',
    jpeg: 'jpeg',
    png: 'png',
    heic: 'heic'
  },

  allowedFiles: {
    pdf: 'pdf',
    xlsx: 'xlsx',
    txt: 'txt',
    csv: 'csv',
  },

  allowedVideos: {
    gif: 'gif',
    avi: 'avi',
    wmv: 'wmv',
    webm: 'webm',
    mkv: 'mkv',
    mp4: 'mp4',
    hevc: 'HEVC'
  },

  contentType: {
    imagePNG: 'image/png',
    imageJPG: 'image/jpg',
    imageJPEG: 'image/jpeg',
    file: 'application/octet-stream',
  },

  nameMinLength: 2,
  nameMaxLength: 20,
  avatarMaxSize: 10000, //KB

  usernameMinLength: 2,
  usernameMaxLength: 64,

};
