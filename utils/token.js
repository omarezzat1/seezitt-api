const jwt = require('jsonwebtoken');

class TokenCredentials {
  static generateToken(userId, userEmail) {
    return jwt.sign({ id: userId, email: userEmail }, process.env.JWT_SECRET);
  }
}

module.exports = TokenCredentials;
