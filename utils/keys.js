class Keys {
    // DB table keys
    static users = 'users';
    static sound_gallery = 'sound_gallery';
    static sound_artist = 'sound_artist';

    //table columns
    static id = '_id';
    static artist = 'artist';
    static artist_id = 'artist_id';
    static active = 'active';
    static artist_name = 'artist_name';

    static development = 'development';
    static production = 'production';
    static type = 'type';

    static jwt = 'jwt';

    static horizontal = "horizontal";
    static vertical = "vertical";

    static mediaVideo = "video";
    static mediaImage = "image";

}
module.exports = Keys;
