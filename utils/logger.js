const log4js = require('log4js');
const keys = require('./keys');

log4js.configure({
  appenders: { actiontake1: { type: 'file', filename: 'actiontake1.log' }, console: { type: 'console' } },
  categories: { default: { appenders: ['actiontake1', 'console'], level: 'debug' } }
});
const logger = log4js.getLogger('actiontake1');
logger.level = 'debug';

class Logger {

  static debugData(data) {
    return process.env.NODE_ENV === keys.development ? data : {};
  }

  static d(message) {
    logger.debug(message);
  }

  static i(message) {
    logger.info(message);
  }

  static dWithParams(message, params) {
    logger.info(params);
    logger.debug(message);
  }

  static dWithObj(message, obj) {
    logger.info(obj);
    logger.debug(message);
  }

  static log(req, res, message) {
    logger.info(req);
    logger.info(res);
    logger.info(message);
  }

  static e(error) {
    logger.error(error);
  }

  static error(req, error) {
    logger.error(`Error: ${req.method} ${req.originalUrl} \n Error Details: ${error}\n${error.stack}`);
  }
}

module.exports = Logger;
