const MediaInteractionsLogs = require('../models/media_interactions_logs_model');

async function logMediaInteraction(userId, mediaId, interactionType,) {
    const interactionLog = await MediaInteractionsLogs.create({
        userId: userId,
        mediaId: mediaId,
        interactionType: interactionType,
    })
    await interactionLog.save();
}

async function deleteMediaInteractions(mediaId) {
    await MediaInteractionsLogs.updateMany(
        { mediaId: mediaId },
        { isDeleted: true }
    );
}

module.exports = {
    logMediaInteraction,
    deleteMediaInteractions
}