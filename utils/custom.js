const logger = require('../utils/logger');

module.exports = function () {
  this.randomString = (length) => {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  this.checkIfImageFile = (imageFile) => {
    try {
      const magicNumbers = {
        jpg: 'ffd8ffe0',
        jpg2: 'ffd8ffed',
        jpg3: 'ffd8ffe1',
        png: '89504e47',
        gif: '47494638'
      }; //Every Image file begins with
      const magicNumberInBody = Buffer.from(imageFile.data).toString('hex', 0, 4);
      logger.i(`imageFile hex ${magicNumberInBody}`);
      if (magicNumberInBody === magicNumbers.jpg || magicNumberInBody === magicNumbers.png || magicNumberInBody === magicNumbers.gif
        || magicNumberInBody === magicNumbers.jpg2 || magicNumberInBody === magicNumbers.jpg3) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }

  this.getFileExtension = (filename) => {
    const i = filename.lastIndexOf('.');
    return (i < 0) ? '' : filename.substr(i).substr(1).toLowerCase();
  }

  this.removeNonAlphanumericChars = (text) => {
    return text.replace(/[^0-9a-z\s]/gi, '').replace(/\s\s+/g, ' ').trim();
  }

}
