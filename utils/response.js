class Response {
  static successResponse(res, message, data) {
    res.status(200).json({
      status: 200,
      message: typeof message !== 'undefined' ? message : 'Success',
      data: typeof data !== 'undefined' ? data : {}
    });
  }

  static badReqResponse(res, message, data) {
    res.status(400).json({
      status: 400,
      message: typeof message !== 'undefined' ? message : 'Bad Request',
      data: data != null ? data : {}
    });
  }

  static notFoundResponse(res, message, data) {
    res.status(404).json({
      status: 404,
      message: typeof message !== 'undefined' ? message : 'Bad Request',
      data: data != null ? data : {}
    });
  }

  static forbiddenResponse(res, message, data) {
    res.status(403).json({
      status: 403,
      message: typeof message !== 'undefined' ? message : 'Forbidden',
      data: typeof data !== 'undefined' ? data : {}
    });
  }

  static unauthorizedReqResponse(res, message, data) {
    res.status(401).json({
      status: 401,
      message: typeof message !== 'undefined' ? message : 'Unauthorized Request',
      data: data != null ? data : {}
    });
  }

  static serverErrorResponse(res, message, data) {
    res.status(500).json({
      status: 500,
      message: typeof message !== 'undefined' ? message : 'Internal Server Error',
      data: typeof data !== 'undefined' ? data : {}
    });
  }
}

module.exports = Response;
