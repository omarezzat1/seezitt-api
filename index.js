const app = require('./app');
const port = process.env.PORT || 3000;
const socket = require("socket.io");

// app.listen(port, () => {
//   /* eslint-disable no-console */
//   console.log(`Listening: http://localhost:${port}`);
//   /* eslint-enable no-console */
// });


const server = app.listen(port, () =>
  console.log(`Listening: http://localhost:${port}`)
);
const io = socket(server, {
  cors: {
    origin: "*",
    credentials: true,
  },
});

require('./sockets/events.socket')(io);

