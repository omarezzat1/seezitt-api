const express = require("express");
const router = express.Router();
const audioController = require("../controllers/audio.controller");


router.get("/sound-gallery", audioController.getSoundGallery);

module.exports = router;
