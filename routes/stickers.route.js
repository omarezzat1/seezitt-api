const express = require("express");
const router = express.Router();
const stickersController = require("../controllers/stickers.controller");

router.get("/", stickersController.getStickers);

module.exports = router;
