const express = require("express");
const router = express.Router();
const migrationController = require("../controllers/migration.controller");


router.post("/demo-data", migrationController.addDemoData);

router.post("/usernames", migrationController.migrateUsernames);

router.post("/cleanup-logs", migrationController.mediaInteractionsLogsCleanUp);

router.post("/migrate-videos-links", migrationController.migrateVideosLinks);

module.exports = router;
