const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');
const mediaContentController = require("../controllers/media-content.controller");

router.post("/", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.addMediaUpgraded);

router.patch("/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.updateMediaContent);

router.delete("/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.deleteMediaContent);

// router.post("/", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.addMediaUpgraded);

router.get("/stories/feed", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getStoriesFeed);

router.patch("/status/:id", mediaContentController.updateByEncoder);

router.get("/videos/feed", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getVideoFeedV2);

router.get("/videos/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getMediaContent);

router.post("/like/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.likeMediaContent);

router.post("/comment/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.commentOnMediaContent);

router.post("/like/comment/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.likeComment);

router.get("/reports/reasons", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getReportingReasons);

router.post("/reports/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.reportMediaContent);

router.get("/reports", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getUserReports);

router.post("/share/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.shareMediaContent);

router.post("/mark-as-started-watching/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.markMediaAsWatched);

router.post("/mark-as-watched-till-end/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.markMediaAsWatchedTillEnd);

router.patch("/mark-video-as-unsafe/:id", mediaContentController.markVideoAsUnSafe);

router.patch("/pin-video/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.pinVideo);

router.post("/collections/:id", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.saveIntoCollection);

router.get("/collections", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getMediaCollection);

router.get("/categories", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getMediaCategories);

router.post("/attach-media", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.uploadMediaFile);

router.get("/stories/active-stories/:userId", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getActiveUserStoriesById);

router.get("/stories/active-stories", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getActiveCurrentUserStories);

router.get("/stories", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getAllCurrentUserStories);

router.get("/stories/highlights", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getCurrentUserHighlights);

router.get("/stories/highlights/:userId", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.getUserHighlights);

router.post("/stories/highlights", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.createHighlight);

router.patch("/stories/highlights", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.updateHighlight);

router.delete("/stories/highlights/:highlightId", passport.authenticate(Keys.jwt, { session: false }), mediaContentController.deleteHighlight);

module.exports = router;
