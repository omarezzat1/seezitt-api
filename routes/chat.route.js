const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');


const chatController = require("../controllers/chat.controller");

router.get("/conversation", passport.authenticate(Keys.jwt, { session: false }), chatController.getConversations);

router.get("/messages/search", passport.authenticate(Keys.jwt, { session: false }), chatController.search);

router.post("/conversation/pin/:conversationId", passport.authenticate(Keys.jwt, { session: false }), chatController.pinConversation);

router.get("/messages/:conversationId", passport.authenticate(Keys.jwt, { session: false }), chatController.getMessages);

router.post("/messages", passport.authenticate(Keys.jwt, { session: false }), chatController.sendMessage);

router.patch("/activity-status", passport.authenticate(Keys.jwt, { session: false }), chatController.updateActivityStatus);



module.exports = router;
