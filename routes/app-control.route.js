const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');

const appControlController = require("../controllers/app-cotrol.controller");


router.get("/check-version/:version", appControlController.checkCurrentAppVersion);


module.exports = router;
