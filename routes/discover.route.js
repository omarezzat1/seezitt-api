const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');

const discoverController = require("../controllers/discover.controller");


router.get("/search", passport.authenticate(Keys.jwt, { session: false }), discoverController.search);

router.get("/suggested-search", passport.authenticate(Keys.jwt, { session: false }), discoverController.suggestedSearch);

router.get("/trending/hashtags", passport.authenticate(Keys.jwt, { session: false }), discoverController.getTrendingHashtags);

router.get("/trending/videos", passport.authenticate(Keys.jwt, { session: false }), discoverController.getTrendingVideos);


module.exports = router;
