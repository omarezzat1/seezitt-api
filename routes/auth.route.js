const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');

const authController = require("../controllers/auth.controller");

router.post("/sign-up", authController.signUp);

router.post("/sign-in", authController.signIn);

router.patch("/password/set-new", authController.setNewPassword);

router.patch("/notification-token", passport.authenticate(Keys.jwt, { session: false }), authController.updateNotificationToken);

router.patch("/password", passport.authenticate(Keys.jwt, { session: false }), authController.changePassword);

router.get("/refresh-token", authController.getRefreshToken);

router.post("/password/forgot", authController.requestResetPassword);

router.patch("/media-categories", passport.authenticate(Keys.jwt, { session: false }), authController.updateUserMediaCategories);

module.exports = router;
