const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');

const notificationController = require("../controllers/notification.controller");


router.get("/", passport.authenticate(Keys.jwt, { session: false }), notificationController.getNotifications);


module.exports = router;
