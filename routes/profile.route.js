const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');

const profileController = require("../controllers/profile.controller");


router.get("/visitors", passport.authenticate(Keys.jwt, { session: false }), profileController.getUserProfileVisitors);

router.get("/collection", passport.authenticate(Keys.jwt, { session: false }), profileController.userCollections);

router.get("/", passport.authenticate(Keys.jwt, { session: false }), profileController.currentUserData);

router.get("/suggested-users", passport.authenticate(Keys.jwt, { session: false }), profileController.getSuggestedUsers);

router.get("/search-users", passport.authenticate(Keys.jwt, { session: false }), profileController.searchUsers);

router.delete("/", passport.authenticate(Keys.jwt, { session: false }), profileController.deleteUser);

router.get("/delete-account-reasons", passport.authenticate(Keys.jwt, { session: false }), profileController.getDeleteAccountReasons);

router.get("/blocked-users", passport.authenticate(Keys.jwt, { session: false }), profileController.getBlockedUsers);

router.get("/privacy-settings", passport.authenticate(Keys.jwt, { session: false }), profileController.getPrivacySettings);

router.get("/notifications-settings", passport.authenticate(Keys.jwt, { session: false }), profileController.getNotificationsSettings);

router.patch("/privacy-settings", passport.authenticate(Keys.jwt, { session: false }), profileController.updatePrivacySettings);

router.patch("/notifications-settings", passport.authenticate(Keys.jwt, { session: false }), profileController.updateNotificationsSettings);

router.get("/:id", passport.authenticate(Keys.jwt, { session: false }), profileController.getUserData);

router.post("/follow/:id", passport.authenticate(Keys.jwt, { session: false }), profileController.followUser);

router.get("/:id/following", passport.authenticate(Keys.jwt, { session: false }), profileController.getUserFollowing);

router.get("/:id/followers", passport.authenticate(Keys.jwt, { session: false }), profileController.getUserFollowers);

router.get("/:id/videos", passport.authenticate(Keys.jwt, { session: false }), profileController.getUserVideos);

router.post("/:id/mute", passport.authenticate(Keys.jwt, { session: false }), profileController.muteUser);

router.post("/:id/block", passport.authenticate(Keys.jwt, { session: false }), profileController.blockUser);

router.get("/username/validate/:username", profileController.validateUsername);

router.get("/:id/liked-videos", passport.authenticate(Keys.jwt, { session: false }), profileController.getLikedVideos);

router.patch("/", passport.authenticate(Keys.jwt, { session: false }), profileController.editUserProfile);


module.exports = router;
