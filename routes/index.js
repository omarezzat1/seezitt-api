const express = require('express');
const authRouter = require('./auth.route');
const audioRouter = require('./audio.route');
const profileRouter = require('./profile.route');
const chatRouter = require('./chat.route');
const stickersRouter = require('./stickers.route');
const mediaContentRouter = require('./media-content.route');
const migrationRouter = require('./migration.route');
const discoverRouter = require('./discover.route');
const appControlRouter = require('./app-control.route');
const giftRouter = require('./gift.route');
const paymentRouter = require('./payment.route');
const notificationRouter = require('./notification.route');
const router = express.Router();


router.use('/auth', authRouter);

router.use('/audio', audioRouter);

router.use('/migration', migrationRouter);

router.use('/profile', profileRouter);

router.use('/profile', profileRouter);

router.use('/chat', chatRouter);

router.use('/stickers', stickersRouter);

router.use('/media-content', mediaContentRouter);

router.use('/discover', discoverRouter);

router.use('/app-control', appControlRouter);

router.use('/gift', giftRouter);

router.use('/payment', paymentRouter);

router.use('/notification', notificationRouter);

module.exports = router;
