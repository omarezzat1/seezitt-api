const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');

const paymentController = require("../controllers/payment.controller");


router.post("/coins", passport.authenticate(Keys.jwt, { session: false }), paymentController.buyCoins);

router.get("/coins/:amount", passport.authenticate(Keys.jwt, { session: false }), paymentController.buyCoinsV2);


router.get("/paypal/success", paymentController.successCallback);


module.exports = router;
