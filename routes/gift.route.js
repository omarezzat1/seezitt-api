const express = require("express");
const router = express.Router();
const passport = require('passport');
const Keys = require('../utils/keys');

const giftController = require("../controllers/gift.controller");

router.post("/", passport.authenticate(Keys.jwt, { session: false }), giftController.createGift);

router.post("/send", passport.authenticate(Keys.jwt, { session: false }), giftController.sendGift);

router.get("/", passport.authenticate(Keys.jwt, { session: false }), giftController.getAll);

router.get("/:id", passport.authenticate(Keys.jwt, { session: false }), giftController.getGiftById);

router.post("/mark-as-seen", passport.authenticate(Keys.jwt, { session: false }), giftController.markGiftAsSeen);




module.exports = router;
