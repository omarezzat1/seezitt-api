const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const fileUpload = require('express-fileupload');
require('dotenv').config();
require('./auth/passport');
const path = require('path');

const app = express();

const middlewares = require('./middlewares');
const routes = require('./routes/index');
const authRoutes = require('./routes/auth.route');
// const home = require('./home');

app.use(fileUpload({
  createParentPath: true
}));

// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '2048mb'}));
app.use(
  session({
    secret: 'session_secret',
    resave: true,
    saveUninitialized: true,
  })
);
app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.options('*', cors());
app.use(express.json());

app.set('view engine', 'ejs');
app.use(express.static(path.resolve(__dirname + '/public')));
app.set('views', __dirname + '/views');

mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true });
mongoose.connection.on('connected', () => {
  console.log('MONGOOSE CONNECTED with url: ', process.env.DATABASE_URL);
});
mongoose.connection.on('error', (e) => {
  console.log('Mongoose Error connecting:', e);
});

app.get('/api/public/.well-known/assetlinks.json', (req, res) => {
  res.sendfile(path.join(__dirname, '/public/.well-known/assetlinks.json'));
});

app.get('/api/public/.well-known/apple-app-site-association', (req, res) => {
  res.sendfile(path.join(__dirname, '/public/.well-known/apple-app-site-association.uu'));
});

app.get('/', (req, res) => {
  res.json({
    message: 'ActionTake1 is here 🌏'
  });
});

// app.use('/h', home);

app.use('/api', routes);

// app.use('/auth', authRoutes);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
