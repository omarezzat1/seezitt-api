const passport = require('passport');
const passportJwt = require('passport-jwt');
// const FacebookStrategy = require('passport-facebook').Strategy;
// const GoogleStrategy = require('passport-google-oauth20').Strategy;
// const InstagramStrategy = require('passport-instagram').Strategy;
// const TwitterStrategy = require('passport-twitter').Strategy;
// const AppleStrategy = require('passport-apple').Strategy;

const StrategyJwt = passportJwt.Strategy;
const { ExtractJwt } = passportJwt;
const config = require('../configs/config');
const User = require('../models/user_model');
const logger = require('../utils/logger');

passport.use(
  new StrategyJwt(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), // Bearer Token
      secretOrKey: process.env.JWT_SECRET,
    },
    (jwtPayload, done) => {
      User.findOne({ _id: jwtPayload.id, isDeleted: false })
        .then((user) => done(null, user))
        .catch((err) => {
          logger.e(err);
          done(null, false);
        });
    }
  )
);

// passport.use(
//   new GoogleStrategy(
//     {
//       clientID: config.auth.google.clientID,
//       clientSecret: config.auth.google.clientSecret,
//       callbackURL: config.auth.google.callbackURL
//     },
//     (async (accessToken, refreshToken, profile, done) => {
//       const email = profile.emails[0].value;
//       const user = await User.findOne({ socialId: profile.id, isDeleted: false });
//       if (user) {
//         // already registered then return (login)
//         return done(null, user);
//       }
//       // register new user
//       const newUser = new User({
//         email, registerType: 'Google', socialId: profile.id, name: profile.displayName
//       });
//       await newUser.save();
//       return done(null, newUser);
//     })
//   )
// );

// passport.use(
//   new FacebookStrategy(
//     {
//       clientID: config.auth.facebook.clientID,
//       clientSecret: config.auth.facebook.clientSecret,
//       callbackURL: config.auth.facebook.callbackURL
//     },
//     (async (accessToken, refreshToken, profile, done) => {
//       const user = await User.findOne({ socialId: profile.id, isDeleted: false });
//       if (user) {
//         // already registered then return (login)
//         return done(null, user);
//       }
//       // register new user
//       const newUser = new User({
//         registerType: 'Facebook', socialId: profile.id, name: profile.displayName
//       });
//       await newUser.save();
//       return done(null, newUser);
//     })
//   )
// );

// passport.serializeUser((user, done) => {
//   logger.i('serializeUser is here');
//   done(null, user);
// });

// passport.deserializeUser((user, done) => {
//   logger.i(`deserializeUser is here ${user}`);
//   done(null, user);
// });

// passport.use(
//     new InstagramStrategy(
//         {
//             clientID: config.auth.instagram.clientID,
//             clientSecret: config.auth.instagram.clientSecret,
//             callbackURL: config.auth.instagram.callbackURL
//         },
//         async function (accessToken, refreshToken, profile, done) {
//             const user = await UserService.find({ socialId: profile.id });
//             if (user) {
//                 // already registered then return (login)
//                 return done(null, user);
//             } else {
//                 // register new user
//                 const newUser = await UserService.addUser
// ({ registerType: 'Instagram', socialId: profile.id, name: profile.displayName })
//                 return done(null, newUser);
//             }

//         }
//     )
// );

// passport.use(
//     new TwitterStrategy(
//         {
//             consumerKey: config.auth.twitter.consumerKey,
//             consumerSecret: config.auth.twitter.consumerSecret,
//             callbackURL: config.auth.twitter.callbackURL
//         },
//         async function (accessToken, refreshToken, profile, done) {
//             const user = await UserService.find({ socialId: profile.id });
//             if (user) {
//                 // already registered then return (login)
//                 return done(null, user);
//             } else {
//                 // register new user
//                 const newUser = await UserService.addUser
// ({ registerType: 'Twitter', socialId: profile.id, name: profile.displayName })
//                 return done(null, newUser);
//             }

//         }
//     )
// );

// passport.use(
//     new AppleStrategy(
//         {
//             clientID: config.auth.apple.clientID,
//             teamID: config.auth.apple.teamID,
//             callbackURL: config.auth.apple.callbackURL,
//             keyID: config.auth.apple.keyID,
//             privateKeyLocation: config.auth.apple.privateKeyLocation,
//         },
//         async function (accessToken, refreshToken, idToken, profile, done) {
//             console.log(idToken)
//             // const user = await UserService.find({ socialId: idToken });
//             // if (user) {
//             //     // already registered then return (login)
//             //     return done(null, user);
//             // } else {
//             //     // register new user
//             //     const newUser =
//                await UserService.addUser
//                ({ registerType: 'Apple', socialId: profile.id, name: profile.displayName })
//             //     return done(null, newUser);
//             // }

//         }
//     )
// );
