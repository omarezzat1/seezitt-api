const mongoose = require('mongoose');
const BaseSchema = require('./base_model');
const Keys = require('../utils/keys');

class MediaMetaSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      media_id: { type: String },
      height: { type: String },
      width: { type: String },
      duration: { type: String },
      resolution: { type: String, default: Keys.vertical },
      frame_rate: { type: String },
    });
  }
}

const schema = new MediaMetaSchema();
module.exports = mongoose.model('MediaMeta', schema, 'media_meta');
