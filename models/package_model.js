const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class PackageSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      name: { type: String },
      description: { type: String },
      mainBalance: { type: Number },
      price: { type: Number },
      extras: [{
        name: { type: String },
        description: { type: String },
        url: { type: String },
        balance: { type: Number, required: false }
      }]
    });
  }
}

const schema = new PackageSchema();
module.exports = mongoose.model('packages', schema, 'packages');
