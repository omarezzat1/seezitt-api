const mongoose = require('mongoose');

class BaseSchema extends mongoose.Schema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      createdTime: { type: Number, default: Date.now() },
      lastModifiedTime: { type: Number, default: Date.now() },
      isDeleted: { type: Boolean, default: false }
    });
  }
}

module.exports = BaseSchema;
