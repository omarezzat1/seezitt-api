const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class ConversationSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            users: [{
                ref: 'User',
                type: mongoose.Types.ObjectId,
            }],
            lastMessage: {
                ref: 'Message',
                type: mongoose.Types.ObjectId,
            },
            pinnedBy: [{
                ref: 'User',
                type: mongoose.Types.ObjectId,
            }],

        });
    }
}

const schema = new ConversationSchema();
module.exports = mongoose.model('Conversation', schema, 'conversations');
