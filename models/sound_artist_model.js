const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class SoundArtistSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      name: { type: String },
      active: { type: Boolean, default: true },
    });
  }
}

const schema = new SoundArtistSchema();
module.exports = mongoose.model('SoundArtist', schema, 'sound_artist');
