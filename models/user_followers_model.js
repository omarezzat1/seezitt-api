const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class UserFollowersSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      followed_userID: {
        ref: 'User',
        type: mongoose.Types.ObjectId,
      },
      follower_userID: {
        ref: 'User',
        type: mongoose.Types.ObjectId,
      }
    });
  }
}

const schema = new UserFollowersSchema();
module.exports = mongoose.model('UserFollowers', schema, 'user_followers');
