const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class SoundGallerySchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      artist_id: { type: String },
      name: { type: String },
      image: { type: String, default: '' },
      url: { type: String },
      active: { type: Boolean, default: true },
      artist: {}
    });
  }
}

const schema = new SoundGallerySchema();
module.exports = mongoose.model('soundGallery', schema, 'sound_gallery');
