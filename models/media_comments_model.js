const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class MediaCommentsSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      user_id: { type: String },
      media_id: { type: String },
      replies: [{
        user_id: { type: String },
        reply: { type: String },
        likes: [{ type: String }],
        createdTime: { type: Number, default: Date.now() }
      }],
      comment: { type: String },
      likes: [{ type: String }],
    });
  }
}

const schema = new MediaCommentsSchema();
module.exports = mongoose.model('MediaComments', schema, 'media_comments');
