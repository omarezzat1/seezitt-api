const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const BaseSchema = require('./base_model');


const DeleteAccountReasons = {
  SOCIAL_MEDIA_BREAK: 'I need a break from social media',
  TIME_CONSUMING: 'I spend so much time on Seezltt',
  SAFETY_PRIVACY: 'Safety or privacy concerns',
  MANY_ADS: 'Too many ads',
  GETTING_STARTED: 'Trouble getting started',
  MULTIPLE_ACCOUNTS: 'I have other accounts'
};

const AccountType = {
  PERSONAL: 'Personal',
  BUSINESS: 'Business',
};


class UserSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      name: { type: String },
      username: { type: String },
      lastUsernameUpdateDate: { type: Date, default: null },
      email: { type: String },
      avatar: { type: String, default: '' },
      cover: { type: String, default: '' },
      bio: { type: String, default: '' },
      password: { type: String },
      mobile: { type: String, default: '' },
      contactEmail: { type: String, default: '' },
      website: { type: String, default: '' },
      gender: { type: String, default: 'male' },
      dateOfBirth: { type: Date },
      country: { type: String, default: '' },
      city: { type: String, default: '' },
      mapLocation: {
        type: { type: String, default: 'Point' },
        coordinates: { type: [Number, Number], index: '2dsphere' }
      },
      notificationsSettings: {
        feedback: { type: Boolean, default: true },
        reminders: { type: Boolean, default: true },
        news: { type: Boolean, default: true },
        interactions: { type: Boolean, default: true },
        followers: { type: Boolean, default: true },
        messages: { type: Boolean, default: true },
      },
      // privacySettings: {
      //   shareMedia: { type: Boolean, default: true },
      //   downloadMedia: { type: Boolean, default: true },
      //   viewFollowing: { type: Boolean, default: true },
      //   viewFollowers: { type: Boolean, default: true },
      //   viewLikedVideos: { type: Boolean, default: true },
      //   messages: { type: Boolean, default: true },
      // },
      privacyOptions: {
        shareMedia: { type: Boolean, default: true },
        downloadMedia: { type: Boolean, default: true },
        viewFollowing: { type: Boolean, default: true },
        viewFollowers: { type: Boolean, default: true },
        viewLikedVideos: { type: Boolean, default: true },
        messages: { type: Boolean, default: true },
        activityStatus: { type: Boolean, default: true },
        viewProfileVisits: { type: Boolean, default: true },
      },
      profileVisitors: [
        {
          userId: { ref: "User", type: mongoose.Types.ObjectId },
          timeStamp: { type: Date },
        },
      ],
      interests: [{ type: String }],
      role: { type: Number },
      following: [{ type: String }],
      followers: [{ type: String }],
      mediaCollection: [{ ref: 'Media', type: mongoose.Types.ObjectId, }],
      mutedUsers: [{ ref: 'User', type: mongoose.Types.ObjectId, }],
      blockedUsers: [{ ref: 'User', type: mongoose.Types.ObjectId, }],
      favouriteMedia: [{ type: String }],
      favouriteHashtags: [{ type: String }],
      mediaCategories: [{ type: String }],
      registerType: { type: String },
      socialId: { type: String },
      pushNotificationToken: { type: String, default: '' },
      privacySettings: [{ type: String }],
      isActive: { type: Boolean, default: true },
      isBlocked: { type: Boolean, default: false },
      isVerified: { type: Boolean, default: false },
      token: { type: String, default: '' },
      balance: { type: Number, default: 0 },
      accountType: { type: String, enum: AccountType, default: AccountType.PERSONAL },
      deletedDetails: {
        deletedDate: { type: Date, default: null },
        permanentlyDeleted: { type: Boolean, default: false },
        reason: { type: String, default: '' }
      },
      activityStatus: {
        isActive: { type: Boolean, default: false }
      },
      businessCategory: { type: String, default: null },
    });
  }
}

const schema = new UserSchema();

// schema.pre('save', async function hashPassword(next) {
//   if (!this.isModified('password')) {
//     return next();
//   }
//   if (!this.password) next();
//   const user = this;
//   const salt = await bcrypt.genSalt(10);
//   const hashedPassword = await bcrypt.hash(user.password, salt);
//   user.password = hashedPassword;
//   next();
// });

schema.methods.comparePassword = async function compare(password) {
  return bcrypt.compare(password, this.password);
};

module.exports = mongoose.model('User', schema, 'users');
module.exports.DeleteAccountReasons = DeleteAccountReasons;