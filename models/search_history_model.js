const mongoose = require('mongoose');
const BaseSchema = require('./base_model');


class SearchHistorySchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            query: { type: String, },
            count: { type: Number, default: 0 },
        });
    }
}

const schema = new SearchHistorySchema();
module.exports = mongoose.model('SearchHistory', schema, 'search_histories');
