const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class PasswordResetTokensSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      user_id: { type: String },
      email: { type: String },
      token: { type: String },
    });
  }
}

const schema = new PasswordResetTokensSchema();
module.exports = mongoose.model('PasswordResetTokens', schema, 'password_token');
