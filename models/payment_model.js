const mongoose = require('mongoose');
const BaseSchema = require('./base_model');


class PaymentSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            userId: {
                type: mongoose.Types.ObjectId,
                ref: 'User',
            },
            paymentId: {
                type: String,
                required: true,
            },
            state: {
                type: String,
                required: true,
            },
            createTime: {
                type: String,
                required: true,
            },
            updateTime: {
                type: String,
                required: true,
            },
            payer: {
                email: {
                    type: String,
                    required: true,
                },
                payerId: {
                    type: String,
                    required: true,
                },
            },
            transactions: [{
                amount: {
                    total: {
                        type: String,
                        required: true,
                    },
                    currency: {
                        type: String,
                        required: true,
                    },
                },
                description: {
                    type: String,
                },
            }],
            links: [{
                href: {
                    type: String,
                    required: true,
                },
                rel: {
                    type: String,
                    required: true,
                },
                method: {
                    type: String,
                    required: true,
                },
            }],
        });
    }
}

const schema = new PaymentSchema();
module.exports = mongoose.model('Payment', schema, 'payments');
