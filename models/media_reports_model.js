const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

const Reasons = {
  MISLEADING_INFO: 'Misleading Information',
  FRAUDS_SCAM: 'Frauds and scams',
  HATE_SPEECH: 'Hate speech',
  HARASSMENT_BULLYING: 'Harassment or bullying',
  VIOLENCE: 'Violence',
  ANIMAL_CRUELTY: 'Animal cruelty'
};

class MediaReportsSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      user_id: { type: String },
      media_id: { type: String },
      reason: { type: String, enum: Reasons },
      isResolved: { type: Boolean, default: false },
      media: {},
      user: {},
    });
  }
}

const schema = new MediaReportsSchema();
module.exports = mongoose.model('MediaReports', schema, 'media_reports');
module.exports.Reasons = Reasons;
