const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const BaseSchema = require('./base_model');

class AdminSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      name: { type: String },
      email: { type: String },
      avatar: { type: String, default: '' },
      password: { type: String },
      role: { type: Number },
      isActive: { type: Boolean, default: true },
      isBlocked: { type: Boolean, default: false },
      token: { type: String, default: '' },
    });
  }
}

const schema = new AdminSchema();

schema.pre('save', async function hashPassword(next) {
  if (!this.password) next();
  const user = this;
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(user.password, salt);
  user.password = hashedPassword;
  next();
});

schema.methods.comparePassword = async function compare(password) {
  return bcrypt.compare(password, this.password);
};

module.exports = mongoose.model('Admin', schema, 'admins');
