const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class GiftSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      name: { type: String },
      imageUrl: { type: String },
      isActive: { type: Boolean, default: true },
      price: { type: Number }
    });
  }
}

const schema = new GiftSchema();
module.exports = mongoose.model('Gift', schema, 'gifts');
