const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

const MessageType = {
    TEXT: 'Text',
    IMAGE: 'Image',
    VIDEO: 'Video',
    FILE: 'File',
    GIF: 'Gif',
    STICKER: 'Sticker',
    EMOJI: 'Emoji',
    SEEZITT_VIDEO: 'SeezittVideo'
};

class MessagesSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            senderId: {
                ref: 'User',
                type: mongoose.Types.ObjectId,
            },
            receiverId: {
                ref: 'User',
                type: mongoose.Types.ObjectId,
            },
            conversationId: {
                ref: 'Conversation',
                type: mongoose.Types.ObjectId,
            },
            message: { type: String },
            type: { type: String, enum: MessageType },
            isRead: { type: Boolean, default: false }


        });
    }
}

const schema = new MessagesSchema();
module.exports = mongoose.model('Message', schema, 'messages');
