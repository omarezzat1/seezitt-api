const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class HighlightSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            userId: {
                ref: 'User',
                type: mongoose.Types.ObjectId,
            },
            storiesIds: [{
                ref: 'Media',
                type: mongoose.Types.ObjectId,
            }],
            thumbnail: { type: String },
            name: { type: String },
        });
    }
}

const schema = new HighlightSchema();
module.exports = mongoose.model('Highlight', schema, 'highlights');
