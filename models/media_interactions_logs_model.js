const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

const MediaInteractionType = {
    WATCHED_TILL_END: 1,
    LIKE: 2,
    SHARE: 3,
    STARTED_WATCHING: 4,
};

class MediaInteractionsLogsSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            userId: { type: String },
            mediaId: { type: String },
            interactionType: { type: Number, enum: MediaInteractionType },
        });
    }
}

const schema = new MediaInteractionsLogsSchema();
module.exports = mongoose.model('MediaInteractionsLogs', schema, 'media_interactions_logs');
module.exports.MediaInteractionType = MediaInteractionType;