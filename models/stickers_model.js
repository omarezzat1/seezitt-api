const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class StickersSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      type: { type: String },
      name: { type: String },
      url: { type: String },
      active: { type: Boolean, default: true },
    });
  }
}

const schema = new StickersSchema();
module.exports = mongoose.model('stickers', schema, 'stickers');
