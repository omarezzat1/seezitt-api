const mongoose = require('mongoose');
const BaseSchema = require('./base_model');


const PaymentStatus = {
  PENDING: 1,
  FAILED: 2,
  COMPLETED: 3,
};


class PaymentSessionSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      sessionId: { type: String },
      coinsAmount: { type: Number },
      price: { type: Number },
      status: { type: String, enum: PaymentStatus, default: PaymentStatus.PENDING },
      userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    });
  }
}

const schema = new PaymentSessionSchema();
module.exports = mongoose.model('PaymentSession', schema, 'payment_sessions');
module.exports.PaymentStatus = PaymentStatus;
