const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class EmailChangeSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      user_id: { type: String },
      email: { type: String },
      token: { type: String },
    });
  }
}

const schema = new EmailChangeSchema();
module.exports = mongoose.model('EmailChange', schema, 'email_change');
