const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

const TransactionType = {
    PURCHASE: 'Purchase',
    GIFT: 'Gift',
    WITHDRAWAL: 'Withdrawal',
};


class CoinTransactionSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            sourceUserId: {
                ref: 'User',
                type: mongoose.Types.ObjectId,
            },
            destinationUserId: {
                ref: 'User',
                type: mongoose.Types.ObjectId,
            },
            transactionId: { type: String },
            amount: { type: Number },
            type: { type: String, enum: TransactionType },
            userIp: { type: String },
        });
    }
}

const schema = new CoinTransactionSchema();
module.exports = mongoose.model('CoinTransaction', schema, 'coin_transactions');
module.exports.TransactionType = TransactionType;
