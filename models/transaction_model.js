const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

const PaymentType = {
    PAYPAL: 'PayPal',
};

class TransactionSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            userId: {
                ref: 'User',
                type: mongoose.Types.ObjectId,
            },
            amount: { type: Number },
            paymentType: { type: String, enum: PaymentType },
            userIp: { type: String },
            paymentReference: { type: String },
        });
    }
}

const schema = new TransactionSchema();
module.exports = mongoose.model('Transaction', schema, 'transactions');
