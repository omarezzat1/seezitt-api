
const mongoose = require('mongoose');

const UpdateStatus = {
    NoUpdates: 1,
    RecommendedUpdate: 2,
    ForceUpdate: 3,
};

class AppVersionSchema extends mongoose.Schema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            version: { type: String },
            status: { type: String, enum: UpdateStatus },
        });
    }
}

const schema = new AppVersionSchema();
module.exports = mongoose.model('AppVersion', schema, 'app_versions');
module.exports.UpdateStatus = UpdateStatus;