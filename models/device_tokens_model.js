const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class DeviceTokensSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      user_id: { type: String },
      token: { type: String },
    });
  }
}

const schema = new DeviceTokensSchema();
module.exports = mongoose.model('DeviceTokens', schema, 'device_tokens');
