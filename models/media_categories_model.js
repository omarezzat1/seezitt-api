const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

class MediaCategoriesSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      name: { type: String },
      isActive: { type: Boolean, default: true },
    });
  }
}

const schema = new MediaCategoriesSchema();
module.exports = mongoose.model('MediaCategories', schema, 'media_categories');
