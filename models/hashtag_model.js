const mongoose = require('mongoose');
const BaseSchema = require('./base_model');


class hashtagSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            name: { type: String },
            active: { type: Boolean, default: true },
            views: { type: Number, default: 0 },
            relatedVideos: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Media' }]
        });
    }
}

const schema = new hashtagSchema();
module.exports = mongoose.model('Hashtag', schema, 'hashtags');
