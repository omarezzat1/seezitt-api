const mongoose = require('mongoose');
const BaseSchema = require('./base_model');

const NotificationType = {
    LIKE: 'Like',
    COMMENT: 'Comment',
    FOLLOW: 'Follow',
    MENTION: 'Mention',
    TAG: 'Tag',
    MESSAGE: 'Message',
    VIDEO_POSTED: 'Video Posted',
    REPLY: 'Reply',
};


class NotificationSchema extends BaseSchema {
    constructor(obj, options) {
        super(obj, options);
        this.add({
            media: {
                ref: 'Media',
                type: mongoose.Types.ObjectId,
            },
            user: {
                ref: 'User',
                type: mongoose.Types.ObjectId,
            },
            triggeredUser: {
                ref: 'User',
                type: mongoose.Types.ObjectId,
            },
            message: { type: String },
            type: { type: String, enum: NotificationType },
            isRead: { type: Boolean, default: false }
        });
    }
}

const schema = new NotificationSchema();
module.exports = mongoose.model('Notification', schema, 'notifications');
module.exports.NotificationType = NotificationType;
