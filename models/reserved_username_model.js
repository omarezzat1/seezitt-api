const mongoose = require('mongoose');


class ReservedUsername extends mongoose.Schema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      username: { type: String },
      used: { type: Boolean, default: false },
    });
  }
}

const schema = new ReservedUsername();


module.exports = mongoose.model('ReservedUsername', schema, 'reserved_usernames');