const mongoose = require('mongoose');
const BaseSchema = require('./base_model');
const Keys = require('../utils/keys');

/*
Categories:
- Science and Technology
- Travel and Events
- Education
- People and Blogs
- News and Politics
- How to and Style
- Entertainment
- Comedy
- Pets and Animals
- Film and Animation
- Autos and Vehicles
- Gaming
- Music
- Sports
- Nonprofits and Activism
 */

const MediaStatus = {
  PROCESSING: 'Processing',
  PROCESSED: 'Processed',
  FAILED: 'Failed',

};

class MediaSchema extends BaseSchema {
  constructor(obj, options) {
    super(obj, options);
    this.add({
      user_id: { type: String },
      hls_url: { type: String },
      hls_10_url: { type: String },
      reducedVideoHlsUrl: { type: String, default: '' },
      reducedVideoUrl: { type: String, default: '' },
      shortVideoUrl: { type: String, default: '' },
      shortVideoHlsUrl: { type: String, default: '' },
      thumbnailUrl: { type: String, default: '' },
      originalUrl: { type: String, default: '' },
      source_url: { type: String },
      caption: { type: String, default: '' }, // One-word caption: Amazing, Awesome .. etc
      thumbnail: { type: String, default: '' },
      comments: [{ type: String }],
      likes: [{ type: String }],
      shares: [{
        ref: 'User',
        type: mongoose.Types.ObjectId,
      }],
      watched_users: [{
        ref: 'User',
        type: mongoose.Types.ObjectId,
      }],
      watched_till_end: [{
        ref: 'User',
        type: mongoose.Types.ObjectId,
      }],
      description: { type: String, default: '' },
      hashtags: [{ type: String }],
      // privacy: { type: String }, //
      categories: [{ type: String }],
      allow_duet: { type: Boolean, default: true },
      sound_id: { type: String, default: '0' },
      media_latlng: {
        type: { type: String, default: 'Point' },
        coordinates: { type: [Number, Number], index: '2dsphere' }
      },
      linked_files: [{ type: String }],
      type: { type: String, default: Keys.mediaVideo },
      tags: [],
      status: { type: String, enum: MediaStatus, default: MediaStatus.PROCESSING },
      category: { type: String, default: '' },
      is_story: { type: Boolean, default: false },
      privacyOptions: {
        isOnlyMe: { type: Boolean, default: false },
      },
      unSafe: { type: Boolean, default: false },
      isPinned: { type: Boolean, default: false },
      receivedGifts: [
        {
          userId: {
            ref: 'User',
            type: mongoose.Types.ObjectId,
          },
          giftId: {
            ref: 'Gift',
            type: mongoose.Types.ObjectId,
          },
          isSeen: {
            type: Boolean,
            default: false,
          },
        }
      ],
      location: {
        type: {
          type: String,
          default: 'Point',
          required: false
        },
        coordinates: {
          type: [Number],
          index: '2dsphere',
          required: false
        },
        place: {
          type: String,
          default: '',
          required: false
        },
        default: {}
      },
    });
  }
}

const schema = new MediaSchema();
module.exports = mongoose.model('Media', schema, 'media');
