const stickersModel = require("../../models/stickers_model");
const stickerService = require("../../services/stickers.service");
const { stickersData } = require("./stickers.data.js");

describe("getStickers", () => {
  let paginationDto;

  const CORRECT_TYPE = "face";
  const CORRECT_NAME = "Smile Face";

  it("should return all of same type and active", async () => {
    // Mock the dependencies
    stickersModel.find = jest.fn(() => ({
      sort: jest.fn(() => ({
        skip: jest.fn(() => ({
          limit: jest.fn(() =>
            stickersData.filter((obj) => obj.type == CORRECT_TYPE && obj.active)
          ),
        })),
      })),
    }));

    paginationDto = { page: 1, type: CORRECT_TYPE };
    expectedValues = stickersData.filter(
      (obj) => obj.active && obj.type == CORRECT_TYPE
    );
    const result = await stickerService.getStickers(paginationDto);
    expect(result).toEqual(expectedValues);
  });

  it("should return all of same type, active and name", async () => {
    // Mock the dependencies
    stickersModel.find = jest.fn(() => ({
      sort: jest.fn(() => ({
        skip: jest.fn(() => ({
          limit: jest.fn(() =>
            stickersData.filter(
              (obj) =>
                obj.type == CORRECT_TYPE &&
                obj.active &&
                obj.name == CORRECT_NAME
            )
          ),
        })),
      })),
    }));

    paginationDto = { page: 1, type: CORRECT_TYPE, name: CORRECT_NAME };
    expectedValues = stickersData.filter(
      (obj) =>
        obj.active && obj.type == CORRECT_TYPE && obj.name == CORRECT_NAME
    );
    const result = await stickerService.getStickers(paginationDto);
    expect(result).toEqual(expectedValues);
  });
});
