const stickersData = [
  {
    type: "face",
    name: "Smile Face",
    url: "https://smileFace.com",
    active: true,
  },
  {
    type: "face",
    name: "Smile Face",
    url: "https://smileFace2.com",
    active: false,
  },
  {
    type: "face",
    name: "Sad Face",
    url: "https://sadFace.com",
    active: false,
  },
  {
    type: "face",
    name: "Angry Face",
    url: "https://angryFace.com",
    active: true,
  },
  {
    type: "animal",
    name: "Panda",
    url: "https://panda.com",
    active: true,
  },
  {
    type: "animal",
    name: "Cat",
    url: "https://cat.com",
    active: false,
  },
  {
    type: "animal",
    name: "Dog",
    url: "https://dog.com",
    active: true,
  },
];
module.exports = { stickersData };
