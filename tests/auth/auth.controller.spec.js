const userModel = require('../../models/user_model');
const authController = require("../../controllers/auth.controller");


describe('signIn', () => {
    let req;
    let res;
    beforeEach(() => {
        req = {
            body: {
                email: 'test@example.com',
                password: 'password',
            },
        };
        res = {
            json: jest.fn(),
            status: jest.fn(() => res),
        };
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should return an error if the email is not provided', async () => {
        req.body.email = '';
        await authController.signIn(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            status: 400,
            message: 'Please add your email first!',
            data: {}
        });
    });

    it('should return an error if the password is not provided', async () => {
        req.body.password = '';
        await authController.signIn(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            status: 400,
            message: 'Please add your password first!',
            data: {}
        });
    });

    it('should return an error if the password is longer than 64 characters', async () => {
        req.body.password = 'a'.repeat(65);
        await authController.signIn(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            status: 400,
            message: 'Please add your password first!',
            data: {}
        });
    });

    it('should return an error if user does not exist', async () => {
        userModel.findOne = jest.fn().mockResolvedValue(null);
        await authController.signIn(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            status: 400,
            message: 'User information not valid!',
            data: {}
        });
    });

    it('should return an error if the user is deleted', async () => {
        const userExists = {
            comparePassword: jest.fn().mockResolvedValue(true),
            deletedDetails: {
                deletedDate: '2021-01-01',
            },
        };
        userModel.findOne = jest.fn().mockResolvedValue(userExists);
        await authController.signIn(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            status: 400,
            message: 'This account is already deleted!',
            data: {}
        });
    });


})

describe('updateNotificationToken', () => {
    let req;
    let res;
    beforeEach(() => {
        req = {
            body: {
                pushNotificationToken: 'token',
            },
            user: {
                id: '123',
            },
        };
        res = {
            json: jest.fn(),
            status: jest.fn(() => res),
        };
        jest.spyOn(userModel, 'updateOne').mockImplementation(() => { });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should update the push notification token for the user', async () => {
        await authController.updateNotificationToken(req, res);
        expect(userModel.updateOne).toHaveBeenCalledWith({ _id: '123' }, { pushNotificationToken: 'token' });
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
            status: 200,
            message: 'Token added successfully!',
            data: {}
        });
    });

    it('should return a server error if updating the token fails', async () => {
        jest.spyOn(userModel, 'updateOne').mockImplementation(() => {
            throw new Error();
        });
        await authController.updateNotificationToken(req, res);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({
            status: 500,
            message: '',
            data: expect.any(Error)
        });
    });
});
