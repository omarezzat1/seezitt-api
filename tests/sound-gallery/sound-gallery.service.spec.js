const soundGallery = require('../../models/sound_gallery_model');
const artistGallery = require('../../models/sound_artist_model');
const audioService = require("../../services/audo.service")
const Constants = require('../../utils/constants');

describe('getSoundGallery', () => {
    let paginationDto;
    let soundGalleryData;
    let artist;

    beforeEach(() => {
        // Set up some test data
        paginationDto = { page: 1 };
        soundGalleryData = [{ active: true, artist_id: 123 }];
        artist = { id: 123 };

        // Mock the dependencies
        soundGallery.find = jest.fn(() => ({
            skip: jest.fn(() => ({ limit: jest.fn(() => soundGalleryData) })),
        }));
        artistGallery.findOne = jest.fn(() => artist);
    });

    it('should return sound gallery data with artist information', async () => {
        const result = await audioService.getSoundGallery(paginationDto);
        expect(result).toEqual([{ active: true, artist_id: 123, artist }]);
    });

    it('should apply pagination to the sound gallery data', async () => {
        paginationDto.page = 2;
        await audioService.getSoundGallery(paginationDto);
        expect(soundGallery.find).toHaveBeenCalledWith({ active: true });

    });

    it('should handle errors properly', async () => {
        soundGallery.find = jest.fn(() => { throw new Error('test error'); });
        await expect(audioService.getSoundGallery(paginationDto)).rejects.toThrow('test error');
    });
});