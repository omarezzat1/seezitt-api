const MediaModel = require("../../models/media_model")
const { updateMediaContent } = require("../../controllers/media-content.controller");

// const req = {
//     params: { id: "1" },
//     body: {
//         description: "new description",
//         tags: "tag1,tag2",
//         category: "category1",
//         privacyOptions: { isOnlyMe: true }
//     }
// };
// const res = {
//     send: jest.fn(),
//     json: jest.fn(),
//     status: jest.fn(() => res),

// };
// const mediaModel = {
// findOne: jest.fn().mockResolvedValue({
//     _id: "1",
//     description: "old description",
//     tags: ["oldTag1"],
//     category: "oldCategory",
//     privacyOptions: { isOnlyMe: false },
//     save: jest.fn().mockResolvedValue(true)
// })
// };
// const response = {
//     successResponse: jest.fn(),
//     serverErrorResponse: jest.fn()
// };
// const logger = {
//     e: jest.fn()
// };
// // 
// MediaModel.findOne = jest.fn().mockResolvedValue({
//     findOne: jest.fn().mockResolvedValue({
//         _id: "1",
//         description: "old description",
//         tags: ["oldTag1"],
//         category: "oldCategory",
//         privacyOptions: { isOnlyMe: false },
//         save: jest.fn().mockResolvedValue(true)
//     })
// });

// // jest.mock(MediaModel, () => {
// //     return {
// //         findOne: jest.fn().mockResolvedValue({
// //             _id: "1",
// //             description: "old description",
// //             tags: ["oldTag1"],
// //             category: "oldCategory",
// //             privacyOptions: { isOnlyMe: false },
// //             save: jest.fn().mockResolvedValue(true)
// //         })
// //     };
// // });

// jest.mock("../../utils/response", () => {
//     return {
//         successResponse: jest.fn(),
//         serverErrorResponse: jest.fn()
//     }
// });
// jest.mock("../../utils/logger", () => { return { e: jest.fn() } });


// describe("updateMediaContent", () => {
//     beforeEach(() => {
//         jest.clearAllMocks();
//     });

// it("updates the media content", async () => {
//     await updateMediaContent(req, res);

//     expect(mediaModel.findOne).toHaveBeenCalledWith({ _id: "1" });
//     expect(res.send).toHaveBeenCalledWith({
//         status: "success",
//         message: "",
//         data: {
//             mediaContent: {
//                 _id: "1",
//                 description: "new description",
//                 tags: ["tag1", "tag2"],
//                 category: "category1",
//                 privacyOptions: { isOnlyMe: true }
//             }
//         }
//     });
// });

//     it("handles errors", async () => {
//         jest.spyOn(MediaModel, 'findOne').mockImplementation(() => {
//             throw new Error();
//         });
//         await updateMediaContent(req, res);
//         expect(res.status).toHaveBeenCalledWith(500);
//         expect(res.json).toHaveBeenCalledWith({
//             status: 500,
//             message: '',
//             data: expect.any(Error)
//         });
//     });
// });



describe('updateMediaContent', () => {
    let req;
    let res;
    beforeEach(() => {
        req = {
            params: { id: "1" },
            body: {
                description: "new description",
                tags: "tag1,tag2",
                category: "category1",
            }
        };
        res = {
            json: jest.fn(),
            status: jest.fn(() => res),
        };
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it("updates the media content", async () => {

        MediaModel.findOne = jest.fn().mockResolvedValue({
            findOne: jest.fn().mockResolvedValue({
                _id: "1",
                description: "old description",
                tags: ["oldTag1"],
                category: "oldCategory",
                privacyOptions: { isOnlyMe: false },
                save: jest.fn().mockResolvedValue(true)
            })
        });
        await updateMediaContent(req, res);

        expect(MediaModel.findOne).toHaveBeenCalledWith({ _id: "1" });
        // expect(res.json).toHaveBeenCalledWith({
        //     status: "success",
        //     message: "",
        //     data: {
        //         mediaContent: {
        //             _id: "1",
        //             description: "new description",
        //             tags: ["tag1", "tag2"],
        //             category: "category1",
        //             privacyOptions: { isOnlyMe: true }
        //         }
        //     }
        // });
    });


    it("handles errors", async () => {
        jest.spyOn(MediaModel, 'findOne').mockImplementation(() => {
            throw new Error();
        });
        await updateMediaContent(req, res);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({
            status: 500,
            message: 'Something went wrong',
            data: expect.any(Error)
        });
    });


})
