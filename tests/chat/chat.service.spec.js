const ConversationsModel = require("../../models/conversation_model");
const chatService = require("../../services/chat.service");
const MessagesModel = require("../../models/message_model");

describe("performIntersection", () => {
  it("return true if same num arrays", () => {
    arr1 = [1, 3, 5];
    arr2 = [1, 3, 5];
    expect(chatService.performIntersection(arr1, arr2)).toBe(true);
  });
  it("return true if one value in the num arrays is similar", () => {
    arr1 = [1, 2, 3];
    arr2 = [0, 3, 5];
    expect(chatService.performIntersection(arr1, arr2)).toBe(true);
  });
  it("return true if one value in the string arrays is similar", () => {
    arr1 = ["hello", "hi", "hey"];
    arr2 = ["goodbye", "hi", "good night"];
    expect(chatService.performIntersection(arr1, arr2)).toBe(true);
  });
  it("return false for empty arrays", () => {
    arr1 = [];
    arr2 = [];
    expect(chatService.performIntersection(arr1, arr2)).toBe(false);
  });
  it("return false for 1 of the arrays is empty", () => {
    arr1 = [1];
    arr2 = [];
    expect(chatService.performIntersection(arr1, arr2)).toBe(false);
  });
  it("return false if arrays not similar", () => {
    arr1 = [1, 2, 3];
    arr2 = [4, 5, 6];
    expect(chatService.performIntersection(arr1, arr2)).toBe(false);
  });
});

const mongoose = require("mongoose");
const userModel = require("../../models/user_model");

let userId, isActive;
describe("pinConversation", () => {
  beforeEach(() => {
    userId = mongoose.Types.ObjectId();
    targetConversationId = mongoose.Types.ObjectId();
  });

  it("pin unpinned conversation (not pinned by the user)", async () => {
    const mockConversation = {
      _id: targetConversationId,
      pinnedBy: [],
      save: jest.fn(() => {}),
    };
    jest
      .spyOn(ConversationsModel, "findOne")
      .mockResolvedValue(mockConversation);

    await chatService.pinConversation(targetConversationId, userId);

    expect(ConversationsModel.findOne).toHaveBeenCalled();
    expect(mockConversation.save).toHaveBeenCalled();
    expect(mockConversation.pinnedBy.includes(userId)).toBe(true);
  });
  it("unpin pinned conversation (pinned by the user)", async () => {
    const mockConversation = {
      _id: targetConversationId,
      pinnedBy: [userId],
      save: jest.fn(() => {}),
    };
    jest
      .spyOn(ConversationsModel, "findOne")
      .mockResolvedValue(mockConversation);

    await chatService.pinConversation(targetConversationId, userId);

    expect(ConversationsModel.findOne).toHaveBeenCalled();
    expect(mockConversation.save).toHaveBeenCalled();
    expect(mockConversation.pinnedBy.includes(userId)).toBe(false);
  });
  
});
describe("getMessages", () => {
  beforeEach(() => {
    userId = mongoose.Types.ObjectId();
    targetMessageId = mongoose.Types.ObjectId();
    targetConversationId = mongoose.Types.ObjectId();
  });
  it("get unread message", async () => {
    const mockMessages = [{
      _id: targetConversationId,
      receiverId:{_id:userId},
      isRead:false,
    }];
    MessagesModel.find = jest.fn(() => ({
      populate: jest.fn(() => ({
        populate: jest.fn(() => ({
          skip: jest.fn(() => ({
            sort: jest.fn(() => ({ limit: jest.fn(() => mockMessages) })),
          })),
        })),
      })),
    }));
    MessagesModel.updateMany = jest.fn(()=>{
      mockMessages.isRead = true;
    });
    MessagesModel.countDocuments = jest.fn(()=>({}))
    await chatService.getMessages({},targetConversationId, userId);

    expect(MessagesModel.find).toHaveBeenCalled();
    expect(MessagesModel.updateMany).toHaveBeenCalled();
    expect(MessagesModel.countDocuments).toHaveBeenCalled();
    expect(mockMessages.isRead).toBe(true);
  });
  it("get already read message", async () => {
    const mockMessages = [{
      _id: targetConversationId,
      receiverId:{_id:userId},
      isRead:true,
    }];
    MessagesModel.find = jest.fn(() => ({
      populate: jest.fn(() => ({
        populate: jest.fn(() => ({
          skip: jest.fn(() => ({
            sort: jest.fn(() => ({ limit: jest.fn(() => mockMessages) })),
          })),
        })),
      })),
    }));
    MessagesModel.updateMany = jest.fn(()=>{
      mockMessages.isRead = true;
    });
    MessagesModel.countDocuments = jest.fn(()=>({}))
    await chatService.getMessages({},targetConversationId, userId);

    expect(MessagesModel.find).toHaveBeenCalled();
    expect(MessagesModel.updateMany).toHaveBeenCalled();
    expect(MessagesModel.countDocuments).toHaveBeenCalled();
    expect(mockMessages.isRead).toBe(true);
  });
});
describe("updateActivityStatus", () => {
  beforeEach(() => {
    userId = mongoose.Types.ObjectId();
    isActive = true;
  });

  it("updates the activity status of a user from false to true", async () => {
    const mockUserModel = {
      _id: userId,
      activityStatus: {
        isActive: false,
      },
      save: jest.fn(() => {}),
    };

    jest.spyOn(userModel, "findOne").mockResolvedValue(mockUserModel);

    await chatService.updateActivityStatus(userId, isActive);
    expect(userModel.findOne).toHaveBeenCalled();
    expect(mockUserModel.save).toHaveBeenCalled();
    expect(mockUserModel.activityStatus.isActive).toBe(isActive);
  });
  it("updates the activity status of a user from true to true", async () => {
    const mockUserModel = {
      _id: userId,
      activityStatus: {
        isActive: true,
      },
      save: jest.fn(() => {}),
    };

    jest.spyOn(userModel, "findOne").mockResolvedValue(mockUserModel);

    await chatService.updateActivityStatus(userId, isActive);
    expect(userModel.findOne).toHaveBeenCalled();
    expect(mockUserModel.save).toHaveBeenCalled();
    expect(mockUserModel.activityStatus.isActive).toBe(isActive);
  });
});
