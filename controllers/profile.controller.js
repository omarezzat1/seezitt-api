const express = require("express");
const passport = require("passport");
const AWS = require("aws-sdk");
const fs = require('fs');
const constants = require("../utils/constants");
const Response = require("../utils/response");
const Keys = require("../utils/keys");
const config = require("../configs/config");
const logger = require("../utils/logger");

const UserFollowerModel = require("../models/user_followers_model");
const ReservedUsernameModel = require("../models/reserved_username_model");
const MediaModel = require("../models/media_model");
const MediaComments = require("../models/media_comments_model");
const userModel = require("../models/user_model");
const MediaCategories = require("../models/media_categories_model");
const DeleteAccountReasons =
    require("../models/user_model").DeleteAccountReasons;

const NotificationService = require("../services/notifications.service");


async function followUser(req, res) {
    try {
        const currentUserId = req.user.id;
        const followedUserId = req.params.id;
        let userFollower = await UserFollowerModel.findOne({
            followed_userID: followedUserId,
            follower_userID: currentUserId,
            isDeleted: false,
        });
        let sendNotification = false;
        if (userFollower) {
            userFollower.isDeleted = true;
        } else {
            sendNotification = true;
            userFollower = await UserFollowerModel.create({
                followed_userID: followedUserId,
                follower_userID: currentUserId,
            });
        }

        await userFollower.save();
        if (sendNotification) {
            NotificationService.sendNewFollowerNotification(currentUserId, followedUserId)
        }
        return Response.successResponse(res, "", userFollower);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function getUserFollowing(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const { page } = query;
        const userId = req.params.id;
        let pageNum = page ?? 1;
        let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;
        const findQuery = { followed_userID: userId, isDeleted: false };
        const following = await UserFollowerModel.find(findQuery)
            .select("follower_userID")
            .populate({
                path: "follower_userID",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            })
            .skip(skip)
            .sort({
                createdTime: -1,
            })
            .limit(constants.pageLimit);

        const count = await UserFollowerModel.countDocuments(findQuery);

        return Response.successResponse(res, "", { data: following, total: count });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function getUserFollowers(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const { page } = query;
        const userId = req.params.id;
        let pageNum = page ?? 1;
        let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;
        const findQuery = { follower_userID: userId, isDeleted: false };
        const followers = await UserFollowerModel.find(findQuery)
            .select("followed_userID")
            .populate({
                path: "followed_userID",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            })
            .skip(skip)
            .sort({
                createdTime: -1,
            })
            .limit(constants.pageLimit);

        const count = await UserFollowerModel.countDocuments(findQuery);

        return Response.successResponse(res, "", { data: followers, total: count });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function getUserVideos(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const { page, pageSize } = query;
        const currentUserId = req.user.id;
        const userId = req.params.id;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;
        const mediaContentQuery = {
            user_id: userId,
            isDeleted: false,
            is_story: { $ne: true },
        };
        if (currentUserId != userId) {
            mediaContentQuery.privacyOptions = {
                isOnlyMe: false,
            }
        }
        const mediaContents = await MediaModel.find(mediaContentQuery)
            .populate({
                path: "user_id",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            })
            .populate({
                path: "category",
                model: "MediaCategories",
                select: ["name"],
            })
            .populate({
                path: "receivedGifts.giftId",
                model: "Gift",
                select: ["_id", "name", "imageUrl", "price"],
            })
            .populate({
                path: "receivedGifts.userId",
                model: "User",
                select: ["_id", "name", "avatar", "username"]
            })
            .skip(skip)
            .sort({
                isPinned: -1,
                _id: -1,
            })
            .limit(pageLimit);

        const mediaUsers = mediaContents.map((content) => content.user_id._id);
        let usersFollowing = await UserFollowerModel.find({
            follower_userID: currentUserId,
            followed_userID: { $in: mediaUsers },
            isDeleted: false,
        }).select("follower_userID");
        if (usersFollowing.length) {
            usersFollowing = usersFollowing.map((userFollowing) =>
                userFollowing.follower_userID.toString()
            );
        }

        const mediaContentsIds = mediaContents.map((mediaContent) =>
            mediaContent._id.toString()
        );
        const mediaContentsResponse = await Promise.all(
            mediaContents.map(async (mediaContent) => {
                let comments = await MediaComments.find({
                    _id: { $in: mediaContent.comments },
                })
                    .populate({
                        path: "user_id",
                        model: "User",
                        select: ["_id", "name", "avatar", "username", "isVerified"],
                    })
                    .populate({
                        path: "replies.user_id",
                        model: "User",
                        select: ["_id", "name", "avatar", "username", "isVerified"],
                    });

                comments = comments.map((comment) => ({
                    id: comment._id,
                    user: comment.user_id,
                    comment: comment.comment,
                    likes: comment.likes.length,
                    isLiked: comment.likes.includes(currentUserId),
                    createdTime: comment.createdTime,
                    replies: comment.replies?.map((reply) => ({
                        id: reply.id,
                        user: reply.user_id,
                        reply: reply.reply,
                        likes: reply.likes.length,
                        isLiked: reply.likes.includes(currentUserId),
                        createdTime: reply.createdTime,
                    })),
                }));
                const mediaContentResponse = {
                    mediaId: mediaContent._id.toString(),
                    createdTime: mediaContent.createdTime,
                    user: {
                        _id: mediaContent.user_id._id,
                        name: mediaContent.user_id.name,
                        avatar: mediaContent.user_id.avatar,
                        username: mediaContent.user_id.username,
                        isFollowed: usersFollowing.includes(currentUserId),
                    },
                    likes: mediaContent.likes.length,
                    shares: mediaContent.shares.length,
                    views: mediaContent.watched_users.length,
                    description: mediaContent.description,
                    linkedFiles: mediaContent.linked_files,
                    hlsUrl: mediaContent.hls_url,
                    reducedVideoUrl: mediaContent.reducedVideoUrl,
                    reducedVideoHlsUrl: mediaContent.reducedVideoHlsUrl,
                    shortVideoUrl: mediaContent.shortVideoUrl,
                    shortVideoHlsUrl: mediaContent.shortVideoHlsUrl,
                    privacyOptions: mediaContent.privacyOptions,
                    thumbnailUrl: mediaContent.thumbnailUrl,
                    thumbnail: mediaContent.thumbnail,
                    originalUrl: mediaContent.originalUrl,
                    comments: comments,
                    isLiked: mediaContent.likes.includes(currentUserId),
                    receivedGifts: mediaContent.receivedGifts,
                    isPinned: mediaContent.isPinned,
                };
                return mediaContentResponse;
            })
        );
        const count = await MediaModel.countDocuments(mediaContentQuery);

        const sortedMediaContentsResponseData = mediaContentsResponse.sort((a, b) => {
            if (a.isPinned && !b.isPinned) return 1;
            if (!a.isPinned && !b.isPinned) {
                if (a.mediaId > b.mediaId) return -1;
                if (a.mediaId < b.mediaId) return 1;
            }
            return 0;
        });
        return Response.successResponse(res, "", {
            data: sortedMediaContentsResponseData,
            total: count,
        });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function userCollections(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const { page, pageSize } = query;
        const userId = req.user.id;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;
        const user = await userModel
            .findOne({ _id: userId })
            .select("mediaCollection");
        const mediaContents = await MediaModel.find({
            _id: { $in: user.mediaCollection },
            isDeleted: false,
        })
            .populate({
                path: "user_id",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            })
            .populate({
                path: "receivedGifts.giftId",
                model: "Gift",
                select: ["_id", "name", "imageUrl", "price"],
            })
            .populate({
                path: "receivedGifts.userId",
                model: "User",
                select: ["_id", "name", "avatar", "username"]
            })
            .skip(skip)
            .sort({
                createdTime: -1,
            })
            .limit(pageLimit);

        const mediaUsers = mediaContents.map((content) => content.user_id._id);
        let usersFollowing = await UserFollowerModel.find({
            follower_userID: userId,
            followed_userID: { $in: mediaUsers },
            isDeleted: false,
        }).select("follower_userID");
        if (usersFollowing.length) {
            usersFollowing = usersFollowing.map((userFollowing) =>
                userFollowing.follower_userID.toString()
            );
        }

        const mediaContentsIds = mediaContents.map((mediaContent) =>
            mediaContent._id.toString()
        );
        const mediaContentsResponse = await Promise.all(
            mediaContents.map(async (mediaContent) => {
                let comments = await MediaComments.find({
                    _id: { $in: mediaContent.comments },
                })
                    .populate({
                        path: "user_id",
                        model: "User",
                        select: ["_id", "name", "avatar", "username", "isVerified"],
                    })
                    .populate({
                        path: "replies.user_id",
                        model: "User",
                        select: ["_id", "name", "avatar", "username", "isVerified"],
                    });

                comments = comments.map((comment) => ({
                    id: comment._id,
                    user: comment.user_id,
                    comment: comment.comment,
                    likes: comment.likes.length,
                    isLiked: comment.likes.includes(userId),
                    createdTime: comment.createdTime,
                    replies: comment.replies?.map((reply) => ({
                        id: reply.id,
                        user: reply.user_id,
                        reply: reply.reply,
                        likes: reply.likes.length,
                        isLiked: reply.likes.includes(userId),
                        createdTime: reply.createdTime,
                    })),
                }));
                const mediaContentResponse = {
                    mediaId: mediaContent._id.toString(),
                    createdTime: mediaContent.createdTime,
                    user: {
                        _id: mediaContent.user_id._id,
                        name: mediaContent.user_id.name,
                        avatar: mediaContent.user_id.avatar,
                        username: mediaContent.user_id.username,
                        isFollowed: usersFollowing.includes(userId),
                    },
                    likes: mediaContent.likes.length,
                    shares: mediaContent.shares.length,
                    views: mediaContent.watched_users.length,
                    description: mediaContent.description,
                    linkedFiles: mediaContent.linked_files,
                    hlsUrl: mediaContent.hls_url,
                    reducedVideoUrl: mediaContent.reducedVideoUrl,
                    reducedVideoHlsUrl: mediaContent.reducedVideoHlsUrl,
                    shortVideoUrl: mediaContent.shortVideoUrl,
                    shortVideoHlsUrl: mediaContent.shortVideoHlsUrl,
                    privacyOptions: mediaContent.privacyOptions,
                    thumbnailUrl: mediaContent.thumbnailUrl,
                    thumbnail: mediaContent.thumbnail,
                    originalUrl: mediaContent.originalUrl,
                    comments: comments,
                    isLiked: mediaContent.likes.includes(userId),
                    receivedGifts: mediaContent.receivedGifts,
                };
                return mediaContentResponse;
            })
        );
        return Response.successResponse(res, "", {
            data: mediaContentsResponse,
            total: user.mediaCollection.length,
        });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function currentUserData(req, res) {
    try {
        const userId = req.user.id;
        const user = await userModel
            .findOne({ _id: userId })
            .select([
                "avatar",
                "cover",
                "name",
                "bio",
                "email",
                "country",
                "dateOfBirth",
                "mediaCategories",
                "website",
                "contactEmail",
                "username",
                "accountType",
                "balance",
                "businessCategory"
            ]);

        let categories = [];
        if (user.mediaCategories.length > 0) {
            await Promise.all(
                user.mediaCategories.map(async (category) => {
                    let categoryObj = await MediaCategories.findOne({ _id: category });
                    if (categoryObj) {
                        categories.push(categoryObj.name);
                    }
                })
            );
        }
        user.mediaCategories = categories;

        user.dateOfBirth = user.dateOfBirth ? user.dateOfBirth : ""; // fix users without BD

        const findFollowersQuery = { followed_userID: userId, isDeleted: false };
        const followersCount = await UserFollowerModel.countDocuments(
            findFollowersQuery
        );

        const findFollowingQuery = { follower_userID: userId, isDeleted: false };
        const followingCount = await UserFollowerModel.countDocuments(
            findFollowingQuery
        );

        const mediaContentQuery = {
            user_id: userId,
            isDeleted: false,
            is_story: { $ne: true },
        };

        const reducer = (accumulator, curr) => accumulator + curr;
        const videosLikes = await MediaModel.find(mediaContentQuery).select(
            "likes"
        );
        const likesNum =
            videosLikes.length > 0
                ? videosLikes.map((media) => media.likes.length).reduce(reducer)
                : 0;

        const userDataDTO = {
            ...user._doc,
            followersNumber: followersCount,
            followingNumber: followingCount,
            likesNum: likesNum,
        };


        return Response.successResponse(res, "", userDataDTO);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function getUserData(req, res) {
    try {
        const currentUserId = req.user.id;
        const userId = req.params.id;
        const user = await userModel
            .findOne({ _id: userId })
            .select([
                "avatar",
                "cover",
                "name",
                "bio",
                "email",
                "country",
                "dateOfBirth",
                "mediaCategories",
                "website",
                "contactEmail",
                "privacyOptions",
                "notificationsSettings",
                "username",
                "accountType",
                "businessCategory",
                "isVerified",
            ]);

        let categories = [];
        if (user.mediaCategories.length > 0) {
            await Promise.all(
                user.mediaCategories.map(async (category) => {
                    let categoryObj = await MediaCategories.findOne({ _id: category });
                    if (categoryObj) {
                        categories.push(categoryObj.name);
                    }
                })
            );
        }
        user.mediaCategories = categories;

        user.dateOfBirth = user.dateOfBirth ? user.dateOfBirth : ""; // fix users without BD

        const findFollowersQuery = { followed_userID: userId, isDeleted: false };
        const followersCount = await UserFollowerModel.countDocuments(
            findFollowersQuery
        );

        const findFollowingQuery = { follower_userID: userId, isDeleted: false };
        const followingCount = await UserFollowerModel.countDocuments(
            findFollowingQuery
        );

        const isFollowedQuery = {
            followed_userID: userId,
            follower_userID: currentUserId,
            isDeleted: false,
        };
        const isFollowed = await UserFollowerModel.countDocuments(isFollowedQuery);

        const mediaContentQuery = {
            user_id: userId,
            isDeleted: false,
            is_story: { $ne: true },
        };

        const reducer = (accumulator, curr) => accumulator + curr;
        const videosLikes = await MediaModel.find(mediaContentQuery).select(
            "likes"
        );
        const likesNum =
            videosLikes.length > 0
                ? videosLikes.map((media) => media.likes.length).reduce(reducer)
                : 0;

        const userDataDTO = {
            ...user._doc,
            isFollowed: isFollowed ? true : false,
            followersNumber: followersCount,
            followingNumber: followingCount,
            likesNum: likesNum,
        };

        profileVisit(currentUserId, userId);
        return Response.successResponse(res, "", userDataDTO);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function profileVisit(currentUserId, targetUserId) {
    try {
        const currentUser = await userModel
            .findOne({ _id: currentUserId })
            .select(["_id", "privacyOptions"]);
        const targetUser = await userModel
            .findOne({ _id: targetUserId })
            .select(["_id", "privacyOptions", "profileVisitors"]);
        if (
            !currentUser.privacyOptions.viewProfileVisits ||
            !targetUser.privacyOptions.viewProfileVisits
        ) {
            return;
        }
        targetUser.profileVisitors.push({
            userId: currentUser._id,
            timeStamp: Date.now(),
        });

        await targetUser.save();
        NotificationService.sendNewProfileVisitNotification(
            currentUserId,
            targetUserId
        );
    } catch (error) {
        throw error;
    }
}
async function getUserProfileVisitors(req, res) {
    try {
        const userId = req.user.id;

        let visitors = await userModel
            .findOne({ _id: userId })
            .populate({
                path: "profileVisitors.userId",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            })
            .select(["profileVisitors"])
            .sort({ 'profileVisitors.timeStamp': -1 })
            //flat the data
            .then((data) => {
                return data.profileVisitors.map((visitor) => { return { ...visitor.userId } });
            })
        visitors = visitors.map(
            visitor => { return { ...visitor._doc, id: visitor._doc._id } }
        )
        const visitorsIds = visitors.map(visitor => visitor.id);

        const followingList = await UserFollowerModel.find(
            {
                follower_userID: userId,
                followed_userID: { $in: visitorsIds },
                isDeleted: false
            }
        );
        const followersList = await UserFollowerModel.find(
            {
                follower_userID: { $in: visitorsIds },
                followed_userID: userId,
                isDeleted: false
            }
        );
        visitors = visitors.map(
            visitor => {
                let isFollowing = followingList.find(f => f.followed_userID.equals(visitor.id)) ? true : false;
                let isFollower = followersList.find(f => f.follower_userID.equals(visitor.id)) ? true : false;
                return { ...visitor, isFollowing, isFollower }
            }
        )


        return Response.successResponse(res, "", visitors);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function deleteUser(req, res) {
    try {
        const { password, reason } = req.body;
        const userId = req.user.id;

        const user = await userModel.findOne({ _id: userId });

        const comparePasswords = await user.comparePassword(password);
        if (!comparePasswords) {
            return Response.badReqResponse(res, "Invalid Password");
        }
        user.deletedDetails.deletedDate = new Date();
        user.deletedDetails.reason = reason;
        user.isDeleted = true;
        await user.save();

        // await userModel.updateOne({ _id: userId }, { isDeleted: true });
        await UserFollowerModel.updateMany(
            { followed_userID: userId },
            { isDeleted: true }
        );
        await UserFollowerModel.updateMany(
            { follower_userID: userId },
            { isDeleted: true }
        );

        return Response.successResponse(res, "");
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function getDeleteAccountReasons(req, res) {
    try {
        return Response.successResponse(res, "", DeleteAccountReasons);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function getBlockedUsers(req, res) {
    try {
        const userId = req.user.id;
        const user = await userModel.findOne({ _id: userId })
            .select(['blockedUsers']).populate({
                path: 'blockedUsers',
                model: 'User',
                select: ['_id', 'name', 'avatar']
            })
        return Response.successResponse(res, '', user.blockedUsers);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, '', error);
    }
}

async function getLikedVideos(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const { page, pageSize } = query;
        const currentUserId = req.user.id;
        const userId = req.params.id;
        let pageNum = page ?? 1;
        let pageLimit = pageSize ?? constants.pageLimit;
        let skip = pageNum > 0 ? (pageNum - 1) * pageLimit : 1;
        const mediaContentQuery = {
            isDeleted: false,
            hls_url: { $ne: "" },
            likes: userId,
            is_story: { $ne: true },
        };
        const mediaContents = await MediaModel.find(mediaContentQuery)
            .populate({
                path: "user_id",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
            })
            .populate({
                path: "receivedGifts.giftId",
                model: "Gift",
                select: ["_id", "name", "imageUrl", "price"],
            })
            .populate({
                path: "receivedGifts.userId",
                model: "User",
                select: ["_id", "name", "avatar", "username"]
            })
            .skip(skip)
            .sort({
                _id: -1,
            })
            .limit(pageLimit);
        const mediaUsers = mediaContents.map((content) => content.user_id._id);
        let usersFollowing = await UserFollowerModel.find({
            follower_userID: currentUserId,
            followed_userID: { $in: mediaUsers },
            isDeleted: false,
        }).select("follower_userID");
        if (usersFollowing.length) {
            usersFollowing = usersFollowing.map((userFollowing) =>
                userFollowing.follower_userID.toString()
            );
        }

        const mediaContentsIds = mediaContents.map((mediaContent) =>
            mediaContent._id.toString()
        );
        const mediaContentsResponse = await Promise.all(
            mediaContents.map(async (mediaContent) => {
                let comments = await MediaComments.find({
                    _id: { $in: mediaContent.comments },
                })
                    .populate({
                        path: "user_id",
                        model: "User",
                        select: ["_id", "name", "avatar", "username", "isVerified"],
                    })
                    .populate({
                        path: "replies.user_id",
                        model: "User",
                        select: ["_id", "name", "avatar", "username", "isVerified"],
                    });

                comments = comments.map((comment) => ({
                    id: comment._id,
                    user: comment.user_id,
                    comment: comment.comment,
                    likes: comment.likes.length,
                    isLiked: comment.likes.includes(currentUserId),
                    createdTime: comment.createdTime,
                    replies: comment.replies?.map((reply) => ({
                        id: reply.id,
                        user: reply.user_id,
                        reply: reply.reply,
                        likes: reply.likes.length,
                        isLiked: reply.likes.includes(currentUserId),
                        createdTime: reply.createdTime,
                    })),
                }));
                const mediaContentResponse = {
                    mediaId: mediaContent._id.toString(),
                    createdTime: mediaContent.createdTime,
                    user: {
                        _id: mediaContent.user_id._id,
                        name: mediaContent.user_id.name,
                        avatar: mediaContent.user_id.avatar,
                        username: mediaContent.user_id.username,
                        isFollowed: usersFollowing.includes(currentUserId),
                    },
                    likes: mediaContent.likes.length,
                    shares: mediaContent.shares.length,
                    views: mediaContent.watched_users.length,
                    description: mediaContent.description,
                    linkedFiles: mediaContent.linked_files,
                    hlsUrl: mediaContent.hls_url,
                    reducedVideoUrl: mediaContent.reducedVideoUrl,
                    reducedVideoHlsUrl: mediaContent.reducedVideoHlsUrl,
                    shortVideoUrl: mediaContent.shortVideoUrl,
                    shortVideoHlsUrl: mediaContent.shortVideoHlsUrl,
                    privacyOptions: mediaContent.privacyOptions,
                    thumbnailUrl: mediaContent.thumbnailUrl,
                    thumbnail: mediaContent.thumbnail,
                    originalUrl: mediaContent.originalUrl,
                    comments: comments,
                    isLiked: mediaContent.likes.includes(currentUserId),
                    receivedGifts: mediaContent.receivedGifts,
                };
                return mediaContentResponse;
            })
        );
        const count = await MediaModel.countDocuments(mediaContentQuery);
        return Response.successResponse(res, "", {
            data: mediaContentsResponse,
            total: count,
        });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function muteUser(req, res) {
    try {
        const currentUserId = req.user.id;
        const muteUser = req.params.id;
        let currUser = await userModel.findOne({ _id: currentUserId });

        let index = currUser.mutedUsers?.findIndex(
            (mutedUser) => mutedUser == muteUser
        );
        if (index != -1) {
            currUser.mutedUsers.splice(index, 1);
        } else {
            currUser.mutedUsers.push(muteUser);
        }
        await currUser.save();
        return Response.successResponse(res);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function blockUser(req, res) {
    try {
        const currentUserId = req.user.id;
        const blockUser = req.params.id;
        let currUser = await userModel.findOne({ _id: currentUserId });
        let index = currUser.blockedUsers?.findIndex(
            (blockedUser) => blockedUser == blockUser
        );
        if (index != -1) {
            currUser.blockedUsers.splice(index, 1);
        } else {
            currUser.blockedUsers.push(blockUser);
        }
        await currUser.save();
        return Response.successResponse(res);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function getNotificationsSettings(req, res) {
    try {
        const userId = req.user.id;
        const user = await userModel
            .findOne({ _id: userId })
            .select(["notificationsSettings"]);

        const userDataDTO = {
            ...user.notificationsSettings,
        };
        return Response.successResponse(res, "", userDataDTO);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function updateNotificationsSettings(req, res) {

    try {
        const { feedback, reminders, news, interactions, followers, messages } =
            req.body;
        const userId = req.user.id;
        const user = await userModel
            .findOne({ _id: userId })
            .select(["notificationsSettings"]);

        user.notificationsSettings.feedback = feedback;
        user.notificationsSettings.reminders = reminders;
        user.notificationsSettings.news = news;
        user.notificationsSettings.interactions = interactions;
        user.notificationsSettings.followers = followers;
        user.notificationsSettings.messages = messages;
        await user.save();

        return Response.successResponse(res, "");
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function updatePrivacySettings(req, res) {
    try {
        const {
            shareMedia,
            downloadMedia,
            viewFollowing,
            viewFollowers,
            messages,
            viewLikedVideos,
            activityStatus
        } = req.body;
        const userId = req.user.id;
        const user = await userModel
            .findOne({ _id: userId })
            .select(["privacyOptions"]);

        user.privacyOptions.shareMedia = shareMedia;
        user.privacyOptions.downloadMedia = downloadMedia;
        user.privacyOptions.viewFollowing = viewFollowing;
        user.privacyOptions.viewFollowers = viewFollowers;
        user.privacyOptions.viewLikedVideos = viewLikedVideos;
        user.privacyOptions.messages = messages;
        user.privacyOptions.activityStatus = activityStatus;
        await user.save();

        return Response.successResponse(res, "");
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}
async function getPrivacySettings(req, res) {
    try {
        const userId = req.user.id;
        const user = await userModel
            .findOne({ _id: userId })
            .select(["privacyOptions"]);

        const userDataDTO = {
            ...user.privacyOptions,
        };
        return Response.successResponse(res, "", userDataDTO);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function validateUsername(req, res) {
    const username = req.params.username;
    const isUsernameTaken = await userModel.findOne({ username: username });
    if (isUsernameTaken) {

        let suggestions = [];
        let i = 1;
        while (suggestions.length < 3) {
            let suggestedUsername = username + i;
            let isSuggestedUsernameTaken = await userModel.findOne({ username: suggestedUsername });
            if (!isSuggestedUsernameTaken) {
                suggestions.push(suggestedUsername);
            }
            i++;
        }
        return Response.badReqResponse(res, "Username already used!", suggestions);
    }
    if (username.length < constants.usernameMinLength || username.length > constants.usernameMaxLength) {
        return Response.badReqResponse(res, "Username length must be between 2 and 64 chars");
    }
    return Response.successResponse(res, "Username is valid!");
}

async function editUserProfile(req, res) {
    try {
        // get user info
        const userProfile = await userModel.findOne({ _id: req.user.id });

        const { name, bio, birthday, country, contactEmail, website, username, accountType, businessCategory } = req.body;

        if (name) {
            if (name.trim() === "") {
                return Response.forbiddenResponse(res);
            }
            const newName = removeNonAlphanumericChars(name); // Remove Non-Alphanumeric & double spaces.
            if (
                newName.length < constants.nameMinLength ||
                constants.nameMaxLength > 20
            ) {
                return Response.badReqResponse(
                    res,
                    `Name must be greater than ${constants.nameMinLength} and less than ${constants.nameMaxLength} characters without non-alphanumeric chars!`
                );
            }
            // update user name
            userProfile.name = newName;
        }

        if (bio) {
            userProfile.bio = bio;
        }

        if (birthday) {
            userProfile.dateOfBirth = new Date(`<${birthday}>`);
        }

        if (accountType) {
            userProfile.accountType = accountType;
        }

        if (country) {
            userProfile.country = country;
        }

        if (contactEmail) {
            userProfile.contactEmail = contactEmail;
        }

        if (website) {
            userProfile.website = website;
        }

        if (businessCategory) {
            userProfile.businessCategory = businessCategory;
        }

        if (username) {
            if (userProfile.lastUsernameUpdateDate) {
                const lastUsernameUpdateDate = new Date(userProfile.lastUsernameUpdateDate);
                const todayDate = new Date();

                // To calculate the time difference of two dates
                const DifferenceInTime = todayDate.getTime() - lastUsernameUpdateDate.getTime();

                // To calculate the no. of days between two dates
                const DifferenceInDays = DifferenceInTime / (1000 * 3600 * 24);

                if (DifferenceInDays < 30) {
                    return Response.badReqResponse(
                        res,
                        "You can only update your username once every 30 days"
                    );
                }
            }

            const isUsernameTaken = await userModel.findOne({ username: username });
            const isReservedUsername = await ReservedModel.findOne({ username: username })
            if (isUsernameTaken || isReservedUsername) {
                return Response.badReqResponse(res, "Username already used!");
            }
            if (username.length < constants.usernameMinLength || username.length > constants.usernameMaxLength) {
                return Response.badReqResponse(res, "Username length must be between 2 and 64 chars");
            }

            userProfile.lastUsernameUpdateDate = Date.now();
            userProfile.username = username
        }


        // Avatar
        if (req.files && req.files.avatar) {
            const { avatar } = req.files;
            // check if file extension is allowed
            const avatarExtension = getFileExtension(avatar.name);
            logger.i(
                `image Extension : ${avatarExtension} ${constants.allowedImages.jpg} ${avatarExtension === constants.allowedImages.jpg
                }`
            );
            if (
                avatarExtension === constants.allowedImages.jpg ||
                avatarExtension === constants.allowedImages.jpeg ||
                avatarExtension === constants.allowedImages.png
            ) {
                // check file mimetype
                if (checkIfImageFile(avatar)) {

                    const avatarFileName = `${new Date().getTime()}-${randomString(
                        10
                    )}.${avatarExtension}`;
                    const avatarFile = `${constants.uploadFile}${avatarFileName}`;
                    // check file size
                    await avatar.mv(avatarFile);
                    const avatarFilesize = fs.statSync(avatarFile).size / 1024; // kb
                    if (avatarFilesize <= constants.avatarMaxSize) {
                        logger.i(`Avatar size: ${avatarFilesize}`);
                        // upload avatar to AWS
                        const s3 = new AWS.S3({
                            accessKeyId: config.aws.access_key_id,
                            secretAccessKey: config.aws.secret_access_key,
                            region: config.aws.region,
                            signatureVersion: config.aws.signatureVersion,
                        });

                        const s3Params = {
                            Bucket: config.aws.bucket_name,
                            Key: avatarFileName,
                            Expires: 60 * 60,
                            ContentType: constants.contentType.imagePNG,
                            ACL: "public-read",
                        };

                        s3.getSignedUrl("putObject", s3Params);

                        const fileContent = fs.readFileSync(avatarFile);

                        const params = {
                            Bucket: config.aws.bucket_name,
                            Key: avatarFileName,
                            Body: fileContent,
                            ACL: "public-read",
                        };
                        await s3.upload(params).promise();

                        logger.i(
                            `user profile avatar : https://${config.aws.bucket_name}.s3.amazonaws.com/${avatarFileName}`
                        );
                        try {
                            // remove draft files
                            fs.unlinkSync(avatarFile);
                        } catch (err) {
                            console.error(err);
                        }
                        // update
                        userProfile.avatar = `https://${config.aws.bucket_name}.s3.amazonaws.com/${avatarFileName}`;
                    } else {
                        return Response.badReqResponse(
                            res,
                            `Avatar max size is ${constants.avatarMaxSize} KB`
                        );
                    }
                } else {
                    return Response.badReqResponse(res, "Avatar not an image file.");
                }
            } else {
                return Response.badReqResponse(
                    res,
                    "Avatar allowed files: JPG, JPEG, PNG."
                );
            }
        }

        // Cover image
        if (req.files && req.files.cover) {
            const { cover } = req.files;
            const coverExtension = getFileExtension(cover.name);
            logger.i(
                `image Extension : ${coverExtension} ${constants.allowedImages.jpg} ${coverExtension === constants.allowedImages.jpg
                }`
            );
            if (
                coverExtension === constants.allowedImages.jpg ||
                coverExtension === constants.allowedImages.jpeg ||
                coverExtension === constants.allowedImages.png
            ) {
                // check file mimetype
                if (checkIfImageFile(cover)) {
                    const coverFileName = `${new Date().getTime()}-${randomString(
                        10
                    )}.${coverExtension}`;
                    const coverFile = `${constants.uploadFile}${coverFileName}`;
                    // check file size
                    await cover.mv(coverFile);
                    const coverFileSize = fs.statSync(coverFile).size / 1024; // kb
                    if (coverFileSize <= constants.avatarMaxSize) {
                        logger.i(`cover size: ${coverFileSize}`);
                        // upload avatar to AWS
                        const s3 = new AWS.S3({
                            accessKeyId: config.aws.access_key_id,
                            secretAccessKey: config.aws.secret_access_key,
                            region: config.aws.region,
                            signatureVersion: config.aws.signatureVersion,
                        });
                        const s3Params = {
                            Bucket: config.aws.bucket_name,
                            Key: coverFileName,
                            Expires: 60 * 60,
                            ContentType: constants.contentType.imagePNG,
                            ACL: "public-read",
                        };
                        s3.getSignedUrl("putObject", s3Params);
                        const fileContent = fs.readFileSync(coverFile);
                        const params = {
                            Bucket: config.aws.bucket_name,
                            Key: coverFileName,
                            Body: fileContent,
                            ACL: "public-read",
                        };
                        await s3.upload(params).promise();
                        logger.i(
                            `user profile cover : https://${config.aws.bucket_name}.s3.amazonaws.com/${coverFileName}`
                        );
                        try {
                            // remove draft files
                            fs.unlinkSync(coverFile);
                        } catch (err) {
                            console.error(err);
                        }
                        // update
                        userProfile.cover = `https://${config.aws.bucket_name}.s3.amazonaws.com/${coverFileName}`;
                    } else {
                        return Response.badReqResponse(
                            res,
                            `Cover image max size is ${constants.avatarMaxSize} KB`
                        );
                    }
                } else {
                    return Response.badReqResponse(res, "Cover image not an image file.");
                }
            } else {
                return Response.badReqResponse(
                    res,
                    "Cover image allowed files: JPG, JPEG, PNG."
                );
            }
        }

        // save the user updates
        userProfile.save();

        return Response.successResponse(res, "Updated successfully!", {
            name: userProfile.name,
            bio: userProfile.bio,
            avatar: userProfile.avatar,
        });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function getSuggestedUsers(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const { page } = query;
        const userId = req.user.id;
        let pageNum = page ?? 1;
        let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;

        const count = await userModel.countDocuments();
        let result = [];

        // Iterate through all pages of results until we find the required number of users
        while (result.length < constants.pageLimit) {
            const users = await userModel.find({ _id: { $ne: userId }, isDeleted: false })
                .limit(constants.pageLimit)
                .skip(skip)
                .select(['_id', 'name', 'avatar'])

            if (!users.length) {
                break;
            }

            const findQuery = { followed_userID: { $in: users.map(user => user._id) }, follower_userID: userId, isDeleted: false };
            const following = await UserFollowerModel.find(findQuery)
                .select("followed_userID")

            const followingIds = following.map(f => f.followed_userID.toString())

            const newUsers = users.filter(user => followingIds.indexOf(user._id.toString()) == -1)
            result = [...result, ...newUsers];

            // If we have found enough users, exit the loop
            if (result.length >= constants.pageLimit) {
                break;
            }

            // Move to the next page
            pageNum++;
            skip = (pageNum - 1) * constants.pageLimit;
        }

        return Response.successResponse(res, "", { data: result, total: count });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}


async function searchUsers(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const { page, searchQuery } = query;
        const userId = req.user.id;
        let pageNum = page ?? 1;
        let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;
        const dbQuery = {
            _id: { $ne: userId }, isDeleted: false, $or: [{ name: { $regex: new RegExp(searchQuery, "i") } },
            { email: { $regex: new RegExp(searchQuery, "i") } },
            { username: { $regex: new RegExp(searchQuery, "i") } }]
        }
        const count = await userModel.countDocuments(dbQuery);
        const users = await userModel.find(dbQuery).limit(constants.pageLimit).skip(skip).select(['_id', 'name', 'avatar', 'username'])
        if (!users.length) {
            return Response.successResponse(res, "", { data: [], total: count });
        }

        return Response.successResponse(res, "", { data: users, total: count });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}


function performIntersection(arr1, arr2) {
    arr1 = arr1.map((item) => item.toString());
    arr2 = arr2.map((item) => item.toString());
    return intersectionResult = arr1.filter((x) => arr2.indexOf(x) == -1);

    // return intersectionResult.length != 0;
}
module.exports = {
    followUser,
    getUserFollowing,
    getUserFollowers,
    getUserVideos,
    userCollections,
    currentUserData,
    getUserData,
    deleteUser,
    getDeleteAccountReasons,
    getLikedVideos,
    muteUser,
    blockUser,
    getPrivacySettings,
    updateNotificationsSettings,
    updatePrivacySettings,
    getNotificationsSettings,
    editUserProfile,
    getBlockedUsers,
    getSuggestedUsers,
    searchUsers,
    validateUsername,
    getUserProfileVisitors
};
