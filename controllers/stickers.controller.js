const express = require("express");
const Response = require("../utils/response");

const stickersService = require("../services/stickers.service")

const logger = require("../utils/logger");


async function getStickers(req, res) {
  try {
    const query = require("url").parse(req.url, true).query;
    const { sticker_type } = query;
    if (sticker_type == null || sticker_type.trim() === "") {
      return Response.serverErrorResponse(res);
    }

    const stickers = await stickersService.getStickers(query);

    if (stickers != null && stickers.length > 0)
      return Response.successResponse(res, "", stickers);

    return Response.badReqResponse(res);
  } catch (error) {
    logger.e(error);
    return Response.serverErrorResponse(res, "", error);
  }
}

module.exports = {
  getStickers,
};
