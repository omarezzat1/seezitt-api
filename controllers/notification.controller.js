const Response = require("../utils/response");
const notificationService = require("../services/notification.service")

async function getNotifications(req, res) {
    try {
        const currentUserId = req.user.id;
        const response = await notificationService.getNotifications(currentUserId);
        return Response.successResponse(res, "", response);
    } catch (error) {
        return Response.serverErrorResponse(res, error);
    }
}
module.exports = {
    getNotifications
};
