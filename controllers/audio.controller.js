const express = require('express');
const Response = require('../utils/response');

let soundGallery = require("../models/sound_gallery_model");
let artistGallery = require("../models/sound_artist_model");
const audioService = require("../services/audo.service")

let Constants = require("../utils/constants");


async function getSoundGallery(req, res) {
    try {
        const data = await audioService.getSoundGallery({ page: req.query.page })
        return Response.successResponse(res, "", data);
    } catch (error) {
        logger.e(error);
        return Response.serverErrorResponse(res, "", error);
    }
};

module.exports = {
    getSoundGallery
};

