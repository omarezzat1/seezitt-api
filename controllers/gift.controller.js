const Response = require("../utils/response");
const giftService = require("../services/gift.service")

async function getAll(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const gifts = await giftService.getAll(query);
        return Response.successResponse(res, "", gifts);
    } catch (error) {
        return Response.serverErrorResponse(res, "", error.message);
    }
}

async function getGiftById(req, res) {
    try {
        const gift = await giftService.getGiftById(req.params.id);
        return Response.successResponse(res, "", gift);
    } catch (error) {
        return Response.serverErrorResponse(res, "", error);
    }
}

async function sendGift(req, res) {
    try {
        const { giftId, mediaId } = req.body;
        const currUser = req.user.id;
        const gift = await giftService.sendGift(currUser, giftId, mediaId);
        return Response.successResponse(res, "", gift);
    } catch (error) {
        return Response.serverErrorResponse(res, "", error.message);
    }
}

async function createGift(req, res) {
    try {
        const { name, price, description, imageUrl } = req.body;
        const gift = await giftService.createGift(name, price, description, imageUrl);
        return Response.successResponse(res, "", gift);
    } catch (error) {
        return Response.serverErrorResponse(res, "", error);
    }
}

async function markGiftAsSeen(req, res) {
    try {
        const {giftId,mediaId} = req.body;
        await giftService.markGiftAsSeen(giftId,mediaId);
        return Response.successResponse(res, "");
    } catch (error) {
        return Response.serverErrorResponse(res, "", error.message);
    }
}


module.exports = {
    getAll,
    getGiftById,
    sendGift,
    createGift,
    markGiftAsSeen
};
