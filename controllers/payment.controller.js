const Response = require("../utils/response");
const paypal = require('paypal-rest-sdk');
const { v4: uuidv4 } = require('uuid');
const sessionModel = require("../models/payment_session_model");
const PaymentStatus = require("../models/payment_session_model").PaymentStatus;
const paymentModel = require("../models/payment_model");
const coinTransactionModel = require("../models/coin_transaction_model");
const TransactionType = require("../models/coin_transaction_model").TransactionType;
const userModel = require("../models/user_model");

paypal.configure({
    mode: process.env.PAYPAL_MODE,
    client_id: process.env.PAYPAL_CLIENT_ID,
    client_secret: process.env.PAYPAL_CLIENT_SECRET
});
async function buyCoins(req, res) {
    const userId = req.user.id
    const coinsAmount = req.body.coinsAmount;

    const sessionId = uuidv4()
    req.session.sessionId = sessionId;

    const price = (coinsAmount * 0.01).toFixed(2);

    await sessionModel.create({
        userId,
        sessionId,
        coinsAmount,
        price,
        createdTime: Date.now(),

    });

    const createPaymentJson = {
        intent: 'sale',
        payer: {
            payment_method: 'paypal',
        },
        redirect_urls: {
            return_url: process.env.PAYPAL_PAYMENT_SUCCESS_CALLBACK,
            cancel_url: process.env.PAYMENT_PAYMENT_CANCEL_CALLBACK,
        },
        transactions: [
            {
                item_list: {
                    items: [
                        {
                            name: 'Coins',
                            sku: '001',
                            price: price,
                            currency: 'USD',
                            quantity: 1,
                        },
                    ],
                },
                amount: {
                    currency: 'USD',
                    total: price,
                },
                description: `Purchase ${coinsAmount} coins`
            },
        ],
    };

    paypal.payment.create(createPaymentJson, (error, payment) => {
        if (error) {
            throw error;
        } else {
            for (let i = 0; i < payment.links.length; i += 1) {
                if (payment.links[i].rel === 'approval_url') {
                    res.redirect(payment.links[i].href);
                }
            }
        }
    });
}

async function buyCoinsV2(req, res) {
    const userId = req.user.id
    const coinsAmount = req.params.amount;

    const sessionId = uuidv4()
    req.session.sessionId = sessionId;

    const price = (coinsAmount * 0.01).toFixed(2);

    await sessionModel.create({
        userId,
        sessionId,
        coinsAmount,
        price,
        createdTime: Date.now(),

    });

    const createPaymentJson = {
        intent: 'sale',
        payer: {
            payment_method: 'paypal',
        },
        redirect_urls: {
            return_url: process.env.PAYPAL_PAYMENT_SUCCESS_CALLBACK,
            cancel_url: process.env.PAYMENT_PAYMENT_CANCEL_CALLBACK,
        },
        transactions: [
            {
                item_list: {
                    items: [
                        {
                            name: 'Coins',
                            sku: '001',
                            price: price,
                            currency: 'USD',
                            quantity: 1,
                        },
                    ],
                },
                amount: {
                    currency: 'USD',
                    total: price,
                },
                description: `Purchase ${coinsAmount} coins`
            },
        ],
    };

    paypal.payment.create(createPaymentJson, (error, payment) => {
        if (error) {
            throw error;
        } else {
            for (let i = 0; i < payment.links.length; i += 1) {
                if (payment.links[i].rel === 'approval_url') {
                    res.redirect(payment.links[i].href);
                }
            }
        }
    });
}


async function successCallback(req, res) {
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;
    const sessionId = req.session.sessionId;


    const session = await sessionModel.findOne({ sessionId: sessionId })
    const executePaymentJson = {
        payer_id: payerId,
        transactions: [
            {
                amount: {
                    currency: 'USD',
                    total: session.price,
                },
            },
        ],
    };

    paypal.payment.execute(paymentId, executePaymentJson, async (error, payment) => {
        if (error) {
            console.log(error.response);
            session.status = PaymentStatus.FAILED;
            await session.save();
            res.redirect('/api/payment/coins/failed')
            throw error;
        } else {

            await paymentModel.create({
                userId: session.userId,
                paymentId: payment.id,
                state: payment.state,
                createTime: payment.create_time,
                updateTime: payment.update_time,
                payer: {
                    email: payment.payer.payer_info.email,
                    payerId: payment.payer.payer_info.payer_id,
                },
                transactions: payment.transactions,
                links: payment.transactions[0].related_resources[0].sale.links,
                createdTime: Date.now(),
            });
            await coinTransactionModel.create({
                sourceUserId: session.userId,
                destinationUserId: session.userId,
                transactionId: uuidv4(),
                amount: session.coinsAmount,
                type: TransactionType.PURCHASE,
                createdTime: Date.now(),
            });
            session.status = PaymentStatus.COMPLETED;
            await session.save();
            await userModel.updateOne({ _id: session.userId }, { $inc: { balance: session.coinsAmount } });

            res.redirect('/api/payment/coins/success')
        }
    });
}
module.exports = {
    buyCoins,
    buyCoinsV2,
    successCallback
};
