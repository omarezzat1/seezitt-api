const Response = require("../utils/response");
const appControlService = require("../services/app-control.service")

async function checkCurrentAppVersion(req, res) {
    try {
        const version = req.params.version;
        const versionStatus = await appControlService.checkAppVersion(version)

        if (!versionStatus) {
            Response.badReqResponse(res, "Invalid app version")
        }

        return Response.successResponse(res, "", versionStatus);
    } catch (error) {
        return Response.serverErrorResponse(res, "", error);
    }
}
module.exports = {
    checkCurrentAppVersion
};
