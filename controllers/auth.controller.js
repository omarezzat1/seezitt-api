const express = require("express");
const passport = require("passport");
const fs = require("fs");
const AWS = require("aws-sdk");
const bcrypt = require("bcrypt");
const Response = require("../utils/response");
const config = require("../configs/config");
const constants = require("../utils/constants");
const inputValidation = require("../utils/input_validation");
const tokenCredentials = require("../utils/token");
const SendGridService = require("../services/send-grid.service");


const logger = require("../utils/logger");

const User = require("../models/user_model");
const ReservedUsernameModel = require("../models/reserved_username_model");
const DeviceToken = require("../models/device_tokens_model");
const PasswordResetTokens = require("../models/password_tokens_model"); // change password req
const EmailChange = require("../models/email_change_model");
const MediaCategories = require("../models/media_categories_model");

const Constants = require("../utils/constants");

const UserFollowerModel = require("../models/user_followers_model");

require("../utils/custom")();

/**
 * @swagger
 * /signIn:
 *   post:
 *     summary: Sign in authentication
 *     tags: [authentication]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *            type: object
 *            required:
 *              - email
 *              - password
 *            properties:
 *              email:
 *                type: email
 *                default: 'amosa@ogoul.com'
 *              password:
 *                type: string
 *                default: Action@8765
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Empty, error or Not Found
 *      401:
 *        description: Unauthorized Request
 *      403:
 *        description: Forbidden
 *      500:
 *        description: Response Error
 */
async function signIn(req, res) {
  try {
    logger.i(req);
    const { email, password } = req.body;
    if (email == null || email.trim() === "") {
      return Response.badReqResponse(res, "Please add your email first!");
    }
    if (password == null || password.trim() === "" || password.length > 64) {
      return Response.badReqResponse(res, "Please add your password first!");
    }
    const userExists = await User.findOne({
      email: { $regex: `^${email}$`, $options: "i" },
    });
    if (userExists) {
      const comparePasswords = await userExists.comparePassword(password);
      if (userExists && comparePasswords) {
        // generate user token

        if (
          userExists.deletedDetails &&
          userExists.deletedDetails.deletedDate
        ) {
          const deletedDate = new Date(userExists.deletedDetails.deletedDate);
          const todayDate = new Date();

          // To calculate the time difference of two dates
          const DifferenceInTime = todayDate.getTime() - deletedDate.getTime();

          // To calculate the no. of days between two dates
          const DifferenceInDays = DifferenceInTime / (1000 * 3600 * 24);

          if (DifferenceInDays > 30) {
            return Response.badReqResponse(
              res,
              "This account is already deleted!"
            );
          }

          userExists.deletedDetails.deletedDate = null;
          userExists.isDeleted = false;
          await userExists.save();

          await UserFollowerModel.updateMany(
            { followed_userID: userExists._id },
            { isDeleted: false }
          );
          await UserFollowerModel.updateMany(
            { follower_userID: userExists._id },
            { isDeleted: false }
          );
        }
        const jwtToken = tokenCredentials.generateToken(
          userExists.id,
          userExists.email
        );
        // save user token
        const deviceToken = new DeviceToken();
        deviceToken.user_id = userExists.id;
        deviceToken.token = jwtToken;
        await deviceToken.save();
        // add token to user model
        userExists.token = jwtToken;
        //
        userExists.password = "";
        return Response.successResponse(res, "Welcome back!", userExists);
      }
    }
    return Response.badReqResponse(res, "User information not valid!");
  } catch (error) {
    logger.d(error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function updateNotificationToken(req, res) {
  try {
    const { pushNotificationToken } = req.body;
    const userId = req.user.id;

    await User.updateOne(
      { _id: userId },
      { pushNotificationToken: pushNotificationToken }
    );
    return Response.successResponse(res, "Token added successfully!");
  } catch (error) {
    logger.d(error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function setLoginCookie(req, res) {
  try {
    const userId = req.user.id;
    const token = req.headers.authorization.split(" ").pop();
    logger.i(
      `setLoginCookie body params : user_id ${userId} ### token ${token}`
    );
    const deviceToken = await DeviceToken.findOne({ userId, token });
    const userExists = await User.findOne({ _id: userId });
    logger.i(`deviceToken userId ${deviceToken.user_id}`);
    logger.i(`userExists userId ${userExists.id}`);
    if (userExists) {
      req.session.login = true;
      return Response.successResponse(res, "User login successfully!");
    }
    return Response.badReqResponse(res);
  } catch (error) {
    logger.d(error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function checkEmailExistsBefore(req, res) {
  try {
    const { email } = req.body;
    if (email == null || email.trim() === "") {
      return Response.badReqResponse(res, "Please add your email first!");
    }
    if (!inputValidation.validateMail(email)) {
      return Response.badReqResponse(res, "Please add your email correctly!");
    }
    const emailExistsBefore = await User.findOne({
      email: { $regex: `^${email}$`, $options: "i" },
    });
    if (emailExistsBefore) {
      return Response.badReqResponse(res, "User email already exists!");
    }
    return Response.successResponse(res, "Not Exists before");
  } catch (error) {
    logger.d(error);
    return Response.serverErrorResponse(res, "", error);
  }
}

/**
 * @swagger
 * /signUp:
 *   post:
 *     summary: Sign up authentication
 *     tags: [authentication]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *            type: object
 *            required:
 *              - email
 *              - name
 *              - password
 *            properties:
 *              name:
 *                type: string
 *                default: Abdullah Mosa
 *              email:
 *                type: email
 *                default: 'amosa@ogoul.com'
 *              password:
 *                type: string
 *                default: Action@8765
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Empty, error or Not Found
 *      401:
 *        description: Unauthorized Request
 *      403:
 *        description: Forbidden
 *      500:
 *        description: Response Error
 */
async function signUp(req, res) {
  try {
    const { email, name, password, username } = req.body;
    if (email == null || email.trim() === "") {
      return Response.badReqResponse(res, "Please add your email first!");
    }
    if (!inputValidation.validateMail(email)) {
      return Response.badReqResponse(res, "Please add your email correctly!");
    }
    if (name == null || name.trim() === "") {
      return Response.badReqResponse(res, "Please add your name first!");
    }
    if (password == null || password.trim() === "") {
      return Response.badReqResponse(res, "Please add your password first!");
    }
    if (!inputValidation.validatePassword(password)) {
      return Response.badReqResponse(
        res,
        "Please follow password requirements!"
      );
    }
    const emailExistsBefore = await User.findOne({
      email: { $regex: `^${email}$`, $options: "i" },
    });
    if (emailExistsBefore) {
      return Response.badReqResponse(res, "User email already exists!");
    }
    // create user
    const user = new User();
    if (username == null || username.trim() === "") {
      user.username = name.split(' ')[0].toLowerCase() + user._id.toString().slice(-6);
    }
    else {
      const isUsernameTaken = await User.findOne({ username: username });
      const isReservedUsername = await ReservedUsernameModel.findOne({ username: username })
      if (isUsernameTaken || isReservedUsername) {
        return Response.badReqResponse(res, "Username already used!");
      }
      if (username.length < constants.usernameMinLength || username.length > constants.usernameMaxLength) {
        return Response.badReqResponse(res, "Username length must be between 2 and 64 chars");
      }
      user.username = username
    }

    user.name = name;
    user.email = email;
    //Encrypt password
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    user.role = Constants.userRole.User;
    await user.save();
    // generate user token
    const jwtToken = tokenCredentials.generateToken(user.id, user.email);
    // add token to user
    const deviceToken = new DeviceToken({ user_id: user.id, token: jwtToken });
    await deviceToken.save();
    // add token to user model
    user.token = jwtToken;
    //
    user.password = "";

    const title = "Seezitt - Account registered successfully";
    const html = `<p>Hi ${user.name},</p>
      <p><br></p>
      <p>Your account has been registered successfully!</p>
      <p><br></p>
      <p>Sincerely yours,</p>
      <p>Seezitt Team</p>`
    await SendGridService.sendEmailHtml(email, title, html);

    return Response.successResponse(res, "User register successfully!", user);
  } catch (error) {
    logger.d(error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function requestResetPassword(req, res) {
  try {

    const { email } = req.body;
    if (email == null || email.trim() === "") {
      return Response.badReqResponse(res, "Please add your email first!");
    }
    const userExists = await User.findOne({
      email: { $regex: `^${email}$`, $options: "i" },
    });

    if (!userExists) {
      return Response.badReqResponse(res, "Account does not exist!");
    }

    // Generate token
    const token = tokenCredentials.generateToken(
      userExists.id,
      userExists.email
    );
    // Save password reset req credintials
    const passwordResetTokens = new PasswordResetTokens();
    passwordResetTokens.user_id = userExists.id;
    passwordResetTokens.email = userExists.email;
    passwordResetTokens.token = token;
    passwordResetTokens.save();

    const title = "Seezitt - Reset Password";
    const websiteUrl = process.env.WEBSITE_URL;

    const html = `<p>Hi ${userExists.name},</p>
      <p><br></p>
      <p>Forgot your password?</p>
      <p><br></p>
      <p>We recieved a request to rest the password for your account.</p>
      <p><br></p>
      <p>To reset your password, click on the link below:</p>
      <p><br></p>
      <p>${websiteUrl}/retrieve/${email}/${token}</p>
      <p><br></p>
      <p>Sincerely yours,</p>
      <p>Seezitt Support Team</p>`
    await SendGridService.sendEmailHtml(email, title, html);

    return Response.successResponse(
      res,
      "Please check your email [ Sometimes email sent to Junk/Spam ]!"
    );
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

/**
 * @swagger
 * /setNewPassword:
 *   post:
 *     summary: Retrieve user password [ step 2 ]
 *     tags: [authentication]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *            type: object
 *            required:
 *              - email
 *              - password
 *              - token
 *            properties:
 *              email:
 *                type: email
 *                default: 'amosa@ogoul.com'
 *              password:
 *                type: email
 *                default: 'new user password'
 *              token:
 *                type: email
 *                default: 'retrieve token from deep-link url'
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Empty, error or Not Found
 *      401:
 *        description: Unauthorized Request
 *      403:
 *        description: Forbidden
 *      500:
 *        description: Response Error
 */
async function setNewPassword(req, res) {
  try {
    const { password, email, token } = req.body;
    if (password == null || password.trim() === "") {
      return Response.badReqResponse(res, "Please add new password first!");
    }
    if (!inputValidation.validatePassword(password)) {
      return Response.badReqResponse(
        res,
        "Please follow password requirements!"
      );
    }
    if (email == null || email.trim() === "") {
      return Response.forbiddenResponse(res);
    }
    if (token == null || token.trim() === "") {
      return Response.forbiddenResponse(res);
    }
    const userExists = await User.findOne({
      email: { $regex: `^${email}$`, $options: "i" },
    });
    logger.dWithObj("userExists", userExists);
    if (userExists) {
      // check credential req is valid
      // eslint-disable-next-line max-len
      const isTokenValid = await PasswordResetTokens.findOne({
        email,
        token,
        user_id: userExists.id,
      });
      logger.dWithObj("isTokenValid", isTokenValid);
      if (isTokenValid) {
        logger.i("inside isTokenValid");
        // update user password

        //Encrypt password
        const salt = await bcrypt.genSalt(10);
        userExists.password = await bcrypt.hash(password, salt);

        userExists.save();
        // remove req credential
        isTokenValid.remove();
        return Response.successResponse(
          res,
          "Your password updates successfully!",
          logger.debugData({ new_password: password })
        );
      }
      return Response.forbiddenResponse(
        res,
        "New password request is not valid!"
      );
    }
    return Response.successResponse(res, "User information not valid!");
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function requestChangeEmail(req, res) {
  try {
    const { email } = req.body;
    if (email == null || email.trim() === "") {
      return Response.serverErrorResponse(res);
    }

    const userExists = await User.findOne({ _id: req.user.id });

    if (userExists) {
      const emailExistsBefore = await User.findOne({
        email: { $regex: `^${email}$`, $options: "i" },
        _id: { $ne: req.user.id },
      });

      if (emailExistsBefore) {
        return Response.badReqResponse(
          res,
          "E-mail you try change to is belong to another account!"
        );
      }

      const token = tokenCredentials.generateToken(
        userExists.id,
        userExists.email
      );

      // reset previous change requests
      await EmailChange.find({ user_id: req.user.id }).remove();
      // Save password reset req credentials
      const emailChange = new EmailChange();
      emailChange.user_id = userExists.id;
      emailChange.email = email;
      emailChange.token = token;
      emailChange.save();

      logger.i(emailChange);

      smtpEmail.sendEmail(
        userExists.email,
        `Change Account Email #${Date.now()}`,
        `<a href="${config.baseUrl}changeEmail/${emailChange.email}/${emailChange.token}">Click here</a>`
      );

      return Response.successResponse(
        res,
        "Please check your email [ Sometimes email sent to Junk/Spam ]!",
        logger.debugData({ token })
      );
    }
    return Response.serverErrorResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function setNewEmail(req, res) {
  try {
    const { email, token } = req.body;
    if (email == null || email.trim() === "") {
      return Response.forbiddenResponse(res);
    }
    if (!inputValidation.validateMail(email)) {
      return Response.badReqResponse(res, "Please add your email correctly!");
    }
    if (token == null || token.trim() === "") {
      return Response.forbiddenResponse(res);
    }

    const userExists = await User.findOne({ _id: req.user.id });

    logger.i(userExists);

    if (userExists) {
      const emailChange = await EmailChange.findOne({
        email,
        token,
        user_id: req.user.id,
      });
      if (emailChange) {
        userExists.email = emailChange.email;
        userExists.save();
        await EmailChange.findOne({ user_id: req.user.id }).remove();
        return Response.successResponse(
          res,
          "Your email updated successfully!",
          logger.debugData({ new_email: emailChange.email })
        );
      }
    }
    return Response.badReqResponse(
      res,
      "Your request is not valid or expired!"
    );
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function editUserProfile(req, res) {
  try {
    // get user info
    const userProfile = await User.findOne({ _id: req.user.id });

    const { name, bio, birthday, country, contactEmail, website } = req.body;

    if (name) {
      if (name.trim() === "") {
        return Response.forbiddenResponse(res);
      }
      const newName = removeNonAlphanumericChars(name); // Remove Non-Alphanumeric & double spaces.
      if (
        newName.length < constants.nameMinLength ||
        constants.nameMaxLength > 20
      ) {
        return Response.badReqResponse(
          res,
          `Name must be greater than ${constants.nameMinLength} and less than ${constants.nameMaxLength} characters without non-alphanumeric chars!`
        );
      }
      // update user name
      userProfile.name = newName;
    }

    // logger.i(userProfile);
    if (bio) {
      userProfile.bio = bio;
    }

    if (birthday) {
      userProfile.dateOfBirth = new Date(`<${birthday}>`);
    }

    if (country) {
      userProfile.country = country;
    }

    if (contactEmail) {
      userProfile.contactEmail = contactEmail;
    }

    if (website) {
      userProfile.website = website;
    }

    // Avatar
    if (req.files && req.files.avatar) {
      const { avatar } = req.files;
      // check if file extension is allowed
      const avatarExtension = getFileExtension(avatar.name);
      logger.i(
        `image Extension : ${avatarExtension} ${constants.allowedImages.jpg} ${avatarExtension === constants.allowedImages.jpg
        }`
      );
      if (
        avatarExtension === constants.allowedImages.jpg ||
        avatarExtension === constants.allowedImages.jpeg ||
        avatarExtension === constants.allowedImages.png
      ) {
        // check file mimetype
        if (checkIfImageFile(avatar)) {
          const avatarFileName = `${new Date().getTime()}-${randomString(
            10
          )}.${avatarExtension}`;
          const avatarFile = `${constants.uploadFile}${avatarFileName}`;
          // check file size
          await avatar.mv(avatarFile);
          const avatarFilesize = fs.statSync(avatarFile).size / 1024; // kb
          if (avatarFilesize <= constants.avatarMaxSize) {
            logger.i(`Avatar size: ${avatarFilesize}`);
            // upload avatar to AWS
            const s3 = new AWS.S3({
              accessKeyId: config.aws.access_key_id,
              secretAccessKey: config.aws.secret_access_key,
              region: config.aws.region,
              signatureVersion: config.aws.signatureVersion,
            });
            const s3Params = {
              Bucket: config.aws.bucket_name,
              Key: avatarFileName,
              Expires: 60 * 60,
              ContentType: constants.contentType.imagePNG,
              ACL: "public-read",
            };
            s3.getSignedUrl("putObject", s3Params);
            const fileContent = fs.readFileSync(avatarFile);
            const params = {
              Bucket: config.aws.bucket_name,
              Key: avatarFileName,
              Body: fileContent,
              ACL: "public-read",
            };
            await s3.upload(params).promise();
            logger.i(
              `user profile avatar : https://${config.aws.bucket_name}.s3.amazonaws.com/${avatarFileName}`
            );
            try {
              // remove draft files
              fs.unlinkSync(avatarFile);
            } catch (err) {
              console.error(err);
            }
            // update
            userProfile.avatar = `https://${config.aws.bucket_name}.s3.amazonaws.com/${avatarFileName}`;
          } else {
            return Response.badReqResponse(
              res,
              `Avatar max size is ${constants.avatarMaxSize} KB`
            );
          }
        } else {
          return Response.badReqResponse(res, "Avatar not an image file.");
        }
      } else {
        return Response.badReqResponse(
          res,
          "Avatar allowed files: JPG, JPEG, PNG."
        );
      }
    }

    // Cover image
    if (req.files && req.files.cover) {
      const { cover } = req.files;
      const coverExtension = getFileExtension(cover.name);
      logger.i(
        `image Extension : ${coverExtension} ${constants.allowedImages.jpg} ${coverExtension === constants.allowedImages.jpg
        }`
      );
      if (
        coverExtension === constants.allowedImages.jpg ||
        coverExtension === constants.allowedImages.jpeg ||
        coverExtension === constants.allowedImages.png
      ) {
        // check file mimetype
        if (checkIfImageFile(cover)) {
          const coverFileName = `${new Date().getTime()}-${randomString(
            10
          )}.${coverExtension}`;
          const coverFile = `${constants.uploadFile}${coverFileName}`;
          // check file size
          await cover.mv(coverFile);
          const coverFileSize = fs.statSync(coverFile).size / 1024; // kb
          if (coverFileSize <= constants.avatarMaxSize) {
            logger.i(`cover size: ${coverFileSize}`);
            // upload avatar to AWS
            const s3 = new AWS.S3({
              accessKeyId: config.aws.access_key_id,
              secretAccessKey: config.aws.secret_access_key,
              region: config.aws.region,
              signatureVersion: config.aws.signatureVersion,
            });
            const s3Params = {
              Bucket: config.aws.bucket_name,
              Key: coverFileName,
              Expires: 60 * 60,
              ContentType: constants.contentType.imagePNG,
              ACL: "public-read",
            };
            s3.getSignedUrl("putObject", s3Params);
            const fileContent = fs.readFileSync(coverFile);
            const params = {
              Bucket: config.aws.bucket_name,
              Key: coverFileName,
              Body: fileContent,
              ACL: "public-read",
            };
            await s3.upload(params).promise();
            logger.i(
              `user profile cover : https://${config.aws.bucket_name}.s3.amazonaws.com/${coverFileName}`
            );
            try {
              // remove draft files
              fs.unlinkSync(coverFile);
            } catch (err) {
              console.error(err);
            }
            // update
            userProfile.cover = `https://${config.aws.bucket_name}.s3.amazonaws.com/${coverFileName}`;
          } else {
            return Response.badReqResponse(
              res,
              `Cover image max size is ${constants.avatarMaxSize} KB`
            );
          }
        } else {
          return Response.badReqResponse(res, "Cover image not an image file.");
        }
      } else {
        return Response.badReqResponse(
          res,
          "Cover image allowed files: JPG, JPEG, PNG."
        );
      }
    }

    // save the user updates
    userProfile.save();

    return Response.successResponse(res, "Updated successfully!", {
      name: userProfile.name,
      bio: userProfile.bio,
      avatar: userProfile.avatar,
    });
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getRefreshToken(req, res) {
  try {
    const authHeader = req.headers.authorization;
    const token = authHeader.split(" ").pop();
    const deviceToken = await DeviceToken.findOneAndUpdate(
      { token, isDeleted: false },
      { isDeleted: true }
    );
    if (!deviceToken) {
      return Response.unauthorizedReqResponse(res);
    }
    const user = await User.findOne({ _id: deviceToken.user_id });
    const jwtToken = tokenCredentials.generateToken(
      user.id,
      user.email ? user.email : user.socialId
    );
    const newDeviceToken = new DeviceToken({
      user_id: user.id,
      token: jwtToken,
    });
    await newDeviceToken.save();
    return Response.successResponse(
      res,
      "New token has been generated successfully",
      newDeviceToken.token
    );
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

/**
 * @swagger
 * /changePassword:
 *   post:
 *     summary: Change user password
 *     tags: [authentication]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *            type: object
 *            required:
 *              - oldPassword
 *              - newPassword
 *            properties:
 *              oldPassword:
 *                type: string
 *              newPassword:
 *                type: string
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Empty, error or Not Found
 *      401:
 *        description: Unauthorized Request
 *      403:
 *        description: Forbidden
 *      500:
 *        description: Response Error
 */
async function changePassword(req, res) {
  try {
    const { oldPassword, newPassword } = req.body;
    const userId = req.user.id;
    if (!inputValidation.validatePassword(newPassword)) {
      return Response.badReqResponse(
        res,
        "Please follow password requirements!"
      );
    }

    const user = await User.findOne({ _id: userId });

    const comparePasswords = await user.comparePassword(oldPassword);
    if (!comparePasswords) {
      return Response.badReqResponse(res, "Invalid old Password");
    }


    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(newPassword, salt);

    await user.save();
    return Response.successResponse(
      res,
      "Password has been changed successfully"
    );
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function updateUserMediaCategories(req, res) {
  try {
    const { categories } = req.body;
    let categoriesArray = categories.replace(/ /g, "").split(",");
    categoriesArray = categoriesArray.filter(function (el) {
      return el != null && el !== "";
    });
    if (
      !Array.isArray(categoriesArray) ||
      (Array.isArray(categoriesArray) && categoriesArray.length < 1)
    )
      return Response.serverErrorResponse(
        res,
        "Please select 3 at least of categories first!"
      );
    const userId = req.user.id;
    logger.i(`user ID: ${userId}`);
    let userInfo = await User.findOne({ _id: userId });
    if (userInfo) {
      logger.i(userInfo);
      userInfo.mediaCategories = categoriesArray;
      await userInfo.save();
      return Response.successResponse(res, "Saved successfully");
    }
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

module.exports = {
  signUp,
  signIn,
  setNewPassword,
  updateNotificationToken,
  changePassword,
  getRefreshToken,
  setNewEmail,
  setLoginCookie,
  requestResetPassword,
  updateUserMediaCategories,
  editUserProfile,
  requestChangeEmail,
  checkEmailExistsBefore,
};
