const Response = require("../utils/response");
const discoverService = require("../services/discover.service")


async function search(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const currentUserId = req.user.id;

        const users = await discoverService.searchUsers(query, currentUserId);
        const videos = await discoverService.searchVideos(query, currentUserId);
        const hashtags = await discoverService.searchHashtags(query, currentUserId);

        return Response.successResponse(res, "", { users, videos, hashtags });
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function suggestedSearch(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;

        const suggestions = await discoverService.suggestedSearch(query);

        return Response.successResponse(res, "", suggestions);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function getTrendingHashtags(req, res) {
    try {

        const trendingHashtags = await discoverService.trendingHashtags();

        return Response.successResponse(res, "", trendingHashtags);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function getTrendingVideos(req, res) {
    try {

        const trendingVideos = await discoverService.trendingVideos();

        return Response.successResponse(res, "", trendingVideos);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}



module.exports = {
    search,
    suggestedSearch,
    getTrendingHashtags,
    getTrendingVideos
};
