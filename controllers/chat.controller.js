const Response = require("../utils/response");
const logger = require("../utils/logger");

const chatService = require("../services/chat.service");

async function getConversations(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const userId = req.user.id;

        const conversations = await chatService.getConversations(query, userId);

        return Response.successResponse(res, "", conversations);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function pinConversation(req, res) {
    try {
        const conversationId = req.params.conversationId;
        const userId = req.user.id;
        await chatService.pinConversation(conversationId, userId);
        return Response.successResponse(res, "");
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function sendMessage(req, res) {
    try {
        const data = req.body;
        await chatService.addMessage(data)
        return Response.successResponse(res, "");
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function updateActivityStatus(req, res) {
    try {
        const { isActive } = req.body;
        const userId = req.user.id;
        await chatService.updateActivityStatus(userId, isActive)
        return Response.successResponse(res, "");
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function getMessages(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const conversationId = req.params.conversationId;
        const currUser = req.user.id;

        const messagesData = await chatService.getMessages(
            query,
            conversationId,
            currUser
        );

        return Response.successResponse(res, "", messagesData);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}

async function search(req, res) {
    try {
        const query = require("url").parse(req.url, true).query;
        const currentUserId = req.user.id;

        const data = await chatService.search(query, currentUserId);

        return Response.successResponse(res, "", data);
    } catch (error) {
        logger.error(req, error);
        return Response.serverErrorResponse(res, "", error);
    }
}



module.exports = {
    getConversations,
    pinConversation,
    getMessages,
    search,
    sendMessage,
    updateActivityStatus
};
