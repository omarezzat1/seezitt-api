const Response = require('../utils/response');


const artistModel = require("../models/sound_artist_model");
const soundModel = require("../models/sound_gallery_model");
const stickersModel = require("../models/stickers_model");
const MediaCategories = require('../models/media_categories_model')
const userModel = require('../models/user_model')
const mediaInteractionsLogs = require('../models/media_interactions_logs_model')
const MediaModel = require('../models/media_model')


async function addDemoData(req, res) {
  try {
    await MediaCategories.remove({});
    await artistModel.remove({});
    await stickersModel.remove({});
    await soundModel.remove({});


    let artist = new artistModel();
    artist.active = true;
    artist.name = "Sia Records";
    artist.save();

    await MediaCategories.insertMany([
      { "name": "Science and Technology", "active": true, },
      { "name": "Travel and Events", "active": true, },
      { "name": "Education", "active": true, },
      { "name": "People and Blogs", "active": true, },
      { "name": "News and Politics", "active": true, },
      { "name": "How to and Style", "active": true, },
      { "name": "Entertainment", "active": true, },
      { "name": "Comedy", "active": true, },
      { "name": "Pets and Animals", "active": true, },
      { "name": "Film and Animation", "active": true, },
      { "name": "Autos and Vehicles", "active": true, },
      { "name": "Gaming", "active": true, },
      { "name": "Music", "active": true, },
      { "name": "Sports", "active": true, },
      { "name": "Nonprofits and Activism", "active": true, },
    ]);

    await soundModel.insertMany([{
      "active": true,
      "artist_id": `${artist.id}`,
      "image": "",
      "name": "Melody and drums",
      "url": "https://download.samplelib.com/mp3/sample-9s.mp3"
    }, {
      "active": true,
      "artist_id": `${artist.id}`,
      "image": "",
      "name": "Cinematic musical dark guitar pluck",
      "url": "https://www.zapsplat.com/wp-content/uploads/2015/sound-effects-77317/zapsplat_sound_design_cinematic_musical_guitar_plucked_long_serious_dark_tension_drama_80203.mp3"
    }]);

    await stickersModel.insertMany([{
      "type": "1",
      "name": "happy",
      "url": "https://i.giphy.com/media/CAKpbsAk91a9sT5Nak/200w.gif",
      "active": true
    }, {
      "type": "1",
      "name": "happy",
      "url": "https://i.giphy.com/media/3oKIPtArcgQmH9dBK0/giphy.webp",
      "active": true
    }, {
      "type": "1",
      "name": "happy",
      "url": "https://i.giphy.com/media/1wlWtflbknYsTbvahh/200w.gif",
      "active": true
    }, {
      "type": "2",
      "name": "happy",
      "url": "https://i.giphy.com/media/b5LTssxCLpvVe/200.gif",
      "active": true
    }, {
      "type": "2",
      "name": "happy",
      "url": "https://i.giphy.com/media/h8UyZ6FiT0ptC/200w.gif",
      "active": true
    }, {
      "type": "2",
      "name": "happy",
      "url": "https://i.giphy.com/media/WUq1cg9K7uzHa/200.gif",
      "active": true
    }, {
      "type": "2",
      "name": "sad",
      "url": "https://i.giphy.com/media/10tIjpzIu8fe0/giphy.gif",
      "active": true
    }, {
      "type": "3",
      "name": "wink",
      "url": "https://i.giphy.com/media/1ryvI707BC2n7pHhrL/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "thumbs up",
      "url": "https://i.giphy.com/media/dalJ0CpF7hwmN1nZXe/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "heart",
      "url": "https://i.giphy.com/media/YondZW6AMjgTEHevF0/200w.gif",
      "active": true
    },
    {
      "type": "1",
      "name": "Weird",
      "url": "https://i.giphy.com/media/5UfYFCPMXVUTC/giphy.gif",
      "active": true
    }, {
      "type": "1",
      "name": "protect",
      "url": "https://i.giphy.com/media/g9rPxYMcTpmRL6I82o/giphy.gif",
      "active": true
    }, {
      "type": "1",
      "name": "Women's Rights",
      "url": "https://i.giphy.com/media/wczQIrkc61pj87n47K/200w.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Head Pat Love",
      "url": "https://i.giphy.com/media/5rjepK9Ue3hA4gcVjB/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Sticker Love",
      "url": "https://i.giphy.com/media/bdhneHJTmJVKHH36hy/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Happy Best Friends",
      "url": "https://i.giphy.com/media/z2h8vUwytb1Iv5sczX/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "sad",
      "url": "https://i.giphy.com/media/10tIjpzIu8fe0/giphy.gif",
      "active": true
    }, {
      "type": "3",
      "name": "wink",
      "url": "https://i.giphy.com/media/1ryvI707BC2n7pHhrL/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "thumbs up",
      "url": "https://i.giphy.com/media/dalJ0CpF7hwmN1nZXe/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "heart",
      "url": "https://i.giphy.com/media/YondZW6AMjgTEHevF0/200w.gif",
      "active": true
    },
    {
      "type": "1",
      "name": "Weird",
      "url": "https://i.giphy.com/media/5UfYFCPMXVUTC/giphy.gif",
      "active": true
    }, {
      "type": "1",
      "name": "protect",
      "url": "https://i.giphy.com/media/g9rPxYMcTpmRL6I82o/giphy.gif",
      "active": true
    }, {
      "type": "1",
      "name": "Women's Rights",
      "url": "https://i.giphy.com/media/wczQIrkc61pj87n47K/200w.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Head Pat Love",
      "url": "https://i.giphy.com/media/5rjepK9Ue3hA4gcVjB/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Sticker Love",
      "url": "https://i.giphy.com/media/bdhneHJTmJVKHH36hy/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Happy Best Friends",
      "url": "https://i.giphy.com/media/z2h8vUwytb1Iv5sczX/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "sad",
      "url": "https://i.giphy.com/media/10tIjpzIu8fe0/giphy.gif",
      "active": true
    }, {
      "type": "3",
      "name": "wink",
      "url": "https://i.giphy.com/media/1ryvI707BC2n7pHhrL/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "thumbs up",
      "url": "https://i.giphy.com/media/dalJ0CpF7hwmN1nZXe/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "heart",
      "url": "https://i.giphy.com/media/YondZW6AMjgTEHevF0/200w.gif",
      "active": true
    },
    {
      "type": "1",
      "name": "Weird",
      "url": "https://i.giphy.com/media/5UfYFCPMXVUTC/giphy.gif",
      "active": true
    }, {
      "type": "1",
      "name": "protect",
      "url": "https://i.giphy.com/media/g9rPxYMcTpmRL6I82o/giphy.gif",
      "active": true
    }, {
      "type": "1",
      "name": "Women's Rights",
      "url": "https://i.giphy.com/media/wczQIrkc61pj87n47K/200w.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Head Pat Love",
      "url": "https://i.giphy.com/media/5rjepK9Ue3hA4gcVjB/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Sticker Love",
      "url": "https://i.giphy.com/media/bdhneHJTmJVKHH36hy/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Happy Best Friends",
      "url": "https://i.giphy.com/media/z2h8vUwytb1Iv5sczX/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "sad",
      "url": "https://i.giphy.com/media/10tIjpzIu8fe0/giphy.gif",
      "active": true
    }, {
      "type": "3",
      "name": "wink",
      "url": "https://i.giphy.com/media/1ryvI707BC2n7pHhrL/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "thumbs up",
      "url": "https://i.giphy.com/media/dalJ0CpF7hwmN1nZXe/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "heart",
      "url": "https://i.giphy.com/media/YondZW6AMjgTEHevF0/200w.gif",
      "active": true
    },
    {
      "type": "1",
      "name": "Weird",
      "url": "https://i.giphy.com/media/5UfYFCPMXVUTC/giphy.gif",
      "active": true
    }, {
      "type": "1",
      "name": "protect",
      "url": "https://i.giphy.com/media/g9rPxYMcTpmRL6I82o/giphy.gif",
      "active": true
    }, {
      "type": "1",
      "name": "Women's Rights",
      "url": "https://i.giphy.com/media/wczQIrkc61pj87n47K/200w.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Head Pat Love",
      "url": "https://i.giphy.com/media/5rjepK9Ue3hA4gcVjB/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Sticker Love",
      "url": "https://i.giphy.com/media/bdhneHJTmJVKHH36hy/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "Happy Best Friends",
      "url": "https://i.giphy.com/media/z2h8vUwytb1Iv5sczX/giphy.gif",
      "active": true
    }, {
      "type": "2",
      "name": "sad",
      "url": "https://i.giphy.com/media/10tIjpzIu8fe0/giphy.gif",
      "active": true
    }, {
      "type": "3",
      "name": "wink",
      "url": "https://i.giphy.com/media/1ryvI707BC2n7pHhrL/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "thumbs up",
      "url": "https://i.giphy.com/media/dalJ0CpF7hwmN1nZXe/200w.gif",
      "active": true
    }, {
      "type": "3",
      "name": "heart",
      "url": "https://i.giphy.com/media/YondZW6AMjgTEHevF0/200w.gif",
      "active": true
    }]);

    return Response.successResponse(res, "Added successfully, All demo data reset!");
  } catch (error) {
    console.log(error);
    return Response.serverErrorResponse(res, "", error);
  }
};

async function migrateUsernames(req, res) {
  try {

    const currentUsers = await userModel.find({ username: undefined });
    currentUsers.forEach(user => {
      user.username = (user.name ? user.name.split(' ')[0].toLowerCase() : undefined) + user._id.toString().slice(-6)
    });

    await userModel.bulkSave(currentUsers)

    return Response.successResponse(res, "Added successfully, All demo data reset!");
  } catch (error) {
    console.log(error);
    return Response.serverErrorResponse(res, "", error);
  }
};

async function migrateVideosLinks(req, res) {
  try {
    const media = await MediaModel.find({ isDeleted: false })
    const updatedVersion = media.map(m => {
      if (m.reducedVideoHlsUrl) {
        m.reducedVideoHlsUrl = m.reducedVideoHlsUrl.replace('https://seezitt-videos-destination-bucket.s3.amazonaws.com', 'https://d1sxmct9g9hjoi.cloudfront.net')
        m.reducedVideoUrl = m.reducedVideoUrl.replace('https://seezitt-videos-destination-bucket.s3.amazonaws.com', 'https://d1sxmct9g9hjoi.cloudfront.net')
        m.shortVideoUrl = m.shortVideoUrl.replace('https://seezitt-videos-destination-bucket.s3.amazonaws.com', 'https://d1sxmct9g9hjoi.cloudfront.net')
        m.shortVideoHlsUrl = m.shortVideoHlsUrl.replace('https://seezitt-videos-destination-bucket.s3.amazonaws.com', 'https://d1sxmct9g9hjoi.cloudfront.net')
        m.thumbnailUrl = m.thumbnailUrl.replace('https://seezitt-videos-destination-bucket.s3.amazonaws.com', 'https://d1sxmct9g9hjoi.cloudfront.net')

      }
      return m;
    })
    await MediaModel.bulkSave(updatedVersion)
    return Response.successResponse(res, "Migrated successfully!");

  }
  catch (error) {
    console.log(error);
    return Response.serverErrorResponse(res, "", error);
  }
};

async function mediaInteractionsLogsCleanUp(req, res) {
  try {
    const MediaInteractions = await mediaInteractionsLogs.find();
    const mediaInteractionsIds = MediaInteractions.map(a => a.mediaId);
    const uniqueMediaInteractionsIds = [...new Set(mediaInteractionsIds)];


    const media = await MediaModel.find();
    const mediaIds = media.map(a => a._id.toString());
    const uniqueMediaIds = [...new Set(mediaIds)];

    const mediaIdsToBeDeleted = uniqueMediaInteractionsIds.filter(x => uniqueMediaIds.indexOf(x) < 0);

    await mediaInteractionsLogs.deleteMany({ mediaId: { $in: mediaIdsToBeDeleted } });

    return Response.successResponse(res, "Cleaned successfully!");
  } catch (error) {
    console.log(error);
    return Response.serverErrorResponse(res, "", error);
  }



}
module.exports = {
  mediaInteractionsLogsCleanUp,
  addDemoData,
  migrateUsernames,
  migrateVideosLinks
};

