const express = require("express");
const passport = require("passport");
const fs = require("fs");
const AWS = require("aws-sdk");
const mongoose = require('mongoose');


const Response = require("../utils/response");
const constants = require("../utils/constants");
const Keys = require("../utils/keys");
const config = require("../configs/config");
const logger = require("../utils/logger");
const { logMediaInteraction, deleteMediaInteractions } = require("../utils/log_media_interaction");
require("../utils/custom")();

const MediaModel = require("../models/media_model");
const userModel = require("../models/user_model");
const MediaComments = require("../models/media_comments_model");
const MediaReportsModel = require("../models/media_reports_model");
const ReportingReasons = require("../models/media_reports_model").Reasons;
const UserFollowerModel = require("../models/user_followers_model");
const MediaInteractionType =
  require("../models/media_interactions_logs_model").MediaInteractionType;
const MediaCategories = require("../models/media_categories_model");
const HighlightModel = require("../models/highlight_model");
const HashtagModel = require("../models/hashtag_model");

const NotificationService = require("../services/notifications.service");
const recommendationService = require("../services/recommendation.service");


async function addMediaUpgraded(req, res) {
  logger.i('Starts video uploading')
  try {
    const { description, lat, lng, type, tags, category, isStory, isOnlyMe } = req.body;
    const mediaType =
      type === Keys.mediaImage ? Keys.mediaImage : Keys.mediaVideo;
    logger.i(`Media upload type: ${mediaType}`);

    const s3 = new AWS.S3({
      accessKeyId: config.aws.access_key_id,
      secretAccessKey: config.aws.secret_access_key,
      region: config.aws.region,
      signatureVersion: config.aws.signatureVersion,
    });

    logger.d(`userId: ${req.user.id}`);
    const draftFolder = `${constants.uploadFile + randomString(10)}/`;
    logger.i(`Draft folder: ${draftFolder}`);

    const media = new MediaModel();
    media.user_id = req.user.id;
    if (description) {
      media.description = description;
    }

    const hashtagRegex = /#[^\s#]+/g;
    const hashtags = description.match(hashtagRegex);

    if (hashtags && hashtags.length) {
      media.hashtags = hashtags;
      for (const hashtag of hashtags) {
        const hashtagDoc = await HashtagModel.findOne({
          name: hashtag,
        });

        if (hashtagDoc) {
          hashtagDoc.relatedVideos.push(media._id);
          await hashtagDoc.save();
        } else {
          const newHashtag = new HashtagModel();
          newHashtag.createdTime = Date.now();
          newHashtag.name = hashtag;
          newHashtag.relatedVideos.push(media._id);
          await newHashtag.save();
        }
      }
    }

    media.tags = tags != null && tags !== "" ? tags.split(",") : [];
    media.category = category != null && category !== "" ? category : "";
    media.type = mediaType;
    media.createdTime = Date.now();
    media.is_story = isStory ? isStory : false;
    if (isOnlyMe) {
      media.privacyOptions.isOnlyMe = isOnlyMe;
    }
    const { file } = req.files;
    if (mediaType === Keys.mediaVideo) {

      // try {
      //   media.createdTime = Date.now();
      //   if (lat != null && lat !== "" && lng != null && lng !== "") {
      //     media.media_latlng = { type: "Point", coordinates: [lat, lng] };
      //   }
      //   await media.save();

      // } catch (e) {
      //   logger.e(e);
      // }

      // video
      const videoFile = `${draftFolder + media._id}.mp4`;
      logger.i(`Moving the file ${videoFile}`);
      await file.mv(videoFile);
      logger.i(`Finished moving the file ${mediaType}`);



      await Promise.all(
        fs.readdirSync(draftFolder).map(async (file, index) => {
          logger.i(file);
          const fileContent = fs.readFileSync(draftFolder + file);
          const params = {
            Bucket: config.aws.bucket_name,
            Key: 'input/videos/' + file,
            Body: fileContent,
            ACL: "public-read",
          };
          logger.i(`Uploading to the S3`);
          await s3.upload(params).promise();
          logger.i(`finished uploading the file to S3`);
          media.originalUrl = `https://${config.aws.bucket_name}.s3.amazonaws.com/input/videos/${file}`;
          logger.i(`Original Url ${media.originalUrl}`);
          await media.save();
          logger.i(`After Saving`);
          console.log(
            `https://${config.aws.bucket_name}.s3.amazonaws.com/input/videos/${file}`
          );
        })
      );

    }
    else {
      logger.i(`Not in media types ${mediaType}`);
    }

    try {
      // delete all drafts
      fs.rmSync(draftFolder, { recursive: true, force: true });
    } catch (e) {
      logger.e(e);
    }
    return Response.successResponse(res, "", { media });
  } catch (error) {
    logger.e(error)
    return Response.serverErrorResponse(res, "Something went wrong", error);
  }
}

async function updateByEncoder(req, res) {
  try {
    const { reducedVideoUrl, reducedVideoHlsUrl, shortVideoUrl, shortVideoHlsUrl, thumbnailUrl, status } = req.body;
    logger.i(reducedVideoUrl);
    logger.i(reducedVideoHlsUrl);
    logger.i(shortVideoUrl);
    logger.i(shortVideoHlsUrl);
    logger.i(thumbnailUrl);
    logger.i(status);
    const mediaId = req.params.id;
    const mediaContent = await MediaModel.findOne({
      _id: mediaId.toString(),
    });
    logger.i(mediaContent);
    mediaContent.reducedVideoUrl = reducedVideoUrl ?? reducedVideoUrl;
    mediaContent.reducedVideoHlsUrl = reducedVideoHlsUrl ?? reducedVideoHlsUrl;
    mediaContent.shortVideoUrl = shortVideoUrl ?? shortVideoUrl;
    mediaContent.shortVideoHlsUrl = shortVideoHlsUrl ?? shortVideoHlsUrl;
    mediaContent.thumbnailUrl = thumbnailUrl ?? thumbnailUrl;
    mediaContent.status = status;
    await mediaContent.save();
    if (!mediaContent.isDeleted) {
      NotificationService.videoProcessingNotification(mediaContent.user_id.toString(), mediaContent)
    }
    return Response.successResponse(res);
  } catch (error) {
    logger.e(error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getStoriesFeed(req, res) {
  try {
    const userId = req.user.id;
    const yesterday = Date.now() - 8.64e7;
    let usersFollowing = await UserFollowerModel.find({
      follower_userID: userId,
      isDeleted: false,
    }).select("followed_userID");
    logger.i(yesterday);

    // const usersFollowed = await UserFollowerModel.find({ follower_userID: userId, isDeleted: false }).select('follower_userID');
    usersFollowing = usersFollowing.map((userFollowing) =>
      userFollowing.followed_userID.toString()
    );
    logger.i(usersFollowing);
    const stories = await MediaModel.find({
      status: "Processed",
      user_id: { $in: usersFollowing },
      is_story: true,
      isDeleted: false,
      createdTime: { $gt: yesterday },
    })
      .populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .sort({ createdTime: -1 });
    logger.i(stories)
    const storiesIds = stories.map((story) => story._id.toString());
    const storiesMapped = [];
    await Promise.all(
      stories.map(async (story) => {
        let mediaCategory = null;
        if (story.category !== "") {
          try {
            mediaCategory = await MediaCategories.findOne({
              _id: story.category,
            });
          } catch (e) { }
        }

        let comments = await MediaComments.find({
          _id: { $in: story.comments },
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .populate({
            path: "replies.user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          });

        comments = comments.map((comment) => ({
          id: comment._id,
          user: comment.user_id,
          comment: comment.comment,
          likes: comment.likes.length,
          isLiked: comment.likes.includes(userId),
          createdTime: comment.createdTime,
          replies: comment.replies?.map((reply) => ({
            id: reply.id,
            user: reply.user_id,
            reply: reply.reply,
            likes: reply.likes.length,
            isLiked: reply.likes.includes(userId),
            createdTime: reply.createdTime,
          })),
        }));

        const storyResponse = {
          mediaId: story._id.toString(),
          createdTime: story.createdTime,
          user: {
            _id: story.user_id._id,
            name: story.user_id.name,
            avatar: story.user_id.avatar,
            username: story.user_id.username,
            isFollowed: usersFollowing.includes(userId),
          },
          likes: story.likes.length,
          shares: story.shares ? story.shares.length : 0,
          views: story.watched_users ? story.watched_users.length : 0,
          description: story.description,
          linkedFiles: story.linked_files,
          hlsUrl: story.hls_url,
          reducedVideoUrl: story.reducedVideoUrl,
          reducedVideoHlsUrl: story.reducedVideoHlsUrl,
          shortVideoUrl: story.shortVideoUrl,
          shortVideoHlsUrl: story.shortVideoHlsUrl,
          privacyOptions: story.privacyOptions,
          thumbnailUrl: story.thumbnailUrl,
          thumbnail: story.thumbnail,
          originalUrl: story.originalUrl,
          comments: comments,
          isLiked: story.likes.includes(userId),
          category: mediaCategory != null ? mediaCategory.name : "",
          type: story.type,
          isWatched: story.watched_users.find(
            (user) => user.toString() === userId
          )
            ? true
            : false,
        };

        storiesMapped.push(storyResponse);
      })
    );

    const storiesGrouped = Object.values(
      storiesMapped.reduce((arr, story) => {
        if (!arr[story.user._id])
          arr[story.user._id] = { ...story.user, stories: [] };
        arr[story.user._id].stories.push(story);
        return arr;
      }, {})
    );

    return Response.successResponse(res, "", storiesGrouped);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function buildVideosFeedDto(req, res) {
  try {
    const query = require("url").parse(req.url, true).query;
    const { page } = query;
    const userId = req.user.id;
    let pageNum = page ?? 1;
    let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;
    const videos = await recommendationService.getVideosRecommendations(userId, pageNum - 1);
    let mediaContents = []
    if (videos && videos.length > 0) {
      mediaContents = await MediaModel.find({
        status: "Processed",
        isDeleted: false,
        is_story: { $ne: true },
        isOnlyMe: { $ne: true },
        _id: { $in: videos.map(id => mongoose.Types.ObjectId(id)) }
      }).populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
        .populate({
          path: "receivedGifts.giftId",
          model: "Gift",
          select: ["_id", "name", "imageUrl", "price"],
        })
        .populate({
          path: "receivedGifts.userId",
          model: "User",
          select: ["_id", "name", "avatar", "username"]
        })
        .sort({
          _id: -1,
        })

    }
    else {
      mediaContents = await MediaModel.find({
        status: "Processed",
        isDeleted: false,
        is_story: { $ne: true },
        isOnlyMe: { $ne: true },
      })
        .populate({
          path: "user_id",
          model: "User",
          select: ["_id", "name", "avatar", "username", "isVerified"],
        })
        .populate({
          path: "receivedGifts.giftId",
          model: "Gift",
          select: ["_id", "name", "imageUrl", "price"],
        })
        .populate({
          path: "receivedGifts.userId",
          model: "User",
          select: ["_id", "name", "avatar", "username"]
        })
        .skip(skip)
        .sort({
          _id: -1,
        })
        .limit(constants.pageLimit);
    }

    const mediaUsers = mediaContents.map((content) => content.user_id._id);
    let usersFollowing = await UserFollowerModel.find({
      follower_userID: userId,
      followed_userID: { $in: mediaUsers },
      isDeleted: false,
    }).select("follower_userID");

    if (usersFollowing.length) {
      usersFollowing = usersFollowing.map((userFollowing) =>
        userFollowing.follower_userID.toString()
      );
    }
    const mediaContentsIds = mediaContents.map((mediaContent) =>
      mediaContent._id.toString()
    );
    const mediaContentsResponseData = [];
    await Promise.all(
      mediaContents.map(async (mediaContent) => {
        let mediaCategory = null;
        if (mediaContent.category !== "") {
          try {
            mediaCategory = await MediaCategories.findOne({
              _id: mediaContent.category,
            });
          } catch (e) { }
        }

        let comments = await MediaComments.find({
          _id: { $in: mediaContent.comments },
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .populate({
            path: "replies.user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          });

        comments = comments.map((comment) => ({
          id: comment._id,
          user: comment.user_id,
          comment: comment.comment,
          likes: comment.likes.length,
          isLiked: comment.likes.includes(userId),
          createdTime: comment.createdTime,
          replies: comment.replies?.map((reply) => ({
            id: reply.id,
            user: reply.user_id,
            reply: reply.reply,
            likes: reply.likes.length,
            isLiked: reply.likes.includes(userId),
            createdTime: reply.createdTime,
          })),
        }));
        const mediaContentResponse = {
          mediaId: mediaContent._id.toString(),
          createdTime: mediaContent.createdTime,
          user: {
            _id: mediaContent.user_id._id,
            name: mediaContent.user_id.name,
            avatar: mediaContent.user_id.avatar,
            username: mediaContent.user_id.username,
            isFollowed: usersFollowing.includes(userId),
          },
          likes: mediaContent.likes.length,
          shares: mediaContent.shares ? mediaContent.shares.length : 0,
          views: mediaContent.watched_users
            ? mediaContent.watched_users.length
            : 0,
          description: mediaContent.description,
          linkedFiles: mediaContent.linked_files,
          hlsUrl: mediaContent.hls_url,
          reducedVideoUrl: mediaContent.reducedVideoUrl,
          reducedVideoHlsUrl: mediaContent.reducedVideoHlsUrl,
          shortVideoUrl: mediaContent.shortVideoUrl,
          shortVideoHlsUrl: mediaContent.shortVideoHlsUrl,
          privacyOptions: mediaContent.privacyOptions,
          thumbnailUrl: mediaContent.thumbnailUrl,
          thumbnail: mediaContent.thumbnail,
          originalUrl: mediaContent.originalUrl,
          comments: comments,
          isLiked: mediaContent.likes.includes(userId),
          category: mediaCategory != null ? mediaCategory.name : "",
          type: mediaContent.type,
          privacyOptions: mediaContent.privacyOptions,
          receivedGifts: mediaContent.receivedGifts,
        };

        mediaContentsResponseData.push(mediaContentResponse);
      })
    );
    const sortedMediaContentsResponseData = mediaContentsResponseData.sort((a, b) => {
      if (a.mediaId > b.mediaId) return -1;
      if (a.mediaId < b.mediaId) return 1;
      return 0;
    });
    return Response.successResponse(res, "", sortedMediaContentsResponseData);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}


async function buildVideosFeedForFollowingDto(req, res) {
  try {
    const query = require("url").parse(req.url, true).query;
    const { page } = query;
    const userId = req.user.id;
    let pageNum = page ?? 1;
    let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;
    const videos = await recommendationService.getFollowingRecommendations(userId, pageNum - 1);
    let mediaContents = []
    if (videos && videos.length > 0) {
      mediaContents = await MediaModel.find({
        status: "Processed",
        isDeleted: false,
        is_story: { $ne: true },
        isOnlyMe: { $ne: true },
        _id: { $in: videos.map(id => mongoose.Types.ObjectId(id)) }
      }).populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
        .populate({
          path: "receivedGifts.giftId",
          model: "Gift",
          select: ["_id", "name", "imageUrl", "price"],
        })
        .populate({
          path: "receivedGifts.userId",
          model: "User",
          select: ["_id", "name", "avatar", "username"]
        })
        .sort({
          _id: -1,
        })

    }
    else {
      mediaContents = await MediaModel.find({
        status: "Processed",
        isDeleted: false,
        is_story: { $ne: true },
        isOnlyMe: { $ne: true },
      })
        .populate({
          path: "user_id",
          model: "User",
          select: ["_id", "name", "avatar", "username", "isVerified"],
        })
        .populate({
          path: "receivedGifts.giftId",
          model: "Gift",
          select: ["_id", "name", "imageUrl", "price"],
        })
        .populate({
          path: "receivedGifts.userId",
          model: "User",
          select: ["_id", "name", "avatar", "username"]
        })
        .skip(skip)
        .sort({
          _id: -1,
        })
        .limit(constants.pageLimit);
    }

    const mediaUsers = mediaContents.map((content) => content.user_id._id);
    let usersFollowing = await UserFollowerModel.find({
      follower_userID: userId,
      followed_userID: { $in: mediaUsers },
      isDeleted: false,
    }).select("follower_userID");

    if (usersFollowing.length) {
      usersFollowing = usersFollowing.map((userFollowing) =>
        userFollowing.follower_userID.toString()
      );
    }
    const mediaContentsIds = mediaContents.map((mediaContent) =>
      mediaContent._id.toString()
    );
    const mediaContentsResponseData = [];
    await Promise.all(
      mediaContents.map(async (mediaContent) => {
        let mediaCategory = null;
        if (mediaContent.category !== "") {
          try {
            mediaCategory = await MediaCategories.findOne({
              _id: mediaContent.category,
            });
          } catch (e) { }
        }

        let comments = await MediaComments.find({
          _id: { $in: mediaContent.comments },
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .populate({
            path: "replies.user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          });

        comments = comments.map((comment) => ({
          id: comment._id,
          user: comment.user_id,
          comment: comment.comment,
          likes: comment.likes.length,
          isLiked: comment.likes.includes(userId),
          createdTime: comment.createdTime,
          replies: comment.replies?.map((reply) => ({
            id: reply.id,
            user: reply.user_id,
            reply: reply.reply,
            likes: reply.likes.length,
            isLiked: reply.likes.includes(userId),
            createdTime: reply.createdTime,
          })),
        }));
        const mediaContentResponse = {
          mediaId: mediaContent._id.toString(),
          createdTime: mediaContent.createdTime,
          user: {
            _id: mediaContent.user_id._id,
            name: mediaContent.user_id.name,
            avatar: mediaContent.user_id.avatar,
            username: mediaContent.user_id.username,
            isFollowed: usersFollowing.includes(userId),
          },
          likes: mediaContent.likes.length,
          shares: mediaContent.shares ? mediaContent.shares.length : 0,
          views: mediaContent.watched_users
            ? mediaContent.watched_users.length
            : 0,
          description: mediaContent.description,
          linkedFiles: mediaContent.linked_files,
          hlsUrl: mediaContent.hls_url,
          reducedVideoUrl: mediaContent.reducedVideoUrl,
          reducedVideoHlsUrl: mediaContent.reducedVideoHlsUrl,
          shortVideoUrl: mediaContent.shortVideoUrl,
          shortVideoHlsUrl: mediaContent.shortVideoHlsUrl,
          privacyOptions: mediaContent.privacyOptions,
          thumbnailUrl: mediaContent.thumbnailUrl,
          thumbnail: mediaContent.thumbnail,
          originalUrl: mediaContent.originalUrl,
          comments: comments,
          isLiked: mediaContent.likes.includes(userId),
          category: mediaCategory != null ? mediaCategory.name : "",
          type: mediaContent.type,
          privacyOptions: mediaContent.privacyOptions,
          receivedGifts: mediaContent.receivedGifts,
        };

        mediaContentsResponseData.push(mediaContentResponse);
      })
    );
    const sortedMediaContentsResponseData = mediaContentsResponseData.sort((a, b) => {
      if (a.mediaId > b.mediaId) return -1;
      if (a.mediaId < b.mediaId) return 1;
      return 0;
    });
    return Response.successResponse(res, "", sortedMediaContentsResponseData);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}



async function getVideoFeedV2(req, res) {
  try {
    const query = require("url").parse(req.url, true).query;
    const { followingSuggestions } = query;
    if (followingSuggestions == 1) {
      return buildVideosFeedForFollowingDto(req, res);
    } else {
      return buildVideosFeedDto(req, res);
    }

  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
/**
 * @swagger
 * '/getMediaContent':
 *  get:
 *     tags: [Media]
 *     summary: get videos with data
 *     parameters:
 *     - name: page
 *     in: path
 *     description: get videos with data
 *     required: true
 *     schema:
 *     type: string
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Empty, error or Not Found
 *      401:
 *        description: Unauthorized Request
 *      403:
 *        description: Forbidden
 *      500:
 *        description: Response Error
 */
async function getVideosFeed(req, res) {
  try {
    const query = require("url").parse(req.url, true).query;
    const { page } = query;
    const userId = req.user.id;
    let pageNum = page ?? 1;
    let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;

    const mediaContents = await MediaModel.find({
      status: "Processed",
      isDeleted: false,
      is_story: { $ne: true },
      isOnlyMe: { $ne: true },
    })
      .populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .populate({
        path: "receivedGifts.giftId",
        model: "Gift",
        select: ["_id", "name", "imageUrl", "price"],
      })
      .populate({
        path: "receivedGifts.userId",
        model: "User",
        select: ["_id", "name", "avatar", "username"]
      })
      .skip(skip)
      .sort({
        _id: -1,
      })
      .limit(constants.pageLimit);

    // const mediaContents = await MediaModel.aggregate([
    //     { $match: { isDeleted: false } },
    //     //{ $sample: { size: 10 } }, // just a random result everytime [ page must be 1 everytime ]
    //     { $skip: skip },
    //     { $limit: constants.pageLimit },
    //     { $group: { _id: "$_id", result: { $push : "$$ROOT"} } },
    //     { $replaceRoot: { newRoot: {$first : "$result" } } }
    //   ]
    // )
    const mediaUsers = mediaContents.map((content) => content.user_id._id);
    let usersFollowing = await UserFollowerModel.find({
      follower_userID: userId,
      followed_userID: { $in: mediaUsers },
      isDeleted: false,
    }).select("follower_userID");

    if (usersFollowing.length) {
      usersFollowing = usersFollowing.map((userFollowing) =>
        userFollowing.follower_userID.toString()
      );
    }
    const mediaContentsIds = mediaContents.map((mediaContent) =>
      mediaContent._id.toString()
    );
    const mediaContentsResponseData = [];
    await Promise.all(
      mediaContents.map(async (mediaContent) => {
        let mediaCategory = null;
        if (mediaContent.category !== "") {
          try {
            mediaCategory = await MediaCategories.findOne({
              _id: mediaContent.category,
            });
          } catch (e) { }
        }

        let comments = await MediaComments.find({
          _id: { $in: mediaContent.comments },
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .populate({
            path: "replies.user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          });

        comments = comments.map((comment) => ({
          id: comment._id,
          user: comment.user_id,
          comment: comment.comment,
          likes: comment.likes.length,
          isLiked: comment.likes.includes(userId),
          createdTime: comment.createdTime,
          replies: comment.replies?.map((reply) => ({
            id: reply.id,
            user: reply.user_id,
            reply: reply.reply,
            likes: reply.likes.length,
            isLiked: reply.likes.includes(userId),
            createdTime: reply.createdTime,
          })),
        }));
        const mediaContentResponse = {
          mediaId: mediaContent._id.toString(),
          createdTime: mediaContent.createdTime,
          user: {
            _id: mediaContent.user_id._id,
            name: mediaContent.user_id.name,
            avatar: mediaContent.user_id.avatar,
            username: mediaContent.user_id.username,
            isFollowed: usersFollowing.includes(userId),
          },
          likes: mediaContent.likes.length,
          shares: mediaContent.shares ? mediaContent.shares.length : 0,
          views: mediaContent.watched_users
            ? mediaContent.watched_users.length
            : 0,
          description: mediaContent.description,
          linkedFiles: mediaContent.linked_files,
          hlsUrl: mediaContent.hls_url,
          reducedVideoUrl: mediaContent.reducedVideoUrl,
          reducedVideoHlsUrl: mediaContent.reducedVideoHlsUrl,
          shortVideoUrl: mediaContent.shortVideoUrl,
          shortVideoHlsUrl: mediaContent.shortVideoHlsUrl,
          privacyOptions: mediaContent.privacyOptions,
          thumbnailUrl: mediaContent.thumbnailUrl,
          thumbnail: mediaContent.thumbnail,
          originalUrl: mediaContent.originalUrl,
          comments: comments,
          isLiked: mediaContent.likes.includes(userId),
          category: mediaCategory != null ? mediaCategory.name : "",
          type: mediaContent.type,
          privacyOptions: mediaContent.privacyOptions,
          receivedGifts: mediaContent.receivedGifts,
        };

        mediaContentsResponseData.push(mediaContentResponse);
      })
    );
    const sortedMediaContentsResponseData = mediaContentsResponseData.sort((a, b) => {
      if (a.mediaId > b.mediaId) return -1;
      if (a.mediaId < b.mediaId) return 1;
      return 0;
    });
    return Response.successResponse(res, "", sortedMediaContentsResponseData);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
async function getMediaContent(req, res) {
  try {
    const query = require("url").parse(req.url, true).query;
    const { page } = query;
    const userId = req.user.id;
    const mediaId = req.params.id;
    let pageNum = page ?? 1;
    let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;
    const mediaContents = await MediaModel.find({
      _id: mediaId,
      isDeleted: false,
      is_story: { $ne: true },
    })
      .populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .populate({
        path: "receivedGifts.giftId",
        model: "Gift",
        select: ["_id", "name", "imageUrl", "price"],
      })
      .populate({
        path: "receivedGifts.userId",
        model: "User",
        select: ["_id", "name", "avatar", "username"]
      })
      .skip(skip)
      .sort({
        createdTime: -1,
      })
      .limit(constants.pageLimit);

    const mediaUsers = mediaContents.map((content) => content.user_id._id);
    let usersFollowing = await UserFollowerModel.find({
      follower_userID: userId,
      followed_userID: { $in: mediaUsers },
      isDeleted: false,
    }).select("follower_userID");
    if (usersFollowing.length) {
      usersFollowing = usersFollowing.map((userFollowing) =>
        userFollowing.follower_userID.toString()
      );
    }

    const mediaContentsIds = mediaContents.map((mediaContent) =>
      mediaContent._id.toString()
    );
    const mediaContentsResponse = await Promise.all(
      mediaContents.map(async (mediaContent) => {
        let comments = await MediaComments.find({
          _id: { $in: mediaContent.comments },
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .populate({
            path: "replies.user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          });

        comments = comments.map((comment) => ({
          id: comment._id,
          user: comment.user_id,
          comment: comment.comment,
          likes: comment.likes.length,
          isLiked: comment.likes.includes(userId),
          createdTime: comment.createdTime,
          replies: comment.replies?.map((reply) => ({
            id: reply.id,
            user: reply.user_id,
            reply: reply.reply,
            likes: reply.likes.length,
            isLiked: reply.likes.includes(userId),
            createdTime: reply.createdTime,
          })),
        }));
        const mediaContentResponse = {
          mediaId: mediaContent._id.toString(),
          createdTime: mediaContent.createdTime,
          user: {
            _id: mediaContent.user_id._id,
            name: mediaContent.user_id.name,
            avatar: mediaContent.user_id.avatar,
            username: mediaContent.user_id.username,
            isFollowed: usersFollowing.includes(userId),
          },
          likes: mediaContent.likes.length,
          shares: mediaContent.shares.length,
          views: mediaContent.watched_users.length,
          description: mediaContent.description,
          linkedFiles: mediaContent.linked_files,
          hlsUrl: mediaContent.hls_url,
          reducedVideoUrl: mediaContent.reducedVideoUrl,
          reducedVideoHlsUrl: mediaContent.reducedVideoHlsUrl,
          shortVideoUrl: mediaContent.shortVideoUrl,
          shortVideoHlsUrl: mediaContent.shortVideoHlsUrl,
          privacyOptions: mediaContent.privacyOptions,
          thumbnailUrl: mediaContent.thumbnailUrl,
          thumbnail: mediaContent.thumbnail,
          originalUrl: mediaContent.originalUrl,
          comments: comments,
          isLiked: mediaContent.likes.includes(userId),
          type: mediaContent.type,
          receivedGifts: mediaContent.receivedGifts,
        };
        return mediaContentResponse;
      })
    );
    return Response.successResponse(res, "", mediaContentsResponse[0]);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
/**
 * @swagger
 * '/likeVideo/{id}':
 *  post:
 *     tags: [Media]
 *     summary: Like/Dislike Videos
 *     parameters:
 *     - name: id
 *     in: path
 *     description: Like/Dislike Videos
 *     required: true
 *     schema:
 *     type: string
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Empty, error or Not Found
 *      401:
 *        description: Unauthorized Request
 *      403:
 *        description: Forbidden
 *      500:
 *        description: Response Error
 */
async function likeMediaContent(req, res) {
  try {
    const userId = req.user.id;
    const videoId = req.params.id;
    const mediaContent = await MediaModel.findOne({
      _id: videoId.toString(),
      isDeleted: false,
    });
    if (!mediaContent) return Response.notFoundResponse(res, "Video Not Found");
    //Dislike case
    const index = mediaContent.likes.findIndex((like) => like == userId);
    if (index != -1) {
      mediaContent.likes.splice(index, 1);
    } else {
      mediaContent.likes.push(userId);
    }
    await mediaContent.save();

    if (index != -1) {
      logMediaInteraction(userId, videoId, MediaInteractionType.LIKE);
      NotificationService.sendNewLikeNotification(userId, mediaContent.user_id, mediaContent._id.toString());
    } else {
      deleteMediaInteractions(videoId);
    }
    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
async function commentOnMediaContent(req, res) {
  try {
    const userId = req.user.id;
    const videoId = req.params.id;
    const { comment, commentId } = req.body;
    if (!comment) return Response.badReqResponse(res);

    const mediaContent = await MediaModel.findOne({
      _id: videoId.toString(),
      isDeleted: false,
    });
    if (!mediaContent) return Response.notFoundResponse(res, "Video Not Found");
    let mediaComment;
    // Reply Case
    if (commentId) {
      mediaComment = await MediaComments.findOne({
        _id: commentId,
        media_id: videoId,
      });
      if (!mediaComment)
        return Response.notFoundResponse(res, "Comment Not Found");
      mediaComment.replies.push({
        user_id: userId,
        reply: comment,
        createdTime: Date.now(),
      });
      if (userId != mediaComment.user_id) {
        NotificationService.sendNewReplyNotification(userId, mediaComment.user_id, videoId);
      }
      await mediaComment.save();
    } else {
      mediaComment = new MediaComments({
        user_id: userId,
        media_id: videoId,
        comment: comment,
        createdTime: Date.now(),
      });
      await mediaComment.save();
      mediaContent.comments.push(mediaComment._id.toString());
      mediaContent.save();
      if (userId != mediaContent.user_id) {
        NotificationService.sendNewCommentNotification(userId, mediaContent.user_id, videoId);
      }

    }

    let commentObj = await MediaComments.findOne({ _id: mediaComment._id })
      .populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .populate({
        path: "replies.user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      });

    let commentResponse = {
      id: commentObj._id,
      user: commentObj.user_id,
      comment: commentObj.comment,
      likes: commentObj.likes.length,
      isLiked: commentObj.likes.includes(userId),
      createdTime: commentObj.createdTime,
      replies: commentObj.replies?.map((reply) => ({
        id: reply.id,
        user: reply.user_id,
        reply: reply.reply,
        likes: reply.likes.length,
        isLiked: reply.likes.includes(userId),
        createdTime: reply.createdTime,
      })),
    };
    return Response.successResponse(res, "", commentResponse);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function likeComment(req, res) {
  try {
    const userId = req.user.id;
    const commentId = req.params.id;
    const { replyId } = req.body;
    const mediaComment = await MediaComments.findOne({ _id: commentId });
    if (!mediaComment)
      return Response.notFoundResponse(res, "Comment Not Found");

    if (replyId) {
      const replyIndex = mediaComment.replies.findIndex(
        (reply) => reply._id == replyId
      );
      if (!replyId) return Response.notFoundResponse(res, "Reply Not Found");
      const existedLikeIndex = mediaComment.replies[replyIndex].likes.findIndex(
        (like) => like == userId
      );
      if (existedLikeIndex != -1) {
        mediaComment.replies[replyIndex].likes.splice(existedLikeIndex, 1);
      } else {
        mediaComment.replies[replyIndex].likes.push(userId);
        if (userId != mediaComment.replies[replyIndex].user_id) {
          NotificationService.sendCommentLikeNotification(userId, mediaComment.replies[replyIndex].user_id, 'Reply', mediaComment.media_id.toString());
        }

      }
    } else {
      const existedLikeIndex = mediaComment.likes.findIndex(
        (like) => like == userId
      );
      if (existedLikeIndex != -1) {
        mediaComment.likes.splice(existedLikeIndex, 1);
      } else {
        mediaComment.likes.push(userId);
        if (userId != mediaComment.user_id) {
          NotificationService.sendCommentLikeNotification(userId, mediaComment.user_id, 'Comment', mediaComment.media_id.toString());
        }
      }
    }
    mediaComment.save();
    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
async function getReportingReasons(req, res) {
  try {
    return Response.successResponse(res, "", ReportingReasons);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
async function reportMediaContent(req, res) {
  try {
    logger.i(req.user.id);
    logger.i(req.params.id);
    const userId = req.user.id;
    const videoId = req.params.id;
    const { reason } = req.body;
    logger.i(`reason : ${reason}`);
    if (!ReportingReasons[reason])
      return Response.badReqResponse(res, "Invalid Reason");
    const mediaReport = new MediaReportsModel({
      user_id: userId,
      media_id: videoId,
      reason: ReportingReasons[reason],
      createdTime: Date.now(),
    });
    await mediaReport.save();
    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getUserReports(req, res) {
  try {
    let pageNum = req.query.page ?? 1;
    let skip = pageNum > 0 ? (pageNum - 1) * constants.pageLimit : 1;
    const userId = req.user.id;
    let mediaReport = await MediaReportsModel.find({ user_id: userId })
      .populate({
        path: "media_id",
        model: "Media",
        select: ["_id", "thumbnail"],
      })
      .sort({ _id: -1 })
      .skip(skip)
      .limit(constants.pageLimit);

    if (!mediaReport.length) {
      return Response.successResponse(res, "No Reports till yet!", []);
    }
    let mediaReportResponse = mediaReport.map((media) => ({
      _id: media._id,
      createdTime: media.createdTime,
      reason: media.reason,
      isResolved: media.isResolved,
      media: {
        _id: media.media_id._id,
        thumbnail: media.media_id.thumbnail,
      },
    }));
    return Response.successResponse(res, "Success", mediaReportResponse);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
async function shareMediaContent(req, res) {
  try {
    const userId = req.user.id;
    const videoId = req.params.id;
    const mediaContent = await MediaModel.findOne({
      _id: videoId.toString(),
      isDeleted: false,
    });
    if (!mediaContent) return Response.notFoundResponse(res, "Video Not Found");
    mediaContent.shares.push(userId);
    await mediaContent.save();
    logMediaInteraction(userId, videoId, MediaInteractionType.SHARE);
    return Response.successResponse(
      res,
      "",
      `${config.baseUrl}/h/video/${videoId}`
    );
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
async function markMediaAsWatched(req, res) {
  try {
    const userId = req.user.id;
    const videoId = req.params.id;
    const mediaContent = await MediaModel.findOne({
      _id: videoId.toString(),
      isDeleted: false,
    });
    if (!mediaContent) return Response.notFoundResponse(res, "Video Not Found");
    if (mediaContent.watched_users.includes(userId)) {
      return Response.successResponse(res, "", "Already marked as watched for that user")
    }
    mediaContent.watched_users.push(userId);
    await mediaContent.save();
    logMediaInteraction(userId, videoId, MediaInteractionType.STARTED_WATCHING);
    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function markMediaAsWatchedTillEnd(req, res) {
  try {
    const userId = req.user.id;
    const videoId = req.params.id;
    const mediaContent = await MediaModel.findOne({
      _id: videoId.toString(),
      isDeleted: false,
    });
    if (!mediaContent) return Response.notFoundResponse(res, "Video Not Found");
    mediaContent.watched_till_end.push(userId);
    await mediaContent.save();
    logMediaInteraction(userId, videoId, MediaInteractionType.WATCHED_TILL_END);
    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function markVideoAsUnSafe(req, res) {
  try {
    const videoId = req.params.id;
    const mediaContent = await MediaModel.findOne({
      _id: videoId.toString(),
    });
    if (!mediaContent) return Response.notFoundResponse(res, "Video Not Found");
    mediaContent.isDeleted = true;
    mediaContent.unSafe = true;
    await mediaContent.save();
    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function pinVideo(req, res) {
  try {
    const currUser = req.user.id;
    const videoId = req.params.id;
    const { isPinned } = req.body;
    const mediaContent = await MediaModel.findOne({
      _id: videoId.toString(),
    });

    if (currUser != mediaContent.user_id) return Response.unauthorizedReqResponse(res, "You are not authorized to pin this video")

    if (!mediaContent) return Response.notFoundResponse(res, "Video Not Found");
    mediaContent.isPinned = isPinned;
    await mediaContent.save();
    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function saveIntoCollection(req, res) {
  try {
    const userId = req.user.id;
    const videoId = req.params.id;
    const user = await userModel.findOne({ _id: userId });

    //Remove from collection case
    const index = user.mediaCollection.findIndex((media) => media == videoId);
    if (index != -1) {
      user.mediaCollection.splice(index, 1);
    } else {
      user.mediaCollection.push(videoId);
    }
    await user.save();

    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}
async function getMediaCollection(req, res) {
  try {
    const userId = req.user.id;
    const videoId = req.params.id;
    const user = await userModel
      .findOne({ _id: userId })
      .populate({
        path: "mediaCollection",
        model: "Media",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .skip(skip)
      .sort({
        createdTime: -1,
      })
      .limit(constants.pageLimit);

    //Remove from collection case
    const index = user.mediaCollection.findIndex((media) => media == videoId);
    if (index != -1) {
      user.mediaCollection.splice(index, 1);
    } else {
      user.mediaCollection.push(videoId);
    }
    await user.save();

    return Response.successResponse(res);
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getMediaCategories(req, res) {
  try {
    let categories = await MediaCategories.find({
      isActive: true,
      isDeleted: false,
    });
    if (categories != null && categories.length > 0)
      return Response.successResponse(res, "", categories);
    return Response.badReqResponse(res);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function uploadMediaFile(req, res) {
  try {
    // Avatar
    if (req.files && req.files.photo) {
      const { photo } = req.files;
      // check if file extension is allowed
      let fileExtension = getFileExtension(photo.name);
      fileExtension = fileExtension.toLowerCase();
      logger.i(fileExtension)
      logger.i(
        `image Extension : ${fileExtension} ${constants.allowedImages.jpg} ${fileExtension === constants.allowedImages.jpg
        }`
      );
      if (
        fileExtension === constants.allowedImages.jpg ||
        fileExtension === constants.allowedImages.jpeg ||
        fileExtension === constants.allowedImages.png ||
        fileExtension === constants.allowedImages.heic ||
        fileExtension === constants.allowedFiles.pdf ||
        fileExtension === constants.allowedFiles.xlsx ||
        fileExtension === constants.allowedFiles.txt ||
        fileExtension === constants.allowedFiles.csv ||
        fileExtension === constants.allowedVideos.wmv ||
        fileExtension === constants.allowedVideos.mkv ||
        fileExtension === constants.allowedVideos.mp4 ||
        fileExtension === constants.allowedVideos.hevc ||
        fileExtension === constants.allowedVideos.avi ||
        fileExtension === constants.allowedVideos.gif ||
        fileExtension === constants.allowedVideos.mov

      ) {
        // check file mimetype
        // if (checkIfImageFile(uploadedFile) || fileExtension === 'pdf') {
        const fileName = `${new Date().getTime()}-${randomString(
          10
        )}.${fileExtension}`;
        const attachedFile = `${constants.uploadFile}${fileName}`;
        // check file size
        await photo.mv(attachedFile);
        const fileSize = fs.statSync(attachedFile).size / 1024; // kb
        if (fileSize <= constants.avatarMaxSize) {
          logger.i(`Image size: ${fileSize}`);
          // upload avatar to AWS
          const s3 = new AWS.S3({
            accessKeyId: config.aws.access_key_id,
            secretAccessKey: config.aws.secret_access_key,
            region: config.aws.region,
            signatureVersion: config.aws.signatureVersion,
          });
          const s3Params = {
            Bucket: config.aws.bucket_name,
            Key: fileName,
            Expires: 60 * 60,
            ContentType: constants.contentType.imagePNG,
            ACL: "public-read",
          };
          s3.getSignedUrl("putObject", s3Params);
          const fileContent = fs.readFileSync(attachedFile);
          const params = {
            Bucket: config.aws.bucket_name,
            Key: fileName,
            Body: fileContent,
            ACL: "public-read",
          };
          await s3.upload(params).promise();
          logger.i(
            ` Image : https://${config.aws.bucket_name}.s3.amazonaws.com/${fileName}`
          );
          try {
            // remove draft files
            fs.unlinkSync(attachedFile);
          } catch (err) {
            console.error(err);
          }
          // update
          const fileURL = `https://${config.aws.bucket_name}.s3.amazonaws.com/${fileName}`;
          return Response.successResponse(res, "", { fileURL });
        } else {
          return Response.badReqResponse(
            res,
            `Image max size is ${constants.avatarMaxSize} KB`
          );
        }
        // } else {
        //   return Response.badReqResponse(res, 'Image not an image file.');
      }
    } else {
      return Response.badReqResponse(
        res,
        "Image allowed files: JPG, JPEG, PNG."
      );
    }
  } catch (error) {
    logger.error(req, error);
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getActiveUserStoriesById(req, res) {
  try {
    const userId = req.params.userId;
    const currentUserId = req.user.id;
    const yesterday = Date.now() - 8.64e7;
    const usersFollowing = await UserFollowerModel.findOne({
      followed_userID: userId,
      follower_userID: currentUserId,
      isDeleted: false,
    });
    const stories = await MediaModel.find({
      user_id: userId,
      is_story: true,
      isDeleted: false,
      createdTime: { $gt: yesterday },
    })
      .populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .sort({ createdTime: -1 });

    const storiesIds = stories.map((story) => story._id.toString());
    const storiesMapped = [];
    await Promise.all(
      stories.map(async (story) => {
        let mediaCategory = null;
        if (story.category !== "") {
          try {
            mediaCategory = await MediaCategories.findOne({
              _id: story.category,
            });
          } catch (e) { }
        }

        let comments = await MediaComments.find({
          _id: { $in: story.comments },
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .populate({
            path: "replies.user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          });

        comments = comments.map((comment) => ({
          id: comment._id,
          user: comment.user_id,
          comment: comment.comment,
          likes: comment.likes.length,
          isLiked: comment.likes.includes(currentUserId),
          createdTime: comment.createdTime,
          replies: comment.replies?.map((reply) => ({
            id: reply.id,
            user: reply.user_id,
            reply: reply.reply,
            likes: reply.likes.length,
            isLiked: reply.likes.includes(currentUserId),
            createdTime: reply.createdTime,
          })),
        }));

        const storyResponse = {
          mediaId: story._id.toString(),
          createdTime: story.createdTime,
          user: {
            _id: story.user_id._id,
            name: story.user_id.name,
            avatar: story.user_id.avatar,
            username: story.user_id.username,
            isFollowed: usersFollowing ? true : false,
          },
          likes: story.likes.length,
          shares: story.shares ? story.shares.length : 0,
          views: story.watched_users ? story.watched_users.length : 0,
          description: story.description,
          linkedFiles: story.linked_files,
          hlsUrl: story.hls_url,
          reducedVideoUrl: story.reducedVideoUrl,
          reducedVideoHlsUrl: story.reducedVideoHlsUrl,
          shortVideoUrl: story.shortVideoUrl,
          shortVideoHlsUrl: story.shortVideoHlsUrl,
          privacyOptions: story.privacyOptions,
          thumbnailUrl: story.thumbnailUrl,
          thumbnail: story.thumbnail,
          originalUrl: story.originalUrl,
          comments: comments,
          isLiked: story.likes.includes(currentUserId),
          category: mediaCategory != null ? mediaCategory.name : "",
          type: story.type,
          isWatched: story.watched_users.find(
            (user) => user.toString() === currentUserId
          )
            ? true
            : false,
        };
        storiesMapped.push(storyResponse);
      })
    );

    const storiesGrouped = Object.values(
      storiesMapped.reduce((arr, story) => {
        if (!arr[story.user._id])
          arr[story.user._id] = { ...story.user, stories: [] };
        arr[story.user._id].stories.push(story);
        return arr;
      }, {})
    );

    return Response.successResponse(res, "", storiesGrouped);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getActiveCurrentUserStories(req, res) {
  try {
    const currentUserId = req.user.id;
    const yesterday = Date.now() - 8.64e7;
    const stories = await MediaModel.find({
      user_id: currentUserId,
      is_story: true,
      isDeleted: false,
      createdTime: { $gt: yesterday },
    })
      .populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .populate({
        path: "watched_users",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .sort({ createdTime: -1 });

    const storiesIds = stories.map((story) => story._id.toString());
    const storiesMapped = [];
    await Promise.all(
      stories.map(async (story) => {
        let mediaCategory = null;
        if (story.category !== "") {
          try {
            mediaCategory = await MediaCategories.findOne({
              _id: story.category,
            });
          } catch (e) { }
        }

        let comments = await MediaComments.find({
          _id: { $in: story.comments },
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .populate({
            path: "replies.user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          });

        comments = comments.map((comment) => ({
          id: comment._id,
          user: comment.user_id,
          comment: comment.comment,
          likes: comment.likes.length,
          isLiked: comment.likes.includes(currentUserId),
          createdTime: comment.createdTime,
          replies: comment.replies?.map((reply) => ({
            id: reply.id,
            user: reply.user_id,
            reply: reply.reply,
            likes: reply.likes.length,
            isLiked: reply.likes.includes(currentUserId),
            createdTime: reply.createdTime,
          })),
        }));

        const storyResponse = {
          mediaId: story._id.toString(),
          createdTime: story.createdTime,
          user: {
            _id: story.user_id._id,
            name: story.user_id.name,
            username: story.user_id.username,
            avatar: story.user_id.avatar,
          },
          watchedUsers: story.watched_users,
          likes: story.likes.length,
          shares: story.shares ? story.shares.length : 0,
          views: story.watched_users ? story.watched_users.length : 0,
          description: story.description,
          linkedFiles: story.linked_files,
          hlsUrl: story.hls_url,
          reducedVideoUrl: story.reducedVideoUrl,
          reducedVideoHlsUrl: story.reducedVideoHlsUrl,
          shortVideoUrl: story.shortVideoUrl,
          shortVideoHlsUrl: story.shortVideoHlsUrl,
          privacyOptions: story.privacyOptions,
          thumbnailUrl: story.thumbnailUrl,
          thumbnail: story.thumbnail,
          originalUrl: story.originalUrl,
          comments: comments,
          isLiked: story.likes.includes(currentUserId),
          category: mediaCategory != null ? mediaCategory.name : "",
          type: story.type,
          isWatched: story.watched_users.find(
            (user) => user.toString() === currentUserId
          )
            ? true
            : false,
        };
        storiesMapped.push(storyResponse);
      })
    );

    const storiesGrouped = Object.values(
      storiesMapped.reduce((arr, story) => {
        if (!arr[story.user._id])
          arr[story.user._id] = { ...story.user, stories: [] };
        arr[story.user._id].stories.push(story);
        return arr;
      }, {})
    );

    return Response.successResponse(res, "", storiesGrouped);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getAllCurrentUserStories(req, res) {
  try {
    const currentUserId = req.user.id;
    const yesterday = Date.now() - 8.64e7;
    const stories = await MediaModel.find({
      user_id: currentUserId,
      is_story: true,
      isDeleted: false,
    })
      .populate({
        path: "user_id",
        model: "User",
        select: ["_id", "name", "avatar", "username", "isVerified"],
      })
      .sort({ createdTime: -1 });

    const storiesIds = stories.map((story) => story._id.toString());
    const storiesMapped = [];
    await Promise.all(
      stories.map(async (story) => {
        let mediaCategory = null;
        if (story.category !== "") {
          try {
            mediaCategory = await MediaCategories.findOne({
              _id: story.category,
            });
          } catch (e) { }
        }

        let comments = await MediaComments.find({
          _id: { $in: story.comments },
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .populate({
            path: "replies.user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          });

        comments = comments.map((comment) => ({
          id: comment._id,
          user: comment.user_id,
          comment: comment.comment,
          likes: comment.likes.length,
          isLiked: comment.likes.includes(currentUserId),
          createdTime: comment.createdTime,
          replies: comment.replies?.map((reply) => ({
            id: reply.id,
            user: reply.user_id,
            reply: reply.reply,
            likes: reply.likes.length,
            isLiked: reply.likes.includes(currentUserId),
            createdTime: reply.createdTime,
          })),
        }));

        const storyResponse = {
          mediaId: story._id.toString(),
          createdTime: story.createdTime,
          user: {
            _id: story.user_id._id,
            name: story.user_id.name,
            username: story.user_id.username,
            avatar: story.user_id.avatar,
          },
          likes: story.likes.length,
          shares: story.shares ? story.shares.length : 0,
          views: story.watched_users ? story.watched_users.length : 0,
          description: story.description,
          linkedFiles: story.linked_files,
          hlsUrl: story.hls_url,
          reducedVideoUrl: story.reducedVideoUrl,
          reducedVideoHlsUrl: story.reducedVideoHlsUrl,
          shortVideoUrl: story.shortVideoUrl,
          shortVideoHlsUrl: story.shortVideoHlsUrl,
          privacyOptions: story.privacyOptions,
          thumbnailUrl: story.thumbnailUrl,
          thumbnail: story.thumbnail,
          originalUrl: story.originalUrl,
          comments: comments,
          isLiked: story.likes.includes(currentUserId),
          category: mediaCategory != null ? mediaCategory.name : "",
          type: story.type,
          isWatched: story.watched_users.find(
            (user) => user.toString() === currentUserId
          )
            ? true
            : false,
        };
        storiesMapped.push(storyResponse);
      })
    );

    const storiesGrouped = Object.values(
      storiesMapped.reduce((arr, story) => {
        if (!arr[story.user._id])
          arr[story.user._id] = { ...story.user, stories: [] };
        arr[story.user._id].stories.push(story);
        return arr;
      }, {})
    );

    return Response.successResponse(res, "", storiesGrouped);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getCurrentUserHighlights(req, res) {
  try {
    const currentUserId = req.user.id;
    const highlights = await HighlightModel.find({
      userId: currentUserId,
      isDeleted: false,
    });
    const result = [];
    await Promise.all(
      highlights.map(async (highlight) => {
        const stories = await MediaModel.find({
          _id: { $in: highlight.storiesIds },
          user_id: currentUserId,
          is_story: true,
          isDeleted: false,
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .sort({ createdTime: -1 });

        const storiesIds = stories.map((story) => story._id.toString());
        const storiesMapped = [];
        await Promise.all(
          stories.map(async (story) => {
            let mediaCategory = null;
            if (story.category !== "") {
              try {
                mediaCategory = await MediaCategories.findOne({
                  _id: story.category,
                });
              } catch (e) { }
            }

            let comments = await MediaComments.find({
              _id: { $in: story.comments },
            })
              .populate({
                path: "user_id",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
              })
              .populate({
                path: "replies.user_id",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
              });

            comments = comments.map((comment) => ({
              id: comment._id,
              user: comment.user_id,
              comment: comment.comment,
              likes: comment.likes.length,
              isLiked: comment.likes.includes(currentUserId),
              createdTime: comment.createdTime,
              replies: comment.replies?.map((reply) => ({
                id: reply.id,
                user: reply.user_id,
                reply: reply.reply,
                likes: reply.likes.length,
                isLiked: reply.likes.includes(currentUserId),
                createdTime: reply.createdTime,
              })),
            }));

            const storyResponse = {
              mediaId: story._id.toString(),
              createdTime: story.createdTime,
              user: {
                _id: story.user_id._id,
                name: story.user_id.name,
                username: story.user_id.username,
                avatar: story.user_id.avatar,
              },
              likes: story.likes.length,
              shares: story.shares ? story.shares.length : 0,
              views: story.watched_users ? story.watched_users.length : 0,
              description: story.description,
              linkedFiles: story.linked_files,
              hlsUrl: story.hls_url,
              reducedVideoUrl: story.reducedVideoUrl,
              reducedVideoHlsUrl: story.reducedVideoHlsUrl,
              shortVideoUrl: story.shortVideoUrl,
              shortVideoHlsUrl: story.shortVideoHlsUrl,
              privacyOptions: story.privacyOptions,
              thumbnailUrl: story.thumbnailUrl,
              thumbnail: story.thumbnail,
              originalUrl: story.originalUrl,
              comments: comments,
              isLiked: story.likes.includes(currentUserId),
              category: mediaCategory != null ? mediaCategory.name : "",
              type: story.type,
              isWatched: story.watched_users.find(
                (user) => user.toString() === currentUserId
              )
                ? true
                : false,
            };
            storiesMapped.push(storyResponse);
          })
        );

        const storiesGrouped = Object.values(
          storiesMapped.reduce((arr, story) => {
            if (!arr[story.user._id])
              arr[story.user._id] = { ...story.user, stories: [] };
            arr[story.user._id].stories.push(story);
            return arr;
          }, {})
        );

        const highlightDto = {
          _id: highlight._id,
          name: highlight.name,
          thumbnail: highlight.thumbnail,
          storiesGrouped,
        };
        result.push(highlightDto);
      })
    );
    return Response.successResponse(res, "", result);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function getUserHighlights(req, res) {
  try {
    const userId = req.params.userId;
    const currentUserId = req.user.id;
    const highlights = await HighlightModel.find({
      userId: userId,
      isDeleted: false,
    });
    const result = [];
    await Promise.all(
      highlights.map(async (highlight) => {
        const stories = await MediaModel.find({
          _id: { $in: highlight.storiesIds },
          is_story: true,
          isDeleted: false,
        })
          .populate({
            path: "user_id",
            model: "User",
            select: ["_id", "name", "avatar", "username", "isVerified"],
          })
          .sort({ createdTime: -1 });

        const storiesIds = stories.map((story) => story._id.toString());
        const storiesMapped = [];
        await Promise.all(
          stories.map(async (story) => {
            let mediaCategory = null;
            if (story.category !== "") {
              try {
                mediaCategory = await MediaCategories.findOne({
                  _id: story.category,
                });
              } catch (e) { }
            }

            let comments = await MediaComments.find({
              _id: { $in: story.comments },
            })
              .populate({
                path: "user_id",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
              })
              .populate({
                path: "replies.user_id",
                model: "User",
                select: ["_id", "name", "avatar", "username", "isVerified"],
              });

            comments = comments.map((comment) => ({
              id: comment._id,
              user: comment.user_id,
              comment: comment.comment,
              likes: comment.likes.length,
              isLiked: comment.likes.includes(currentUserId),
              createdTime: comment.createdTime,
              replies: comment.replies?.map((reply) => ({
                id: reply.id,
                user: reply.user_id,
                reply: reply.reply,
                likes: reply.likes.length,
                isLiked: reply.likes.includes(currentUserId),
                createdTime: reply.createdTime,
              })),
            }));

            const storyResponse = {
              mediaId: story._id.toString(),
              createdTime: story.createdTime,
              user: {
                _id: story.user_id._id,
                name: story.user_id.name,
                username: story.user_id.username,
                avatar: story.user_id.avatar,
              },
              likes: story.likes.length,
              shares: story.shares ? story.shares.length : 0,
              views: story.watched_users ? story.watched_users.length : 0,
              description: story.description,
              linkedFiles: story.linked_files,
              hlsUrl: story.hls_url,
              reducedVideoUrl: story.reducedVideoUrl,
              reducedVideoHlsUrl: story.reducedVideoHlsUrl,
              shortVideoUrl: story.shortVideoUrl,
              shortVideoHlsUrl: story.shortVideoHlsUrl,
              privacyOptions: story.privacyOptions,
              thumbnailUrl: story.thumbnailUrl,
              thumbnail: story.thumbnail,
              originalUrl: story.originalUrl,
              comments: comments,
              isLiked: story.likes.includes(currentUserId),
              category: mediaCategory != null ? mediaCategory.name : "",
              type: story.type,
              isWatched: story.watched_users.find(
                (user) => user.toString() === currentUserId
              )
                ? true
                : false,
            };
            storiesMapped.push(storyResponse);
          })
        );

        const storiesGrouped = Object.values(
          storiesMapped.reduce((arr, story) => {
            if (!arr[story.user._id])
              arr[story.user._id] = { ...story.user, stories: [] };
            arr[story.user._id].stories.push(story);
            return arr;
          }, {})
        );

        const highlightDto = {
          _id: highlight._id,
          name: highlight.name,
          thumbnail: highlight.thumbnail,
          storiesGrouped,
        };
        result.push(highlightDto);
      })
    );
    return Response.successResponse(res, "", result);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function createHighlight(req, res) {
  try {
    const { name, storiesIds } = req.body;
    const currentUserId = req.user.id;

    const s3 = new AWS.S3({
      accessKeyId: config.aws.access_key_id,
      secretAccessKey: config.aws.secret_access_key,
      region: config.aws.region,
      signatureVersion: config.aws.signatureVersion,
    });
    logger.d(`userId: ${req.user.id}`);

    const draftFolder = `${constants.uploadFile + randomString(10)}/`;
    logger.i(`Draft folder: ${draftFolder}`);

    const highlight = new HighlightModel();
    highlight.userId = currentUserId;
    highlight.name = name;
    highlight.storiesIds = storiesIds;

    const { file } = req.files;
    //upload image media
    const imageFileName = `${randomString(20) + "-" + new Date().getTime()
      }.png`;
    const imageFile = `${draftFolder + imageFileName}`;

    await file.mv(imageFile);

    //reduce image quality
    try {
      const compress_images = require("compress-images");
      await compress_images(
        imageFile,
        draftFolder,
        {
          compress_force: false,
          statistic: false,
          autoupdate: true,
          pathLog: "./uploads/",
        },
        false,
        { jpg: { engine: "mozjpeg", command: ["-quality", "30"] } },
        {
          png: {
            engine: "pngquant",
            command: ["--quality=20-30", "--ext=.png", "--force"],
          },
        },
        { svg: { engine: false, command: false } },
        { gif: { engine: false, command: false } },
        function (error) {
          logger.e(`compress Image error ${error}`);
          if (error != null) {
            //Delete draft if there is any error
            fs.rmSync(draftFolder, { recursive: true, force: true });
          }
        }
      );
    } catch (e) { }

    const sizeOf = require("image-size");
    const dimensions = sizeOf(imageFile);

    try {
      const s3ThumbnailParams = {
        Bucket: config.aws.bucket_name,
        Key: imageFileName,
        Expires: 60 * 60,
        ContentType: constants.contentType.imageJPG,
        ACL: "public-read",
      };
      s3.getSignedUrl("putObject", s3ThumbnailParams);
      const fileContent = fs.readFileSync(imageFile);
      const params = {
        Bucket: config.aws.bucket_name,
        Key: imageFileName,
        Body: fileContent,
        ACL: "public-read",
      };

      await s3.upload(params).promise();

      const thumbnailFileURL = `https://${config.aws.bucket_name}.s3.amazonaws.com/${imageFileName}`;
      logger.i(`thumbnailFileURL ${thumbnailFileURL}`);

      // add image media
      highlight.thumbnail = thumbnailFileURL;

      await highlight.save();
    } catch (e) {
      logger.e(e);
    }
    try {
      // delete all drafts
      fs.rmSync(draftFolder, { recursive: true, force: true });
    } catch (e) {
      logger.e(e);
    }
    // await NotificationService.videoProcessingNotification(req.user.id, { media, mediaMeta })
    return Response.successResponse(res, "", highlight);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function updateHighlight(req, res) {
  try {
    const { highlightId, name, storiesIds } = req.body;

    const highlight = await HighlightModel.findOne({ _id: highlightId });
    highlight.name = name;
    highlight.storiesIds = storiesIds;
    await highlight.save();

    return Response.successResponse(res, "", highlight);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function deleteHighlight(req, res) {
  try {
    const highlightId = req.params.highlightId;

    const highlight = await HighlightModel.findOne({ _id: highlightId });
    highlight.isDeleted = true;
    await highlight.save();

    return Response.successResponse(res);
  } catch (error) {
    return Response.serverErrorResponse(res, "", error);
  }
}

async function updateMediaContent(req, res) {
  try {
    const { description, tags, category, privacyOptions } = req.body;
    const mediaId = req.params.id;
    const mediaContent = await MediaModel.findOne({
      _id: mediaId.toString(),
    });
    if (description) {
      mediaContent.description = description;
    }
    if (tags) {
      mediaContent.tags = tags != null && tags !== "" ? tags.split(",") : [];
    }
    if (category) {
      mediaContent.category = category != null && category !== "" ? category : "";
    }
    if (privacyOptions && privacyOptions.isOnlyMe) {
      mediaContent.privacyOptions.isOnlyMe = privacyOptions.isOnlyMe;
    }
    await mediaContent.save();
    return Response.successResponse(res, "", { mediaContent });
  } catch (error) {
    logger.e(error)
    return Response.serverErrorResponse(res, "Something went wrong", error);
  }
}

async function deleteMediaContent(req, res) {
  try {
    const mediaId = req.params.id;
    const mediaContent = await MediaModel.findOne({
      _id: mediaId.toString(),
    });
    mediaContent.isDeleted = true;
    await mediaContent.save();
    deleteMediaInteractions(mediaId.toString())
    return Response.successResponse(res);
  } catch (error) {
    logger.e(error)
    return Response.serverErrorResponse(res, "Something went wrong", error);
  }
}


module.exports = {
  getVideoFeedV2,
  getStoriesFeed,
  getVideosFeed,
  getMediaContent,
  likeMediaContent,
  commentOnMediaContent,
  likeComment,
  getReportingReasons,
  reportMediaContent,
  getUserReports,
  shareMediaContent,
  markMediaAsWatched,
  saveIntoCollection,
  getMediaCollection,
  getMediaCategories,
  uploadMediaFile,
  getActiveUserStoriesById,
  getActiveCurrentUserStories,
  getAllCurrentUserStories,
  getCurrentUserHighlights,
  getUserHighlights,
  createHighlight,
  updateHighlight,
  deleteHighlight,
  addMediaUpgraded,
  updateByEncoder,
  markMediaAsWatchedTillEnd,
  updateMediaContent,
  deleteMediaContent,
  markVideoAsUnSafe,
  pinVideo
};
